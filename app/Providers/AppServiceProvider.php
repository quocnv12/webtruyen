<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\models\{Category, Review, Story};

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with([
                'categories' => Category::all(),
                'reviews' => Review::orderBy('id', 'DESC')->take(5)->get(),
                'story_goods' => Story::where('story_good', 1)->where('display', 1)->orderBy('id', 'DESC')->take(10)->get(),
                'story_news' => Story::where('display', 1)->orderBy('id', 'DESC')->take(10)->get(),
            ]);
        });
    }
}
