<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'  =>  'required|email|unique:users,email,'.$this->id.'id',
            'name' => 'required|min:3|max:30',
        ];
    }
    public function messages()
    {
        return [
           'email.required' => 'Email không được để trống!',
           'email.email' => 'Email không đúng định dạng!',
           'email.unique' => 'Email đã tồn tại!',
           'name.required' => 'Tên hiển thị không được để trống!',
           'name.min' => 'Tên hiển thị phải lớn hơn 3 kí tự!',
           'name.max' => 'Tên hiển thị phải ít hơn 30 kí tự!',
        ];
    }
}
