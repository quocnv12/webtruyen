<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddStoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255|unique:stories,name',
            'author' => 'required|min:3|max:255',
            'summary' => 'required|min:6',
            'source' => 'required|max:255',
            'image' => 'required',
            'categories' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên truyện không được để trống!',
            'name.unique' => 'Tên truyện đã tồn tại!',
            'author.required' => 'Tên tác giả không được để trống!',
            'summary.required' => 'Tóm tắt không được để trống!',
            'image.required' => 'Hình ảnh không được để trống!',
            'source.required' => 'Nguồn truyện không được để trống!',
            'categories.required' => 'Thể loại không được để trống!',
            'name.min' => 'Tên truyện phải lớn hơn 3 kí tự!',
            'author.min' => 'Tên tác giả phải lớn hơn 3 kí tự!',
            'summary.min' => 'Tóm tắt phải lớn hơn 6 kí tự!',
            'name.max' => 'Tên truyện phải ít hơn 255 kí tự!',
            'author.max' => 'Tên tác giả phải ít hơn 255 kí tự!',
            'source.max' => 'Nguồn truyện phải ít hơn 255 kí tự!',
        ];
    }
}
