<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangerPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old'=>'required',
            'password'=>'required|min:6|max:30',
            'password_confirmation'=>'required|same:password'
        ];
    }
    public function messages()
    {
        return [
            'password_old.required'=>'Mật khẩu cũ không được để trống!',
            'password.required'=>'Mật khẩu mới không được để trống!',
            'password.min'=>'Mật khẩu tối thiếu 6 kí tự!',
            'password.max'=>'Mật khẩu tối đa 30 kí tự!',
            'password_confirmation.required'=>'Nhập lại mật khẩu không được để trống!',
            'password_confirmation.same'=>'Mật khẩu không trùng khớp!'
        ];
    }
}
