<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:6|max:255|unique:review,title',
            'summary' => 'required|min:6',
            'content' => 'required|min:6',
            'image' => 'required',
            'author' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề không được để trống!',
            'title.min' => 'Tiêu đề phải lớn hơn 6 kí tự!',
            'title.max' => 'Tiêu đề phải ít hơn 255 kí tự!',
            'title.unique' => 'Tiêu đề đã tồn tại!',
            'summary.required' => 'Tóm tắt không được để trống!',
            'summary.min' => 'Tóm tắt phải lớn hơn 6 kí tự!',
            'content.required' => 'Nội dung không được để trống!',
            'content.min' => 'Nội dung phải lớn hơn 6 kí tự!',
            'image.required' => 'Hình ảnh không được để trống!',
            'author.required' => 'Tác giả không được để trống!',
        ];
    }
}
