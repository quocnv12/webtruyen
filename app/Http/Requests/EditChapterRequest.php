<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditChapterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_chapter' => 'required',
            'name' => 'required|unique:chapters,name,'.$this->id.'id',
            'content' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name_chapterrequired' => 'Tên chương không được để trống!',
            'name.required' => 'Nội dung tên chương không được để trống',
            'name.unique' => 'Tên chương đã tồn tại',
            'content.required' => 'Không được để trống',
        ];
    }
}
