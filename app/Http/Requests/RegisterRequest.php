<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'     =>  'required|email|unique:users,email',
            'password'  =>  'required|min:6|max:30',
            'passwordConfirmation'  =>  'required|same:password',
            'name' => 'required|min:3|max:30',
        ];
    }
    public function messages()
    {
        return [
           'email.required' => 'Email không được để trống!',
           'email.email' => 'Email không đúng định dạng!',
           'email.unique' => 'Email đã tồn tại!',
           'password.required' => 'Mật khẩu không được để trống!',
           'password.min' => 'Mật khẩu phải lớn hơn 6 kí tự!',
           'password.max' => 'Mật khẩu phải ít hơn 30 kí tự!',
           'passwordConfirmation.required' => 'Nhập lại mật khẩu!',
           'passwordConfirmation.same' => 'Mật khẩu không trùng khớp!',
           'name.required' => 'Tên hiển thị không được để trống!',
           'name.min' => 'Tên hiển thị phải lớn hơn 3 kí tự!',
           'name.max' => 'Tên hiển thị phải ít hơn 30 kí tự!',
        ];
    }
}
