<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\ShortStory;
use App\Http\Requests\{AddReviewRequest, EditReviewRequest};

class ShortStoryController extends Controller
{
    public function getList()
    {
        $data['short_story'] = ShortStory::all();
        return view('backend.short_story.list', $data);
    }

    public function getDetail($id)
    {
        $data['short_story'] = ShortStory::find($id);
        return view('backend.short_story.detail', $data);
    }

    public function getAdd()
    {
        return view('backend.short_story.add');
    }

    public function postAdd(AddReviewRequest $request)
    {
        $short_story = new ShortStory;
        $short_story->title = $request->title;
        $short_story->slug = str_slug($request->title);
        $short_story->summary = $request->summary;
        $short_story->summary = $request->summary;
        $short_story->author = $request->author;
        $short_story->content = $request->content;
        if($request->hasFile('image'))
        {
            $file = $request->image;
            $file_name = str_slug($request->image).'.'.$file->getClientOriginalExtension();
            $file->move('frontend/images',$file_name);
            $short_story->image = $file_name;
        }
        $short_story->save();
        return redirect(route('admin.get.list.short.story'))->with('success', 'Thêm truyện ngắn thành công!')->withInput();
    }


    public function getEdit($id)
    {
        $data['short_story'] = ShortStory::find($id);
        return view('backend.short_story.edit', $data);
    }
    public function postEdit(EditReviewRequest $request, $id)
    {
        $short_story = ShortStory::find($id);
        $short_story->title = $request->title;
        $short_story->slug = str_slug($request->title);
        $short_story->summary = $request->summary;
        $short_story->summary = $request->summary;
        $short_story->author = $request->author;
        $short_story->content = $request->content;
        if($request->hasFile('image'))
        {
            unlink('frontend/images/'.$short_story->image);
            $file = $request->image;
            $file_name = str_slug($request->image).'.'.$file->getClientOriginalExtension();
            $file->move('frontend/images',$file_name);
            $short_story->image = $file_name;
        }
        $short_story->save();
        return redirect(route('admin.get.list.short.story'))->with('success', 'Sửa truyện ngắn thành công!')->withInput();
    }

    public function delete($id)
    {
        ShortStory::destroy($id);
        return redirect(route('admin.get.list.short.story'))->with('success', 'Xóa truyện ngắn thành công!');
    }

}
