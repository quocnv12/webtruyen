<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\{Story, Chapter};
use App\Http\Requests\{AddChapterRequest, EditChapterRequest};

class ChapterController extends Controller
{
    public function getList($id)
    {
        $data['story'] = Story::find($id);
        $data['id'] = $id;
        return view('backend.chapter.list', $data);
    }

    public function getAdd($id)
    {
        $data['id'] = $id;
        return view('backend.chapter.add', $data);
    }
    public function postAdd(AddChapterRequest $request, $id)
    {
        $stories = Story::find($id);
        $number_chapter = Chapter::where('id_stories' ,$id)->orderBy('id', 'DESC')->take(1)->first();
        $chapter = new Chapter;
        $chapter->name_chapter = $request->name_chapter;
        $chapter->name = $request->name;
        $chapter->slug = str_slug($request->name);
        $chapter->content = $request->content;
        $chapter->id_stories = $id;
        $chapter->number_chapter = $number_chapter->number_chapter + 1;
        $chapter->save();

        return redirect(route('admin.get.list.chapter',['id' => $id]))->with('success', 'Thêm chương thành công');
        
    }


    public function getEdit($id)
    {
        $data['chapter'] = Chapter::find($id);
        $data['id'] = $id;
        return view('backend.chapter.edit', $data);
    }

    public function postEdit(EditChapterRequest $request, $id)
    {

        $chapter = Chapter::find($id);
        $chapter->name_chapter = $request->name_chapter;
        $chapter->name = $request->name;
        $chapter->slug = str_slug($request->name);
        $chapter->content = $request->content;
        $chapter->save();

        return redirect(route('admin.get.list.chapter',['id' => $chapter->id_stories]))->with('success', 'Sửa chương thành công');
        
    }

    public function delete($id)
    {
        $chapter = Chapter::find($id);
        Chapter::destroy($id);
        return redirect(route('admin.get.list.chapter',['id' => $chapter->id_stories]))->with('success', 'Xóa chương thành công');
    }
}
