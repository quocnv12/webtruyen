<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Category;
use App\Http\Requests\{AddCategoryRequest, EditCategoryRequest};

class CategoryController extends Controller
{
    public function getList()
    {
        $data['categories'] = Category::all();
        return view('backend.category.list', $data);
    }

    //thêm thể loại
    public function getAdd()
    {
        return view('backend.category.add');
    }
    public function postAdd(AddCategoryRequest $request)
    {
        $cate = new Category;
        $cate->name = $request->name;
        $cate->slug = str_slug($request->name);
        $cate->save();
        return redirect(route('admin.get.list.category'))->with('success', 'Thêm thể loại thành công')->withInput();
    }

    //sửa thể loại
    public function getEdit($id)
    {
        $data['cate'] = Category::find($id);
        return view('backend.category.edit', $data);
    }
    public function postEdit(EditCategoryRequest $request, $id)
    {
        $cate = Category::find($id);
        $cate->name = $request->name;
        $cate->slug = str_slug($request->name);
        $cate->save();
        return redirect(route('admin.get.list.category'))->with('success', 'Sửa thể loại thành công')->withInput();
    }

    //xóa thể loại
    public function delete($id)
    {
        Category::destroy($id);
        return redirect(route('admin.get.list.category'))->with('success', 'Xóa thể loại thành công')->withInput();
    }
}
