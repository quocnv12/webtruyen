<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLoginAdmin()
    {
        return view('backend.login.login');
    }

    public function postLoginAdmin(request $request)
    {
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password], $request->remember)) 
        {
            return redirect(route('admin.index'))->withInput();
        }
        else
        {
            return redirect(route('get.login.admin'))->with('danger', 'Email hoặc mật khẩu không chính xác!');
        }
    }
    

}
