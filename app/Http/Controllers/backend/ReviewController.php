<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Review;
use App\Http\Requests\{AddReviewRequest, EditReviewRequest};

class ReviewController extends Controller
{
    public function getList()
    {
        $data['review'] = Review::all();
        return view('backend.review.list', $data);
    }

    public function getDetail($id)
    {
        $data['review'] = Review::find($id);
        return view('backend.review.detail', $data);
    }

    public function getAdd()
    {
        return view('backend.review.add');
    }
    public function postAdd(AddReviewRequest $request)
    {
        //  dd($request->all());
        $review = new Review;
        $review->title = $request->title;
        $review->slug = str_slug($request->title);
        $review->summary = $request->summary;
        $review->author = $request->author;
        $review->content = $request->content;
        if($request->hasFile('image'))
        {
            $file = $request->image;
            $file_name = str_slug($request->image).'.'.$file->getClientOriginalExtension();
            $file->move('frontend/images',$file_name);
            $review->image = $file_name;
        }
        $review->save();
        return redirect(route('admin.get.list.review'))->with('success', 'Thêm review thành công!')->withInput();
    }

    public function getEdit($id)
    {
        $data['review'] = Review::find($id);
        return view('backend.review.edit', $data);
    }

    public function postEdit(EditReviewRequest $request, $id)
    {
        $review =  Review::find($id);   
        $review->title = $request->title;
        $review->slug = str_slug($request->title);
        $review->summary = $request->summary;
        $review->author = $request->author;
        $review->content = $request->content;
        if($request->hasFile('image'))
        {
            unlink('frontend/images/'.$review->image);
            $file = $request->image;
            $file_name = str_slug($request->image).'.'.$file->getClientOriginalExtension();
            $file->move('frontend/images',$file_name);
            $review->image = $file_name;
        }
        $review->save();
        return redirect(route('admin.get.list.review'))->with('success', 'Thêm review thành công!')->withInput();
    }

    public function delete($id)
    {
        Review::destroy($id);
        return redirect(route('admin.get.list.review'))->with('success', 'Xóa review thành công!')->withInput();
    }
}
