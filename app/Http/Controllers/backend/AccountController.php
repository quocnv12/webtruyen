<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Requests\{UpdateInfoRequest, ForgotPasswordRequest, ResetPasswordRequest};
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AccountController extends Controller
{
    public function getProfile($id)
    {
        $data['user'] = User::find($id);
        return view('backend.account.profile', $data);
    }

    public function postProfile(UpdateInfoRequest $request, $id)
    {
        $user = User::find($id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->save();
        return redirect()->back()->with('success', 'Cập nhật thông tin tài khoản thành công');
    }

    public function getChangerPassword()
    {
        return view('backend.account.change_password');
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect(route('get.login.admin'));
    }


    //quên mật khẩu
    public function getForgotPassword()
    {
        return view('backend.login.forgot_password');
    }
    public function postForgotPassword(ForgotPasswordRequest $request)
    {
        $email=$request->email;
        $check_email = User::where('email',$email)->first();
        if(!$check_email)
        {
            return redirect()->back()->with('danger', 'Không tìm thấy địa chỉ email!')->withInput();
        }
        $code = bcrypt(time().$email);
        $check_email->code =$code;
        $check_email->time_code =Carbon::now();
        $check_email->save();
        $url = route('admin.link.reset.password',['code'=>$check_email->code,'email'=>$email]);
        $data=[
            'route' => $url,
        ];

        Mail::send('frontend.password.email_reset', $data, function($message) use ($email){
            $message->to($email, 'Reset Password')->subject('Reset Password Webtruyen!');
        });

        return redirect()->back()->with('success','Gửi mail thành công');
    }
    public function getResetPassword(request $request)
    {
        $code = $request->code;
        $email = $request->email;
       
        $check_email = User::where([
            'code' => $code,
            'email' => $email])->first();
        if(!$check_email)
        {
            return redirect()->back()->with('danger','Link lấy lại mật khẩu không chính xác!');   
        }
        return view('backend.login.reset_passsword');
    }

    public function postResetPassword(ResetPasswordRequest $request)
    {
        //  dd($request->all());
        if($request->password)
        {
            $code = $request->code;
            $email = $request->email;
            $check_email=User::where([
                'code'=>$code,
                'email'=>$email
            ])->first(); 
        }
        if(!$check_email)
        {
           return redirect()->back()->with('danger','Link lấy lại mật khẩu không chính xác!')->withInput(); 
        }

        $check_email->password=bcrypt($request->password);
        $code = bcrypt(str::random(9));
        $check_email->code =$code;
        $check_email->save();

        return redirect(route('get.login.admin'))->with('success','Lấy lại mật khẩu thành công');
    }


}
