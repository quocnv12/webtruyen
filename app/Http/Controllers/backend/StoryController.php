<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\{Story, Category};
use App\Http\Requests\{AddStoryRequest, EditStoryRequest};
use DB;

class StoryController extends Controller
{
    public function getList()
    {
        $data['stories'] = Story::all();
        return view('backend.story.list', $data);
    }

    public function getAdd()
    {
        $data['categories'] = Category::all();
        return view('backend.story.add', $data);
    }
    public function postAdd(AddStoryRequest $request)
    {
        $story = new Story;
        $story->name = $request->name;
        $story->slug = str_slug($request->name);
        $story->summary = $request->summary;
        $story->source = $request->source;
        $story->status = $request->status;
        $story->active = $request->active;
        $story->author = $request->author;
        $story->display = $request->display;
        $story->view = 0;
        if($request->hasFile('image'))
        {
            $file = $request->image;
            $file_name = str_slug($request->image).'.'.$file->getClientOriginalExtension();
            $file->move('frontend/images',$file_name);
            $story->image = $file_name;
        }
        $story->save();

        $story->categories()->Attach($request->categories);

        return redirect(route('admin.get.list.story'))->with('success', 'Thêm truyện thành công')->withInput();
    }

    public function getEdit($id)
    {
        $data['story'] = Story::find($id);
        $data['categories'] = Category::all();

        $data['categories_of_story'] = DB::table('category_has_stories')->where('id_stories',$id)->pluck('id_category');

        return view('backend.story.edit', $data);
    }

    public function postEdit(EditStoryRequest $request, $id)
    {
        $story = Story::find($id);
        $story->name = $request->name;
        $story->slug = str_slug($request->name);
        $story->summary = $request->summary;
        $story->source = $request->source;
        $story->status = $request->status;
        $story->active = $request->active;
        $story->author = $request->author;
        $story->view = 0;

        if($request->hasFile('image'))
        {
            unlink('frontend/images/'.$story->image);
            $file = $request->image;
            $file_name = str_slug($request->image).'.'.$file->getClientOriginalExtension();
            $file->move('frontend/images',$file_name);
            $story->image = $file_name;
        }
        $story->save();

        $story->categories()->Sync($request->categories);

        return redirect(route('admin.get.list.story'))->with('success', 'Sửa truyện thành công')->withInput();
    }

    public function delete($id)
    {
        Story::destroy($id);
        return redirect(route('admin.get.list.story'))->with('success', 'Xóa truyện thành công')->withInput();
    }
}
