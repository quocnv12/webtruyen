<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use  App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('frontend.login');
    }
    public function postLogin(LoginRequest $request)
    {
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password], $request->remember)) 
        {
            return redirect(route('index'))->withInput();
        }
        else
        {
            return redirect(route('get.login.user'))->with('danger', 'Email hoặc mật khẩu không chính xác!');
        }
    }

    //đăng nhập từ modal
    public function postLoginModal(request $request)
    {
        $rules =  
        [
            'email'    =>  'required|email',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules,);

        if($validator->fails())
        {
            return redirect(route('get.login.user'));
        }


        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password], $request->remember)) 
        {
            return redirect(route('index'))->withInput();
        }
        else
        {
            return redirect(route('get.login.user'))->with('danger', 'Email hoặc mật khẩu không chính xác!');
        }


    }

    //đăng xuất
    public function getLogout()
    {
        Auth::logout();
        return redirect(route('index'));
    }

}
