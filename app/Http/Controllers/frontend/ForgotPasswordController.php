<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\{ForgotPasswordRequest, ResetPasswordRequest};
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    public function getForgotPassword()
    {
        return view('frontend.password.forgot_password');
    }
    public function postForgotPassword(ForgotPasswordRequest $request)
    {
        $email=$request->email;
        $check_email = User::where('email',$email)->first();
        if(!$check_email)
        {
            return redirect()->back()->with('danger', 'Không tìm thấy địa chỉ email!')->withInput();
        }
        $code = bcrypt(time().$email);
        $check_email->code =$code;
        $check_email->time_code =Carbon::now();
        $check_email->save();
        $url = route('link.reset.password',['code'=>$check_email->code,'email'=>$email]);
        $data=[
            'route' => $url,
        ];

        Mail::send('frontend.password.email_reset', $data, function($message) use ($email){
            $message->to($email, 'Reset Password')->subject('Reset Password Webtruyen!');
        });

        return redirect()->back()->with('success','Gửi mail thành công');
    }
    public function getResetPassword(request $request)
    {
        $code = $request->code;
        $email = $request->email;
       
        $check_email = User::where([
            'code' => $code,
            'email' => $email])->first();
        if(!$check_email)
        {
            return redirect()->back()->with('danger','Link lấy lại mật khẩu không chính xác!');   
        }
        return view('frontend.password.reset_passsword');
    }

    public function postResetPassword(ResetPasswordRequest $request)
    {
        // dd($request->all());
        if($request->password)
        {
            $code = $request->code;
            $email = $request->email;
            $check_email=User::where([
                'code'=>$code,
                'email'=>$email
            ])->first(); 
        }
        if(!$check_email)
        {
            dd(1);
           return redirect()->back()->with('danger','Link lấy lại mật khẩu không chính xác!')->withInput(); 
        }

        $check_email->password=bcrypt($request->password);
        $code = bcrypt(str::random(9));
        $check_email->code =$code;
        $check_email->save();

        return redirect(route('get.login.user'))->with('success','Lấy lại mật khẩu thành công');
    }
}
