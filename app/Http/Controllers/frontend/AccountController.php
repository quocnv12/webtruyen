<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\{RegisterRequest, UpdateInfoRequest, ChangerPasswordRequest};
use App\User;
use Validator;
use Hash;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function getRegister()
    {
        return view('frontend.register');
    }
    public function postRegister(RegisterRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->admin = 0;
        $user->save();
        return redirect(route('get.login.user'))->with('success', 'Đăng kí thành công ');
    }

    //đăng kí modal
    public function postRegisterModal(request $request)
    {
        $rules =  
        [
            'reg_email'     =>  'required|email|unique:users,email',
            'reg_password'  =>  'required|min:6|max:30',
            'passwordConfirmation'  =>  'required|same:password',
            'namedisplay' => 'required|min:4|max:30',
        ];

        $validator = Validator::make($request->all(), $rules,);

        if($validator->fails())
        {
            return redirect(route('get.register.user'));
        }


        $user = new User;
        $user->email = $request->reg_email;
        $user->password = bcrypt($request->reg_password);
        $user->name = $request->namedisplay;
        $user->gender = $request->gender;
        $user->admin = 0;
        $user->save();
        return redirect(route('get.login.user'))->with('success', 'Đăng kí thành công ');
    }

    //thông tin tài khoản
    public function getInfo($id)
    {
        $data['user'] = User::find($id);
        return view('frontend.info', $data);
    }
    public function postInfo(UpdateInfoRequest $request, $id)
    {
        $user = User::find($id);
        $user->email = $request->email;
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->save();
        return redirect()->back()->with('success', 'Cập nhật thông tin tài khoản thành công');
    }

    //đổi mật khẩu
    public function getChangerPassword()
    {
        return view('frontend.changer_password');
    }
    public function postChangerPassword(ChangerPasswordRequest $request)
    {
        if (Hash::check($request->password_old, Auth::user()->password))
        {
            Auth::user()->update([
                'password' => bcrypt($request->password)
            ]);
            return redirect()->back()->with('success','Đổi mật khẩu thành công !');
        }
        else 
        {
            return redirect()->back()->with('danger','Mật khẩu cũ không chính xác !');
        }
    }
}
