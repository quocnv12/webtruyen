<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\{Category, Story, Review, Chapter, ShortStory};
use App\User;
use DB;

class IndexController extends Controller
{
    public function getIndex()
    {
        $data['story_hot'] = Story::where('display', 1)->where('active', 2)->orderBy('id', 'DESC')->take(6)->get();
        $data['story_vips'] = Story::where('display', 1)->where('active', 1)->orderBy('id', 'DESC')->take(8)->get();
        $data['chapter_new'] = Chapter::orderBy('id', 'DESC')->take(30)->get();
        $data['story_full'] = Story::where('display', 1)->where('active', 3)->orderBy('id', 'DESC')->take(12)->get();
        $data['short_story'] = ShortStory::orderBy('id', 'DESC')->take(5)->get();

        return view('frontend.index', $data);
    }

    public function getCategory($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if(isset($category))
        {
            $stories = $category->stories()->paginate(30);
            return view('frontend.category', compact('category', 'stories'));
        }
        else
        {
        
            $story = Story::where('display', 1)->where('slug', $slug)->first();
            $cate = $story->categories()->get();
            $chapters = $story->chapters()->paginate(30);
            $chapter_new = $story->chapters()->orderBy('id', 'DESC')->take(5)->get();
            return view('frontend.story_detail', compact('story', 'cate', 'chapters', 'chapter_new'));
        }
       
    }

    public function getReview()
    {
        $data['review'] = Review::orderBy('id', 'DESC')->paginate(20);
        return view('frontend.review', $data);
    }

    public function getReviewDetail($slug)
    {
        $data['review'] = Review::where('slug', $slug)->first();
        return view('frontend.review_detail', $data);
    }

    
    public function getDetailChapter($slug_chapter,$slug)
    {
        // dd($slug);
        $data['chapter'] = Chapter::where('slug', $slug)->first();
        // dd($data['chapter']);
       return view('frontend.view', $data);
    }


    //tất cả truyện hay
    public function getAllStoryGood()
    {
        $data['all_story_good'] = Story::where('display', 1)->orderBy('view', 'DESC')->take(240)->paginate(30);
       return view('frontend.story_good', $data);
    }

    //tất cả truyện mới đăng
    public function getStoryNew()
    {
        $data['all_story_new'] = Story::where('display', 1)->orderBy('id', 'DESC')->take(240)->paginate(30);
       return view('frontend.story_new', $data);
    }
    //tất cả truyện full
    public function getStoryFull()
    {
        $data['all_story_full'] = Story::where('display', 1)->where('active', 3)->orderBy('id', 'DESC')->take(240)->paginate(30);
       return view('frontend.story_full', $data);
    }

    //tat cả truyện vip
    public function getStoryVip()
    {
        $data['all_story_vip'] = Story::where('display', 1)->where('active', 1)->orderBy('id', 'DESC')->take(240)->paginate(30);
       return view('frontend.story_vip', $data);
    }

    //tim kiem
    public function getSearch(request $request)
    {
        $data['key'] = $request->key;
        $data['search'] = Story::where('name', 'like' ,'%'.$request->key.'%')->paginate(30);
       return view('frontend.search', $data);
    }


    public function getShortStoryDetail($slug)
    {
        $data['short_story'] = ShortStory::where('slug', $slug)->first();
        return view('frontend.short_story_detail', $data);
    }

    public function getShortStory()
    {
        $data['short_story'] = ShortStory::paginate(20);
        return view('frontend.short_story', $data);
    }

    public function getIndexAdmin()
    {
        $data['slStory'] = count(Story::all());
        $data['user'] = count(User::all());
        return view('backend.index', $data);
    }
}
