<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'chapters';
    public function story()
    {
        return $this->belongsTo('App\models\Story', 'id_stories', 'id');
    }
}
