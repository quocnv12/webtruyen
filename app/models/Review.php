<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
}
