<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ShortStory extends Model
{
    protected $table = 'short_story';
}
