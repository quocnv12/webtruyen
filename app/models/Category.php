<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    public function stories()
    {
        return $this->belongsToMany('App\models\Story', 'category_has_stories', 'id_category', 'id_stories');
    }
}
