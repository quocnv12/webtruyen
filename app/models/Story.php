<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $table = 'stories';
    public function categories()
    {
        return $this->belongsToMany('App\models\Category', 'category_has_stories', 'id_stories', 'id_category');
    }
    public function chapters()
    {
        return $this->hasMany('App\models\Chapter', 'id_stories', 'id');
    }
}
