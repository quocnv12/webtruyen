<?php
function showErrors($errors, $name)
{
    if($errors->has($name))
    return '
        <div>
            <p class="error-input">'.$errors->first($name).'</p>
        </div>
    ';
}