@extends('frontend.master.master')
@section('title', 'Truyện mới đăng')
@section('content')
<div id="list" class="w3-col s12 m12 l8 w3-container">
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('get.story.new') }}" itemprop="name" class="active"> <span itemprop="name">Truyện Mới Đăng</span> </a> 
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="title-left">
                <h1><a title="Truyện mới đăng" href="{{ route('get.story.new') }}">Truyện mới đăng <i class="fa fa-angle-right"></i></a></h1>
            </div>
            <div class="title-right"><a title="Truyện Hay" href="{{ route('get.story.good') }}">Truyện Hay</a><i class="fa fa-circle"></i>
                <a title="Tiên full" href="{{ route('get.story.full') }}">Truyện Full</a><i class="fa fa-circle"></i>
                <a title="Tiên mới" href="{{ route('get.story.new') }}">Truyện Mới</a>
            </div>
        </div>

        <div class="w3-row list-content">
            @foreach ($all_story_new as $item)
            <div class="w3-row list-row-img">
                <div class="w3-col s2 m2 l2 row-image">
                    <div><a rel="nofollow" href="{{ $item->slug }}" title="{{ $item->name }}"><img src="frontend/images/{{ $item->image }}" alt="Vạn Cổ Thần Đế" width="55" height="70"/></a></div>
                </div>
                <div class="w3-col s7 m7 l7 row-info">
                    <div>
                        <a href="{{ $item->slug }}" title="{{ $item->name }}">{{ $item->name }}</a><span class="row-author"><i class="fa fa-user"></i> {{ $item->author }}</span>
                    </div>
                </div>
                <div class="w3-col s3 m3 l3 row-number">
                     @php
                     foreach ($item->chapters as $value){
                        if($loop->last)
                        {
                            $value;
                        }     
                     }
                    @endphp
                    <div><span class="row-chapter">{{ $value->name_chapter }}</span><span class="row-time"><i class="fa fa-history"></i>{{ Carbon\Carbon::parse($item->updated_at )->format('H:i  d-m-Y') }}</span></div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="w3-center pagination">
            <ul class="w3-pagination paging">
                {{ $all_story_new->links() }}
            </ul>
        </div>
    </div>
</div>
@endsection