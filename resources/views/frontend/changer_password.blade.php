@extends('frontend.master.master')
@section('title', 'Đổi mật khẩu')
@section('content')
<div id="list" class="w3-col s12 m12 l8">
    <!-- SITEMAP -->
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                    Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('get.changer.password') }}" itemprop="name" class="active"> <span itemprop="name">Đổi mật khẩu</span> </a>
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="w3-col s12 m6 l6 title-left">
                <h1><a href="{{ route('get.changer.password') }}">Đổi mật khẩu <i class="fa fa-angle-right"></i></a></h1>
            </div>
        </div>
        <form class="w3-container" name="frmAdd" id="frmAdd" action="{{ route('post.changer.password') }}" method="post" enctype="multipart-formdata">
                @csrf
                <p>
                    <label class="w3-label w3-text-grey"><b>Mật Khẩu Cũ</b> <i>(ít nhất 6 ký tự)</i></label>
                    <div class="form-group" style="position: relative;">
                        <input name="password_old" id="reg_password" class="w3-input w3-border" type="password" placeholder="********">
                        <a style="position: absolute;top: 15%;right: 15px;" href="javascript:;void(0)"><i class="fa fa-eye"></i></a>
                        {!! showErrors($errors,'password_old') !!}
                    </div>
                </p>
                <p><label class="w3-label w3-text-grey"><b>Mật Khẩu Mới</b> <i>(ít nhất 6 ký tự)</i></label>
                    <div class="form-group" style="position: relative;">
                        <input name="password" id="reg_password" class="w3-input w3-border" type="password" placeholder="********">
                        <a style="position: absolute;top: 15%;right: 15px;" href="javascript:;void(0)"><i class="fa fa-eye"></i></a>
                        {!! showErrors($errors,'password') !!}
                    </div>
                </p>
                <p><label class="w3-label w3-text-grey"><b>Nhập lại Mật Khẩu</b></label>
                    <div class="form-group" style="position: relative;">
                        <input name="password_confirmation" id="reg_repassword" class="w3-input w3-border" type="password" placeholder="********">
                        <a style="position: absolute;top: 15%;right: 15px;" href="javascript:;void(0)"><i class="fa fa-eye"></i></a>
                        {!! showErrors($errors,'password_confirmation') !!}
                    </div>
                </p>
            <p><button class="w3-btn w3-round w3-green" type="submit">Cập nhật</button> 
                <a class="w3-btn w3-round w3-grey w3-margin-left" type="reset" title="quay về"
                    href="{{ route('index') }}">Hủy</a></p>
        </form>
    </div>
</div>
@endsection
@section('js')
    <script>
        $(function () {
            $(".form-group a").click(function () {
                let $this = $(this);
                if ($this.hasClass('active')) {
                    $this.parents('.form-group').find('input').attr('type', 'password');
                    $this.removeClass('active');
                } else {
                    $this.parents('.form-group').find('input').attr('type', 'text');
                    $this.addClass('active');
                }

            });
        });
    </script>
@endsection
