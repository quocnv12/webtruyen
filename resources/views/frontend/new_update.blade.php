@extends('frontend.master.master')
@section('title','Đọc Truyện Online Hay Và Mới Nhất')
@section('content')
<div id="list" class="w3-col s12 m12 l8 w3-container">
<div class="w3-round sitemap" itemscope itemtype="https://schema.org/BreadcrumbList">
    <div itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" style="float: left;">
        <a href="https://webtruyen.com/" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang Chủ</span></a>
        <meta itemprop="position" content="1" />
        <i class="fa fa-angle-right"></i>
    </div>
    <div itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" style="float: left;">
        <a href="https://webtruyen.com/all/" itemprop="name" class="active"> <span itemprop="name">Tất cả</span> </a> 
        <meta itemprop="position" content="2" />
    </div>
    <div class="clear"></div>
</div>
<!-- LIST STORY -->
<div class="list-update">
    <div class="w3-row list-title">
        <div class="title-left">
            <h1><a title="Tất cả" href="https://webtruyen.com/all/">Tất cả <i class="fa fa-angle-right"></i></a></h1>
        </div>
        <div class="title-right"><a title="Truyện VIP" href="https://webtruyen.com/truyen-vip/">Truyện VIP</a><i class="fa fa-circle"></i><a title="Truyện Full" href="https://webtruyen.com/truyen-full/">Truyện Full</a><i class="fa fa-circle"></i><a title="Truyện Mới" href="https://webtruyen.com/truyen-moi-dang/">Truyện Mới</a><i class="fa fa-circle"></i><a title="Truyện hay" href="https://webtruyen.com/truyen-duoc-xem-nhieu-nhat/">Truyện hay</a><i class="fa fa-circle"></i></div>
    </div>
    <!--<div class="list-title"><div class="w3-center list-description"><p>Tất cả</p></div></div>-->
    <div class="w3-row list-content">
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/app-dia-nguc/" title="App Địa Ngục"><img src="frontend/images/medium/appdiangucUESIYTT8Im.jpg" alt="App Địa Ngục" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/app-dia-nguc/" title="App Địa Ngục">App Địa Ngục</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Tuyết Chi Vũ Thời Phong</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.6</span><span class="row-time"><i class="fa fa-history"></i>9 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/ma-than-hoang-thien/" title="Ma Thần Hoàng Thiên"><img src="frontend/images/medium/mathanhoangthieng5GEZE2DwM.jpg" alt="Ma Thần Hoàng Thiên" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/ma-than-hoang-thien/" title="Ma Thần Hoàng Thiên">Ma Thần Hoàng Thiên</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Đỉnh Kiên</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.420</span><span class="row-time"><i class="fa fa-history"></i>15 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/nu-vuong-mommy-gia-lam/" title="Nữ Vương Mommy Giá Lâm"><img src="frontend/images/medium/nuvuongmommygialamCI4IkahDLD.jpg" alt="Nữ Vương Mommy Giá Lâm" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/nu-vuong-mommy-gia-lam/" title="Nữ Vương Mommy Giá Lâm">Nữ Vương Mommy Giá Lâm</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Tề Thành Côn</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.492</span><span class="row-time"><i class="fa fa-history"></i>17 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/vo-chong-anh-hau-nguoc-cau-hang-ngay/" title="Vợ Chồng Ảnh Hậu Ngược Cẩu Hằng Ngày"><img src="frontend/images/medium/vochonganhhaunguoccauhangngayU6kD3rwfFP.jpg" alt="Vợ Chồng Ảnh Hậu Ngược Cẩu Hằng Ngày" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/vo-chong-anh-hau-nguoc-cau-hang-ngay/" title="Vợ Chồng Ảnh Hậu Ngược Cẩu Hằng Ngày">Vợ Chồng Ảnh Hậu Ngược Cẩu Hằng Ngày</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Bán Hạ Lương Lương</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.28</span><span class="row-time"><i class="fa fa-history"></i>19 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/phu-quan-truoc-cua-ta-la-quyen-than/" title="Phu Quân Trước Của Ta Là Quyền Thần"><img src="frontend/images/medium/phuquantruoccuatalaquyenthanrV9ha2P1LZ.jpg" alt="Phu Quân Trước Của Ta Là Quyền Thần" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/phu-quan-truoc-cua-ta-la-quyen-than/" title="Phu Quân Trước Của Ta Là Quyền Thần">Phu Quân Trước Của Ta Là Quyền Thần</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Cửu Lam</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.70</span><span class="row-time"><i class="fa fa-history"></i>19 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/cung-chieu-co-vo-nho-nham-hiem/" title="Cưng Chiều Cô Vợ Nhỏ Nham Hiểm"><img src="frontend/images/medium/cungchieucovonhonhamhiemm6StHHGwLK.jpg" alt="Cưng Chiều Cô Vợ Nhỏ Nham Hiểm" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/cung-chieu-co-vo-nho-nham-hiem/" title="Cưng Chiều Cô Vợ Nhỏ Nham Hiểm">Cưng Chiều Cô Vợ Nhỏ Nham Hiểm</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Vân Cẩn Nhân</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.32</span><span class="row-time"><i class="fa fa-history"></i>21 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/ban-trai-ngay-ngo-cua-toi/" title="Bạn Trai Ngây Ngô Của Tôi"><img src="frontend/images/medium/bantraingayngocuatoiRbjkimYQ6K.jpg" alt="Bạn Trai Ngây Ngô Của Tôi" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/ban-trai-ngay-ngo-cua-toi/" title="Bạn Trai Ngây Ngô Của Tôi">Bạn Trai Ngây Ngô Của Tôi</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Trọng Hi</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.21</span><span class="row-time"><i class="fa fa-history"></i>21 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/nghiem-tuc-ho-nhao/" title="Nghiêm Túc Hồ Nháo"><img src="frontend/images/medium/nghiemtuchonhao21Da7NBgjt.jpg" alt="Nghiêm Túc Hồ Nháo" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/nghiem-tuc-ho-nhao/" title="Nghiêm Túc Hồ Nháo">Nghiêm Túc Hồ Nháo</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Giải Tổng</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.29</span><span class="row-time"><i class="fa fa-history"></i>23 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/mau-xuyen-vai-ac-lai-hac-hoa/" title="Mau Xuyên: Vai Ác Lại Hắc Hóa"><img src="frontend/images/medium/mauxuyenvaiaclaihachoazRL3DJyIol.jpg" alt="Mau Xuyên: Vai Ác Lại Hắc Hóa" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/mau-xuyen-vai-ac-lai-hac-hoa/" title="Mau Xuyên: Vai Ác Lại Hắc Hóa">Mau Xuyên: Vai Ác Lại Hắc Hóa</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Lỏa Bôn Man Đầu</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.517</span><span class="row-time"><i class="fa fa-history"></i>24 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/ta-muon-lam-thien-dao/" title="Ta Muốn Làm Thiên Đao"><img src="frontend/images/medium/tamuonlamthiendaoKr4cTwIvte.jpg" alt="Ta Muốn Làm Thiên Đao" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/ta-muon-lam-thien-dao/" title="Ta Muốn Làm Thiên Đao">Ta Muốn Làm Thiên Đao</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> DD Lão huynh</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.280</span><span class="row-time"><i class="fa fa-history"></i>26 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/tai-ha-khong-phai-la-nu/" title="Tại Hạ Không Phải Là Nữ"><img src="frontend/images/medium/taihakhongphailanuYHCVGymQ9T.jpg" alt="Tại Hạ Không Phải Là Nữ" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/tai-ha-khong-phai-la-nu/" title="Tại Hạ Không Phải Là Nữ">Tại Hạ Không Phải Là Nữ</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Nguyệt Hạ Tiểu Dương</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.873</span><span class="row-time"><i class="fa fa-history"></i>26 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/ban-trai-ky-la-cua-toi/" title="Bạn Trai Kỳ Lạ Của Tôi "><img src="frontend/images/medium/bantraikylacuatoiuoURFQz7nh.jpg" alt="Bạn Trai Kỳ Lạ Của Tôi " width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/ban-trai-ky-la-cua-toi/" title="Bạn Trai Kỳ Lạ Của Tôi ">Bạn Trai Kỳ Lạ Của Tôi </a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Kim Tử Tựu Thị Sao Phiếu</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.190</span><span class="row-time"><i class="fa fa-history"></i>27 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/yeu-ban-than/" title="Yêu Bản Thân ..."><img src="frontend/images/medium/yeubanthanUkgNshJGX1.jpg" alt="Yêu Bản Thân ..." width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/yeu-ban-than/" title="Yêu Bản Thân ...">Yêu Bản Thân ...</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> aptrungmochi</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.66</span><span class="row-time"><i class="fa fa-history"></i>30 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/len-giuong-voi-toi-thi-phai-cuoi-toi/" title=" Lên Giường Với Tôi Thì Phải Cưới Tôi"><img src="frontend/images/medium/lengiuongvoitoithiphaicuoitoiigpMBorhnP.jpg" alt=" Lên Giường Với Tôi Thì Phải Cưới Tôi" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/len-giuong-voi-toi-thi-phai-cuoi-toi/" title=" Lên Giường Với Tôi Thì Phải Cưới Tôi"> Lên Giường Với Tôi Thì Phải Cưới Tôi</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Chó Sủa</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.36</span><span class="row-time"><i class="fa fa-history"></i>30 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/su-thuong-de-nhat-mat-tham/" title="Sử Thượng Đệ Nhất Mật Thám"><img src="frontend/images/medium/suthuongdenhatmattham1eaZhA3STZ.jpg" alt="Sử Thượng Đệ Nhất Mật Thám" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/su-thuong-de-nhat-mat-tham/" title="Sử Thượng Đệ Nhất Mật Thám">Sử Thượng Đệ Nhất Mật Thám</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Cao Điểm Trầm Mặc</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.163</span><span class="row-time"><i class="fa fa-history"></i>31 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/lan-kha-ki-duyen/" title="Lạn Kha Kì Duyên"><img src="frontend/images/medium/lankhakiduyenA3iyLLqvdf.jpg" alt="Lạn Kha Kì Duyên" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/lan-kha-ki-duyen/" title="Lạn Kha Kì Duyên">Lạn Kha Kì Duyên</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Chân Phí Sự</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.186</span><span class="row-time"><i class="fa fa-history"></i>34 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/quy-nu-yeu-kieu/" title="Quý Nữ Yêu Kiều"><img src="frontend/images/medium/quynuyeukieuNapGBir3vG.jpg" alt="Quý Nữ Yêu Kiều" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/quy-nu-yeu-kieu/" title="Quý Nữ Yêu Kiều">Quý Nữ Yêu Kiều</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Thập Nguyệt Vi Vi Lương</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.142</span><span class="row-time"><i class="fa fa-history"></i>45 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/am-hon-luc-nua-dem/" title="Âm Hôn Lúc Nửa Đêm"><img src="frontend/images/medium/amhonlucnuademcJfR4ZbpPa.jpg" alt="Âm Hôn Lúc Nửa Đêm" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/am-hon-luc-nua-dem/" title="Âm Hôn Lúc Nửa Đêm">Âm Hôn Lúc Nửa Đêm</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Mộ Hi Ngôn</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.85</span><span class="row-time"><i class="fa fa-history"></i>47 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/dien-tinh-xuyen-vao-kich-kho-tinh/" title="Diễn Tinh Xuyên Vào Kịch Khổ Tình"><img src="frontend/images/medium/dientinhxuyenvaokichkhotinhN2zXB24r2o.jpg" alt="Diễn Tinh Xuyên Vào Kịch Khổ Tình" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/dien-tinh-xuyen-vao-kich-kho-tinh/" title="Diễn Tinh Xuyên Vào Kịch Khổ Tình">Diễn Tinh Xuyên Vào Kịch Khổ Tình</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Phù Hoa</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.57</span><span class="row-time"><i class="fa fa-history"></i>52 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/cung-chieu-vo-yeu-phuc-hac-de-thuong/" title="Cưng Chiều Vợ Yêu Phúc Hắc Dễ Thương"><img src="frontend/images/medium/cungchieuvoyeuphuchacdethuongw5lmTY17T5.jpg" alt="Cưng Chiều Vợ Yêu Phúc Hắc Dễ Thương" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/cung-chieu-vo-yeu-phuc-hac-de-thuong/" title="Cưng Chiều Vợ Yêu Phúc Hắc Dễ Thương">Cưng Chiều Vợ Yêu Phúc Hắc Dễ Thương</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Tiên Nhược An Nhiên</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.230</span><span class="row-time"><i class="fa fa-history"></i>54 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/xuyen-den-truoc-khi-dai-lao-hac-hoa/" title="Xuyên Đến Trước Khi Đại Lão Hắc Hóa"><img src="frontend/images/medium/xuyendentruockhidailaohachoad9b7tY147s.jpg" alt="Xuyên Đến Trước Khi Đại Lão Hắc Hóa" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/xuyen-den-truoc-khi-dai-lao-hac-hoa/" title="Xuyên Đến Trước Khi Đại Lão Hắc Hóa">Xuyên Đến Trước Khi Đại Lão Hắc Hóa</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Cẩm Chanh</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.10</span><span class="row-time"><i class="fa fa-history"></i>56 phút trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/tu-chan-tu-van-nien/" title="Tu Chân Tứ Vạn Niên"><img src="frontend/images/medium/tuchantuvannienGYIi1tj3Sr.jpg" alt="Tu Chân Tứ Vạn Niên" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/tu-chan-tu-van-nien/" title="Tu Chân Tứ Vạn Niên">Tu Chân Tứ Vạn Niên</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><span class="vip-icon"></span><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Ngọa Ngưu Chân Nhân</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.2837</span><span class="row-time"><i class="fa fa-history"></i>2 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/kiem-lai/" title="Kiếm Lai"><img src="frontend/images/medium/kiemlaiqdiNZJfMFB.jpg" alt="Kiếm Lai" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/kiem-lai/" title="Kiếm Lai">Kiếm Lai</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><span class="vip-icon"></span><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Phong Hỏa Hí Chư Hầu</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.639</span><span class="row-time"><i class="fa fa-history"></i>2 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/quy-bi-chi-chu/" title="Quỷ Bí Chi Chủ"><img src="frontend/images/medium/quybichichuB9aTQCscJE.jpg" alt="Quỷ Bí Chi Chủ" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/quy-bi-chi-chu/" title="Quỷ Bí Chi Chủ">Quỷ Bí Chi Chủ</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><span class="vip-icon"></span><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Mực Thích Lặn Nước</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.380</span><span class="row-time"><i class="fa fa-history"></i>2 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/tho-tinh-trong-gio/" title="Thơ Tình Trong Gió"><img src="frontend/images/medium/thotinhtronggiopRk97VuVML.jpg" alt="Thơ Tình Trong Gió" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/tho-tinh-trong-gio/" title="Thơ Tình Trong Gió">Thơ Tình Trong Gió</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Tống Cửu Cận</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.63</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/xuyen-nhanh-nam-than-bung-chay-di/" title="Xuyên Nhanh: Nam Thần, Bùng Cháy Đi"><img src="frontend/images/medium/xuyennhanhnamthanbungchaydiXpvttVxiFi.jpg" alt="Xuyên Nhanh: Nam Thần, Bùng Cháy Đi" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/xuyen-nhanh-nam-than-bung-chay-di/" title="Xuyên Nhanh: Nam Thần, Bùng Cháy Đi">Xuyên Nhanh: Nam Thần, Bùng Cháy Đi</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Mặc Linh</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.2090</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/khoang-troi-rong-lon/" title="Khoảng Trời Rộng Lớn"><img src="frontend/images/medium/khoangtroironglon3oAbEBYDcs.jpg" alt="Khoảng Trời Rộng Lớn" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/khoang-troi-rong-lon/" title="Khoảng Trời Rộng Lớn">Khoảng Trời Rộng Lớn</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Dơi Trắng</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.15</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/nam-chu-deu-la-xa-tinh-benh/" title="Nam Chủ Đều Là Xà Tinh Bệnh"><img src="frontend/images/medium/namchudeulaxatinhbenhdmDCVt9DVK.jpg" alt="Nam Chủ Đều Là Xà Tinh Bệnh" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/nam-chu-deu-la-xa-tinh-benh/" title="Nam Chủ Đều Là Xà Tinh Bệnh">Nam Chủ Đều Là Xà Tinh Bệnh</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Cật Hoàng Qua Đích Cúc Hoa</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.83</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/tong-tai-qua-tan-nhan/" title="Tổng Tài Quá Tàn Nhẫn"><img src="frontend/images/medium/tongtaiquatannhanWtiryaen8c.jpg" alt="Tổng Tài Quá Tàn Nhẫn" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/tong-tai-qua-tan-nhan/" title="Tổng Tài Quá Tàn Nhẫn">Tổng Tài Quá Tàn Nhẫn</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Lục Thiểu</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.192</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/he-thong-tho-san-tai-di-gioi/" title="Hệ Thống: Thợ Săn Tại Dị Giới "><img src="frontend/images/medium/hethongthosantaidigioiFPiqVYBrlY.jpg" alt="Hệ Thống: Thợ Săn Tại Dị Giới " width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/he-thong-tho-san-tai-di-gioi/" title="Hệ Thống: Thợ Săn Tại Dị Giới ">Hệ Thống: Thợ Săn Tại Dị Giới </a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Mạc Vô Danh</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.103</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/huyen-linh-ky/" title="Huyền Linh Ký"><img src="frontend/images/medium/huyenlinhkyXhVcRlNo4s.jpg" alt="Huyền Linh Ký" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/huyen-linh-ky/" title="Huyền Linh Ký">Huyền Linh Ký</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> htdlht</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.117</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
        <div class="w3-row list-row-img">
            <div class="w3-col s2 m2 l2 row-image">
                <div><a rel="nofollow" href="https://webtruyen.com/nu-quan-van-su/" title="Nữ Quan Vận Sự"><img src="frontend/images/medium/nuquanvansuMMyuPdXbXe.jpg" alt="Nữ Quan Vận Sự" width="55" height="70"/></a></div>
            </div>
            <div class="w3-col s7 m7 l7 row-info">
                <div>
                    <a href="https://webtruyen.com/nu-quan-van-su/" title="Nữ Quan Vận Sự">Nữ Quan Vận Sự</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Tiểu Nhục Tống</span>
                </div>
            </div>
            <div class="w3-col s3 m3 l3 row-number">
                <div><span class="row-chapter">C.318</span><span class="row-time"><i class="fa fa-history"></i>10 giờ trước</span></div>
            </div>
        </div>
    </div>
    <!-- PAGES -->
    <div class="w3-center pagination">
        <ul class="w3-pagination paging">
            <li><a class='active'>1</a></li>
            <li><a title='page-2' href='https://webtruyen.com/all/2/'>2</a></li>
            <li><a title='page-3' href='https://webtruyen.com/all/3/'>3</a></li>
            <li><a title='page-4' href='https://webtruyen.com/all/4/'>4</a></li>
            <li><a title='page-5' href='https://webtruyen.com/all/5/'>5</a></li>
            <li><a href='https://webtruyen.com/all/623/' title='page-623' class='last'><i class='fa fa-angle-double-right'></i></a></li>
        </ul>
        <div class="w3-center">
            <form class="pages" method="GET" action="https://webtruyen.com/home/gopage_listbook">
                <span class="w3-tag w3-round numbpage">Trang 1 / 623</span><label for="gopage">GO </label><input class="w3-input w3-border" type="text" placeholder="Tới trang" name='gopage' id='gopage'/>
                <button class="w3-btn w3-round w3-hover-green" value="go" id="gopage_listbook" name="gopage_listbook">
                    <!--<i class="fa fa-arrow-right"></i>-->GO
                </button>
                <input type='hidden' name='hdkey' id='hdkey' value='all'/>
            </form>
        </div>
    </div>
</div>
</div>
@endsection