@extends('frontend.master.master_login')
@section('title', 'Lấy lại mật khẩu')
@section('content_login')
<div id="list" class="w3-col s12 m12 l8">
    <!-- SITEMAP -->
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                    Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="javascript:;void(0)" itemprop="name" class="active"> <span itemprop="name">Lấy lại mật khẩu</span> </a>
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="w3-col s12 m6 l6 title-left">
                <h1><a href="javascript:;void(0)">Lấy lại mật khẩu <i class="fa fa-angle-right"></i></a></h1>
            </div>
        </div>
        <form class="w3-container" name="frmAdd" id="frmAdd" action="" method="post" enctype="multipart-formdata">
                @csrf
            <p><label class="w3-label w3-text-grey"><b>Mật Khẩu</b> <i>(ít nhất 6 ký tự)</i></label>
                <input name="password" id="reg_password" class="w3-input w3-border" value="" type="password" placeholder="********">
                {!! showErrors($errors,'password') !!}
            </p>
            <p><label class="w3-label w3-text-grey"><b>Nhập lại Mật Khẩu</b></label>
                <input name="passwordConfirmation" id="reg_repassword" class="w3-input w3-border" value="" type="password" placeholder="********">
                {!! showErrors($errors,'passwordConfirmation') !!}
            </p>
            <p><button class="w3-btn w3-round w3-green" type="submit">Xác nhận</button> 
                <a class="w3-btn w3-round w3-grey w3-margin-left" type="reset" title="quay về"
                    href="{{ route('index') }}">Hủy</a></p>
        </form>
    </div>
</div>
@endsection
