@extends('frontend.master.master')
@section('title', 'Quên mật khẩu')
@section('content')
<div id="list" class="w3-col s12 m12 l8">
    <!-- SITEMAP -->
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                    Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('get.forgot.password') }}" itemprop="name" class="active"> <span itemprop="name">Quên mật khẩu</span> </a>
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="w3-col s12 m6 l6 title-left">
                <h1><a href="{{ route('get.forgot.password') }}">Quên mật khẩu <i class="fa fa-angle-right"></i></a></h1>
            </div>
        </div>
        <form class="w3-container" name="frmAdd" id="frmAdd" action="" method="post" enctype="multipart-formdata">
                @csrf
            <p><label class="w3-label w3-text-grey"><b>Email</b> <i>(Nhập email để lấy lại mật khẩu)</i></label>
                <input name="email" id="reg_email" class="w3-input w3-border" type="text" placeholder="youremail@gmail.com" value="">
                {!! showErrors($errors,'email') !!}
            </p>
            <p><button class="w3-btn w3-round w3-green" type="submit">Gửi mail</button> 
                <a class="w3-btn w3-round w3-grey w3-margin-left" type="reset" title="quay về"
                    href="{{ route('index') }}">Hủy</a>
            </p>
        </form>
    </div>
</div>
@endsection
