@extends('frontend.master.master_view')
@section('title' , $chapter->name)
@section('content_view')
<div class="w3-round sitemap" itemscope itemtype="">
    <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
        <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                Chủ</span></a>
        <meta itemprop="position" content="1" />
        <i class="fa fa-angle-right"></i>
    </div>
    <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
        <a href="{{ $chapter->story->slug }}" itemscope itemtype="" itemprop="item" itemid=""><span
                itemprop="name">{{ $chapter->story->name }}</span></a>
        <meta itemprop="position" content="3" />
        <i class="fa fa-angle-right"></i>
    </div>
    <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
        <a href="" itemprop="name" class="active"> <span
                itemprop="name">{{ $chapter->name }}</span> </a>
        <meta itemprop="position" content="4" />
    </div>
    <div class="clear"></div>
</div>
<div id="reading">
    <!-- TITLE -->
    <div class="w3-row w3-center chapter-header">
        <div class="w3-row w3-round">
            <h1><a href="{{ $chapter->story->slug }}/{{ $chapter->slug }}" tittle="{{ $chapter->name }}"
                    class="w3-text-red w3-hover-text-grey">{{ $chapter->name }}</a></h1>
            <p>Truyện: <a href="{{ $chapter->story->slug }}" tittle="{{ $chapter->story->name }}"
                    class="w3-hover-text-grey">{{ $chapter->story->name }}</a></p>
            <p><i class="fa fa-user"></i><a title="{{ $chapter->story->author }}" href="javacrip::voi(0)">
                    {{ $chapter->story->author }}</a></p>
            <p><i class="fa fa-clock-o w3-margin-left"></i>
                {{ Carbon\Carbon::parse($chapter->story->created_at )->format('d/m/Y') }}</p>
        </div>
    </div>
    @php
        $slug_chapter_next = \App\models\Chapter::where('id_stories', $chapter->story->id)->where('number_chapter',
        $chapter->number_chapter +1 )->first();
        $slug_chapter_after = \App\models\Chapter::where('id_stories', $chapter->story->id)->where('number_chapter',
        $chapter->number_chapter - 1 )->first();
        $slug_chapter_big = \App\models\Chapter::where('id_stories', $chapter->story->id)->orderBy('number_chapter', 'DESC')->first();
    @endphp
    <div class="w3-center chapter-button">
        @if($chapter->number_chapter > 1)
            <a id='nextchap' class='w3-btn w3-round w3-blue w3-hover-white' title='Tam Quỷ Gia (hạ)'
            href='{{ $chapter->story->slug }}/{{ $slug_chapter_after->slug }}'><i class='fa fa-chevron-left'></i> <span class='w3-hide-small'>Chương</span> trước
            </a>
        @endif
        <a class="w3-btn w3-round w3-grey w3-hover-white" name="loadListChap"
            id="loadListChap" onclick="loadListChap()"><i class="fa fa-list"></i> <span class='w3-hide-small'>DS Chương</span></a>
        @if ($chapter->number_chapter < $slug_chapter_big->number_chapter)
                <a id='nextchap' class='w3-btn w3-round w3-blue w3-hover-white' title='Tam Quỷ Gia (hạ)'
                    href='{{ $chapter->story->slug }}/{{ $slug_chapter_next->slug }}'> <span class='w3-hide-small'>Chương</span> Sau
                    <i class='fa fa-chevron-right'></i>
                </a>
        @endif
        

    </div>
    <!-- CHAPTER BOX -->
    <div id="list-drop" class="w3-hide w3-center w3-animate-opacity">
        <p>
            Danh Sách Chương:
            <select class="w3-border" name="option">
                <option value="1">Chương 1</option>
                <option value="2">Chương 2</option>
            </select>
        </p>
    </div>
    <style>
        .text-webtruyen {
            display: flex;
            flex-direction: row;
            font-size: 16px;
            color: slategray;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .text-webtruyen:before,
        .text-webtruyen:after {
            content: "";
            flex: 1 1;
            border-bottom: 1px solid #cfcbcb;
            margin: auto;
        }

    </style>
    <!-- CONTENT -->
    <div id="content" class="w3-justify chapter-content detailcontent">
        <div id="divcontent">
            {!! $chapter->content !!}

            <p class="text-webtruyen">Bạn đang đọc truyện trên: <span
                    style="color: blue; font-weight: bold;">Webtruyen.com</span></p>
        </div>
        <!--</p>-->
    </div>
    <!-- ADS CHAPTER 2 -->
    <div class="w3-row w3-center ads-chapter-2" id="adscontenttren">
        <div class="w3-col s12 m6 l6 adsleftcontent" style="margin-right:4px;"></div>
        <div class="w3-col s12 m6 l6 adsrightcontent w3-hide-small" style="margin-left:4px;"></div>
    </div>
    <!-- BOTTOM BUTTON -->
    <div class="w3-center chapter-button">
        @if($chapter->number_chapter > 1)
            <a id='nextchap' class='w3-btn w3-round w3-blue w3-hover-white' title='Tam Quỷ Gia (hạ)'
            href='{{ $chapter->story->slug }}/{{ $slug_chapter_after->slug }}'><i class='fa fa-chevron-left'></i> <span class='w3-hide-small'>Chương</span> trước
            </a>
        @endif
        <a class="w3-btn w3-round w3-grey w3-hover-white" name="loadListChap"
            id="loadListChap" onclick="loadListChap()"><i class="fa fa-list"></i> <span class='w3-hide-small'>DS Chương</span></a>
        
            @if ($chapter->number_chapter < $slug_chapter_big->number_chapter)
                    <a id='nextchap' class='w3-btn w3-round w3-blue w3-hover-white' title='Tam Quỷ Gia (hạ)'
                    href='{{ $chapter->story->slug }}/{{ $slug_chapter_next->slug }}'> <span class='w3-hide-small'>Chương</span> Sau
                    <i class='fa fa-chevron-right'></i>
                    </a>
            @endif
    </div>


    <!-- HOT STORY -->
    <div class="w3-row recommend">
        <div class="w3-row recommend-header">
            <h4>TRUYỆN TIÊN HIỆP HOT</h4>
        </div>
        <div class="w3-row recommend-content">
            <div class="w3-col s12 m6 l4 recommend-list">
                <ul class="w3-ul">
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/doc-bo-thien-ha/"
                                title="Độc Bộ Thiên Hạ">Độc Bộ Thiên Hạ</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/bach-luyen-thanh-tien/"
                                title="Bách Luyện Thành Tiên">Bách Luyện Thành Tiên</a></p>
                        <span class="hot-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/tuong-da/" title="Tướng Dạ">Tướng
                                Dạ</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/duoc-than/" title="Dược Thần">Dược
                                Thần</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/linh-vuc/" title="Linh Vực">Linh
                                Vực</a></p>
                        <span class="full-tag"></span>
                    </li>
                </ul>
            </div>
            <div class="w3-col s12 m6 l4 w3-hide-medium w3-hide-small recommend-list">
                <ul class="w3-ul">
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/vu-cuc-thien-ha/"
                                title="Vũ Cực Thiên Hạ">Vũ Cực Thiên Hạ</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/bac-tong-phong-luu/"
                                title="Bắc Tống Phong Lưu">Bắc Tống Phong Lưu</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/than-khong-thien-ha/"
                                title="Thần Khống Thiên Hạ">Thần Khống Thiên Hạ</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/cuu-tinh-thien-than-quyet/"
                                title="Cửu Tinh Thiên Thần Quyết">Cửu Tinh Thiên Thần Quyết</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/trieu-hoan-than-binh/"
                                title="Triệu Hoán Thần Binh">Triệu Hoán Thần Binh</a></p>
                        <span class="full-tag"></span>
                    </li>
                </ul>
            </div>
            <div class="w3-col s12 m6 l4 w3-hide-small recommend-list">
                <ul class="w3-ul">
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/linh-vu-thien-ha/"
                                title="Linh Vũ Thiên Hạ">Linh Vũ Thiên Hạ</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/ngu-hanh-thien/"
                                title="Ngũ Hành Thiên">Ngũ Hành Thiên</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/doc-ton-tam-gioi/"
                                title="Độc Tôn Tam Giới">Độc Tôn Tam Giới</a></p>
                        <span class="full-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/vo-dich-kiem-vuc/"
                                title="Vô Địch Kiếm Vực">Vô Địch Kiếm Vực</a></p>
                        <span class="hot-tag"></span>
                    </li>
                    <li>
                        <i class="fa fa-chevron-right"></i>
                        <p class="recommend-title"><a href="https://webtruyen.com/toi-cuong-than-thoai-de-hoang/"
                                title="Tối Cường Thần Thoại Đế Hoàng">Tối Cường Thần Thoại Đế Hoàng</a></p>
                        <span class="full-tag"></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="w3-row w3-round chapter-notification">
        <div class="w3-col s12 m12 l8 w3-center"><span>Bạn đang đọc truyện trên website <b>WebTruyen.Com</b></span>
        </div>
    </div>
</div>
<!-- COMMENT -->
<div class="comments">
    <!--<div class="w3-row w3-center comment-title"><a onclick="openTab(event, 'comment');"><div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><i class="fa fa-comments"></i> Bình Luận</div></a><a onclick="openTab(event, 'facebook');"><div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><i class="fa fa-facebook-square"></i> Facebook</div></a></div>-->
    <div class="comment-member">
        <div class="comment-heading">
            <i class="fa fa-comments"></i>
            <h3>Nhận xét của đọc giả về truyện {{ $chapter->story->name }}</h3>
        </div>
        <div class="comment-content">
            <form class="w3-row comment-form" style="margin: 15px 0;" name="formcm" action="/method/comment"
                method="post" enctype="multipart/form-data" onsubmit="return checkvailcm()">
                <div class="w3-col s12 m12 l12 comment-text"><textarea onclick="openModal('login')" id="txtcm"
                        name="txtcm" class="w3-round"
                        placeholder="Nội dung bình luận tối thiểu 15 ký tự, tối đa 500 ký tự!"></textarea></div>
                <div class="comment">
                    <div class="w3-col s12 m6 l6 comment-count"><label class="character" for="txtcm">Số ký tự:
                        </label><label id="ccm" class="number">0</label></div>
                    <div class="w3-col s12 m6 l6 comment-button"><button disabled="disabled" type="button" id="btcm"
                            name="btcm" onclick="" class="w3-btn button-send"><i class="fa fa-send"></i> Gửi</button>
                    </div>
                    <div id="diverro" class="w3-col s12 m12 l12 comment-error"><label id="lberro"></label></div>
                </div>
                <p style="display:none;"><span></span><input id="notinput" name="notinput" type="text"
                        class="form-control"></p>
                <input type="hidden" name="storyid-hidden" id="storyid-hidden" value="6408" /> <input type="hidden"
                    name="comment-cate" id="comment-cate" value="0" /> <input type="hidden" name="actual_link"
                    id="actual_link" value="https://webtruyen.com/de-ba/tam-quy-gia-thuong_612706.html" />
            </form>
            <input type="hidden" id="comment-flag" value="true" /> <input type="hidden" id="comment-page" value="1" />

            <div id="comment-list">
                <div class="comment-row" id="comment_74723">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Trần Lương</a><span
                            class="comment-date">21:04 25/04/2020</span>
                        <div class="comment-chat">Viết truyện dài dòng vừa thôi lặp đi lặp lại làm lozz j </div>
                    </div>
                </div>
                <div class="comment-row" id="comment_70392">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Đức Mạnh</a><span
                            class="comment-date">10:02 15/02/2020</span>
                        <div class="comment-chat">Chuyện dài dòng khó chịu có nhiều chi tiết lặp đi lặp lại nhiều lần.
                            Nếu có thể viết gọn lại thì sẽ hay hơn nhiều</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_68334">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">You Mun</a><span
                            class="comment-date">10:12 26/12/2019</span>
                        <div class="comment-chat">Hồi kí là nhìu.....tình tiếc truyện k hấp dẫn những hay.....</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_67338">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Nguyễn doãn tân</a><span
                            class="comment-date">15:11 28/11/2019</span>
                        <div class="comment-chat">Hon 4k van chua da</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_63286">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">ĐẠI ĐẾ</a><span
                            class="comment-date">02:08 11/08/2019</span>
                        <div class="comment-chat">Tác giả ơi em đọc ht r cx gần 1 năm r mà sao h vẫn chưa ra chương mới
                            vậy ra nhanh đi chứ k ra nữa thì buồn lắm á</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_60064">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Trần Danh Hoàn</a><span
                            class="comment-date">14:06 26/06/2019</span>
                        <div class="comment-chat">Mới đọc mấy chương đầu thấy truyện mở đầu khá hay và hấp dẫn</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_58260">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Heo sua</a><span
                            class="comment-date">20:06 19/06/2019</span>
                        <div class="comment-chat">Sao không ra chương tiếp vậy mình theo dõi từ đầu tới giờ hơi hụt hẫng
                            quá</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_56349">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Anh Trung</a><span
                            class="comment-date">19:06 07/06/2019</span>
                        <div class="comment-chat">không ra chương mới nữa ư ad???</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_48408">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Admin</a><span
                            class="comment-date">02:03 26/03/2018</span>
                        <div class="comment-chat">ad đã update lại tới chương mới nhất rồi, các bạn xem ủng hộ nhe</div>
                    </div>
                </div>
                <div class="comment-row" id="comment_47758">
                    <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                            src="https://webtruyen.com/public/frontend/mobile/img/avatar.png"></div>
                    <div class="comment-right">
                        <a class="comment-user" href="#" rel="nofollow" title="">Binh Phan</a><span
                            class="comment-date">12:10 20/10/2017</span>
                        <div class="comment-chat">Lâu rồi không được cập nhật chương mới, không biết có vấn đề gì không
                            ta?</div>
                    </div>
                </div>
            </div>
            <div id="lastPostsLoader"></div>
            <div class="w3-center comment-more"><button id="loadMoreComment" onclick="loadListComment()"
                    class="w3-btn w3-round-xlarge w3-grey w3-hover-white"><i class="fa fa-chevron-down"></i> Xem
                    thêm</button></div>
        </div>
    </div>
</div>
</div>
@endsection
