@extends('frontend.master.master_view')
@section('title', $review->title)
@section('content_view')
<div id="reading" style="background: rgb(255, 255, 255); color: rgb(51, 51, 51);">
    <!-- TITLE -->
    <div class="w3-row w3-center chapter-header">
        <ul class="w3-ul">
            <li>
                <h1><a title="{{ $review->name }}"
                        href="{{ $review->slug }}"
                        class="w3-text-red w3-hover-text-grey">{{ $review->title }}</a></h1>
            </li>
            <li><i class="fa fa-user"></i> {{ $review->author }}<i class="fa fa-clock-o w3-margin-left"></i>
                {{ Carbon\Carbon::parse($review->created_at )->format('d-m-Y H:i:s') }}</li>
        </ul>
    </div>

    <div id="content" class="w3-justify shortstory-content reviews-content">
      {!! $review->content !!}
    </div>
</div>
@endsection
