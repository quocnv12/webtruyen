@extends('frontend.master.master_login')
@section('title', 'Đăng ký')
@section('content_login')
<div id="list" class="w3-col s12 m12 l8">
    <!-- SITEMAP -->
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                    Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('get.register.user') }}" itemprop="name" class="active"> <span itemprop="name">Đăng
                    ký</span> </a>
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="w3-col s12 m6 l6 title-left">
                <h1><a href="{{ route('get.register.user') }}">Đăng nhập <i class="fa fa-angle-right"></i></a></h1>
            </div>
        </div>
        <form class="w3-container" name="frmAdd" id="frmAdd" action="{{ route('post.register.user') }}" method="post" enctype="multipart-formdata">
                @csrf
            <p>
                <label class="w3-label w3-text-grey"><b>Email</b> <i>(bắt buộc)</i></label>
                <input name="email" id="reg_email" class="w3-input w3-border" type="text" placeholder="youremail@gmail.com" value="">
                {!! showErrors($errors,'email') !!}
            </p>
            <p>
                <label class="w3-label w3-text-grey"><b>Mật Khẩu</b> <i>(ít nhất 6 ký tự)</i></label>
                <input name="password" id="reg_password" class="w3-input w3-border" type="password" placeholder="********">
                {!! showErrors($errors,'password') !!}
            </p>
            <p>
                <label class="w3-label w3-text-grey"><b>Nhập lại Mật Khẩu</b></label>
                <input name="passwordConfirmation" id="reg_repassword" class="w3-input w3-border" type="password" placeholder="********">
                {!! showErrors($errors,'passwordConfirmation') !!}
            </p>
            <p>
                <label class="w3-label w3-text-grey"><b>Tên hiển thị</b> <i>(bắt buộc)</i></label>
                <input  name="name" id="namedisplay" class="w3-input w3-border" type="text" placeholder="Tên hiển thị" value="">
                {!! showErrors($errors,'name') !!}
            </p>
            <p>
                <input class="w3-radio" type="radio" name="gender" value="1" checked=""><label class="w3-validate w3-margin-right">Nam</label>
                <input class="w3-radio" type="radio" name="gender" value="2"><label class="w3-validate w3-margin-right">Nữ</label>
            </p>
            {!! showErrors($errors,'gender') !!}
            <p><button class="w3-btn w3-round w3-green" type="submit">Đăng kí</button> 
                <a class="w3-btn w3-round w3-grey w3-margin-left" type="reset" title="quay về"
                    href="{{ route('index') }}">Hủy</a></p>
        </form>
    </div>
</div>
@endsection
