@extends('frontend.master.master')
@section('title', $story->name)
@section('content')

<div id="list" class="w3-col s12 m12 l8 w3-container">

    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                    Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ $story->slug }}" itemprop="name" class="active"> <span itemprop="name">{{ $story->name }}</span> </a>
            <meta itemprop="position" content="3" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- DETAIL -->
    <div class="w3-row detail">
        <div class="w3-col m4 l4 detail-left">
            <input type="hidden" id="hiddenID" value="15793" /> <input type="hidden" id="hiddenKey"
                value="{{ $story->name }}" /> <input type="hidden" id="hiddenCateRate" value="0" />
            <div class="w3-col s4 m12 l12 detail-thumbnail"> <span class="{{ $story->active == 1 ? 'vip-label' : 'hot-icon' }}"></span>
                <img width="228" height="270" alt="{{ $story->name }}" src="frontend/images/{{ $story->image }}"> </div>
            <div class="w3-col s8 m12 l12 detail-info">
                <ul class="w3-ul">
                    <li><i class="fa fa-user"></i> <a href="javascrip::voi(0)"
                            title="{{ $story->author }}">{{ $story->author }}</a></li>
                    <li><i class="fa fa-tags"></i>
                        @foreach ($cate as $item)
                        @if (count($cate) <= 1) <a title='{{ $item->name }}' href={{ $item->slug }}>
                            {{ $item->name }}</a>
                            @else
                            <a title='{{ $item->name }}' href={{ $item->slug }}>{{ $item->name }}</a>,
                            @endif
                            @endforeach

                    </li>
                    <li><i class="fa fa-cloud-download"></i> {{ $story->source }}</li>
                    <li>
                        <i class="fa fa-eye"></i> <b>{{ $story->view }}</b>
                        <!--<i class="fa fa-comments w3-margin-left"></i> <b>99</b>-->
                    </li>
                    <li><i class="fa fa-rss"></i> <span>{{ $story->status == 1 ? 'Đang cập nhật' : 'Hoàn thành'}}</span>
                    </li>
                    <li><i class="fa fa-cloud-upload"></i> <time datetime="2017-07-27T22:41:19+07:00">
                            {{ Carbon\Carbon::parse($story->created_at )->format('H:i:s d-m-Y') }}</time></li>
                    <li><i class="fa fa-pencil-square-o"></i> <time
                            datetime="2020-03-21T21:42:19+07:00">{{ Carbon\Carbon::parse($story->updated_at )->format('H:i:s d-m-Y') }}</time>
                    </li>
                </ul>
            </div>
        </div>
        <div class="w3-col m8 l8 w3-container w3-center detail-right">
            <h1><a href="{{ $story->slug }}" title="{{ $story->name }}">{{ $story->name }}</a></h1>
            <hr>
            <div class="w3-justify summary">
               {{ $story->summary }}
            </div>
        </div>
    </div>
    <!-- LIST CHAPTER -->
    <div class="list-chapter">
        <h2><i class="fa fa-pencil-square-o"></i> 5 Chương mới cập nhật truyện {{ $story->name }}</h2>
        <ul class="w3-ul">
            @foreach ($chapter_new as $item)
                <li class="w3-pale-red">
                    <h4><a rel='nofollow' title={{ $item->name }}"
                            href="{{ $story->slug }}/{{ $item->slug }}">{{ $item->name_chapter }} - {{ $item->name }}</a>
                    </h4>
                    <span class="w3-right w3-hide-small">{{ Carbon\Carbon::parse($item->created_at )->format('d/m/Y') }}</span>
                </li>
            @endforeach
           
          
        </ul>
    </div>
    <!-- LIST FULL -->
    <div id="divtab" class="list-chapter">
        <h2><i class="fa fa-list"></i> Danh sách chương truyện {{ $story->name }} </h2>
        <ul class="w3-ul">
            @foreach ($chapters as $item)
                <li class="">
                    <h4><a title="{{ $item->name }}" href="{{ $story->slug }}/{{ $item->slug }}">{{ $item->name_chapter }} - {{ $item->name }}</a>
                    </h4>
                    <span class="w3-right w3-hide-small">{{ Carbon\Carbon::parse($item->created_at )->format('d/m/Y') }}</span>
                </li>
            @endforeach
        </ul>
        <!-- PAGES -->
        <div class="w3-center pagination">
            <ul class="w3-pagination paging">
                {{ $chapters->links() }}
            </ul>
          
        </div>
    </div>
    <!-- COMMENT -->
    <div class="comments">
        <!--<div class="w3-row w3-center comment-title"><a onclick="openTab(event, 'comment');"><div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><i class="fa fa-comments"></i> Bình Luận</div></a><a onclick="openTab(event, 'facebook');"><div class="w3-col s6 tablink w3-bottombar w3-hover-light-grey w3-padding"><i class="fa fa-facebook-square"></i> Facebook</div></a></div>-->
        <div class="comment-member">
            <div class="comment-heading">
                <i class="fa fa-comments"></i>
                <h3>Nhận xét của đọc giả về truyện Vạn Cổ Thần Đế</h3>
            </div>
            <div class="comment-content">
                <form class="w3-row comment-form" style="margin: 15px 0;" name="formcm"
                    action="https://webtruyen.com/method/comment" method="post" enctype="multipart/form-data"
                    onsubmit="return checkvailcm()">
                    <div class="w3-col s12 m12 l12 comment-text"><textarea onclick="openModal('login')" id="txtcm"
                            name="txtcm" class="w3-round"
                            placeholder="Nội dung bình luận tối thiểu 15 ký tự, tối đa 500 ký tự!"></textarea></div>
                    <div class="comment">
                        <div class="w3-col s12 m6 l6 comment-count"><label class="character" for="txtcm">Số ký tự:
                            </label><label id="ccm" class="number">0</label></div>
                        <div class="w3-col s12 m6 l6 comment-button"><button disabled="disabled" type="button" id="btcm"
                                name="btcm" onclick="" class="w3-btn button-send"><i class="fa fa-send"></i>
                                Gửi</button></div>
                        <div id="diverro" class="w3-col s12 m12 l12 comment-error"><label id="lberro"></label></div>
                    </div>
                    <p style="display:none;"><span></span><input id="notinput" name="notinput" type="text"
                            class="form-control"></p>
                    <input type="hidden" name="storyid-hidden" id="storyid-hidden" value="15793" /> <input type="hidden"
                        name="comment-cate" id="comment-cate" value="0" /> <input type="hidden" name="actual_link"
                        id="actual_link" value="index.html" />
                </form>
                <!--<button id="bt_a_d_fs" onclick="openModal('login')" class="w3-btn w3-round w3-blue w3-hover-white"><i class="fa fa-sign-in"></i> Đăng nhập</button>-->
                <!--<a href="#" rel="nofollow" onclick="openModal('login')"><i class="fa fa-sign-in"></i> Đăng Nhập</a>--><input
                    type="hidden" id="comment-flag" value="true" /> <input type="hidden" id="comment-page" value="1" />
                <div id="comment-list">
                    <div class="comment-row" id="comment_75437">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Ấu thần</a><span
                                class="comment-date">12:05 10/05/2020</span>
                            <div class="comment-chat">Truyện có sắc ko </div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_75363">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Hoàng Minh Anh</a><span
                                class="comment-date">14:05 08/05/2020</span>
                            <div class="comment-chat">Mng có ai biết bộ nào có main không trẻ trâu ko. Như Hàn Lập ấy
                            </div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_74872">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Jackie Lee</a><span
                                class="comment-date">00:04 29/04/2020</span>
                            <div class="comment-chat">Truyện rất hay nha mọi người.. mỗi ra chậm quá </div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_74854">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Duc Van</a><span
                                class="comment-date">18:04 28/04/2020</span>
                            <div class="comment-chat">Hay ko moi nguoi</div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_74742">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Vinh</a><span
                                class="comment-date">13:04 26/04/2020</span>
                            <div class="comment-chat">Ung hộ</div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_74468">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Hoanghoa</a><span
                                class="comment-date">19:04 21/04/2020</span>
                            <div class="comment-chat">1chương 10 điểm được có mấy dòng đắt vậy ???</div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_73959">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Jackie Lee</a><span
                                class="comment-date">19:04 14/04/2020</span>
                            <div class="comment-chat">Lịch ra truyện ntn vậy ad?</div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_73909">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">kiên mai</a><span
                                class="comment-date">23:04 13/04/2020</span>
                            <div class="comment-chat">bắt trả tiền đọc tiếp à</div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_73880">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Hà Công Thành</a><span
                                class="comment-date">13:04 13/04/2020</span>
                            <div class="comment-chat">Đọc chương 71 cần 10đ
                                Thế đọcc hương 72 có cần tiếp 10đ ko
                            </div>
                        </div>
                    </div>
                    <div class="comment-row" id="comment_73822">
                        <div class="w3-left comment-left"><img alt="Avatar" class="w3-circle" width="50" height="50"
                                src="frontend/images/avatar.png"></div>
                        <div class="comment-right">
                            <a class="comment-user" href="#" rel="nofollow" title="">Nhật hoà Bùi</a><span
                                class="comment-date">16:04 12/04/2020</span>
                            <div class="comment-chat">hay nhé mọi người ơiii</div>
                        </div>
                    </div>
                </div>
                <div id="lastPostsLoader"></div>
                <div class="w3-center comment-more"><button id="loadMoreComment" onclick="loadListComment()"
                        class="w3-btn w3-round-xlarge w3-grey w3-hover-white"><i class="fa fa-chevron-down"></i> Xem
                        thêm</button></div>
            </div>
        </div>
        <div class="comment-fb">
            <div class="comment-heading">
                <i class="fa fa-facebook-square"></i>
                <h3>BÌNH LUẬN FACEBOOK</h3>
            </div>
            <div class="comment-content">
                <div class="fb-comments" data-href="https://webtruyen.com/van-co-than-de/" data-width="auto"
                    data-numposts="5"></div>
            </div>
        </div>
    </div>
    <!-- TAGS -->
    <div class="w3-row detail-tags"></div>
</div>
@endsection
