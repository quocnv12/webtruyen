@extends('frontend.master.master')
@section('title', 'Review')
@section('content')
<div id="list" class="w3-col s12 m12 l8 w3-container">
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                    Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('get.review') }}" itemprop="name" class="active"> <span itemprop="name">Review </span> </a>
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="title-left">
                <h1><a title="Truyện Tiên hiệp" href="">Review  <i
                            class="fa fa-angle-right"></i></a></h1>
            </div>
            <div class="title-right">
                <a title="Tiên hiệp hay" href="{{ route('get.story.good') }}">Truyện Hay</a><i class="fa fa-circle"></i>
                <a title="Tiên full" href="{{ route('get.story.full') }}">Truyện Full</a><i class="fa fa-circle"></i>
                <a title="Tiên mới" href="{{ route('get.story.new') }}">Truyện Mới</a>
            </div>
        </div>

        <div class="w3-row list-content" style="padding: 10px 15px;">
            @foreach ($review as $item)
            <div class="w3-row post-row">
                <div class="w3-col s3 m3 l3 post-thumbnail"><a rel="nofollow"
                        href="{{ route('get.review.detail',['slug' => $item->slug]) }}"
                        title="{{ $item->title}}"><img width="160"
                            height="112"
                            src="frontend/images/{{ $item->image }}"
                            alt="{{ $item->title}}"></a></div>
                <div class="w3-col s9 m9 l9 post-content">
                    <h3 class="post-title"><a href="{{ route('get.review.detail',['slug' => $item->slug]) }}"
                            title="Tổng hợp review 5 truyện trọng sinh đáng đọc nhất của “mọt” ngôn tình">{{ $item->title}}</a></h3>
                    <div class="post-date"><span><i class="fa fa-clock-o"></i> {{ Carbon\Carbon::parse($item->created_at )->format('d-m-Y H:i:s') }}</span>
                    </div>
                    <p class="post-caption">{{ $item->summary}}</p>
                </div>
            </div>
            @endforeach
            
        </div>
        <div class="w3-center pagination">
            <ul class="w3-pagination paging">
                {{ $review->links() }}
            </ul>
        </div>
    </div>
</div>
@endsection
