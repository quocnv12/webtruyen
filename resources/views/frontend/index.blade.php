@extends('frontend.master.master')
@section('title','Đọc Truyện Online Hay Và Mới Nhất')
@section('content')
<div id="list" class="w3-col s12 m12 l8 main w3-container">
    <!-- LIST HOT -->
    <div class="list-hot">
        <div class="w3-row list-title">
            <div class="title-left">
                <h2>TRUYỆN ĐỀ CỬ <i class="fa fa-angle-right"></i></h2>
            </div>
            <div class="title-right">
                <a title="Truyện Ngôn tình hay" href="ngon-tinh">Ngôn Tình</a><i class="fa fa-circle"></i>
                <a title="Truyện Teen hay" href="truyen-teen">Truyện Teen</a><i class="fa fa-circle"></i>
                <a title="Truyện Tiên Hiệp hay" href="tien-hiep">Tiên Hiệp</a><i class="fa fa-circle"></i>
                <a title="Truyện Kiếm Hiệp hay" href="kiem-hiep">Kiếm Hiệp</a><i class="fa fa-circle"></i>
                <a title="Truyện Xuyên Không hay" href="xuyen-khong">Xuyên Không</a>
            </div>
        </div>
        <div class="w3-row w3-content w3-display-container list-content">
            <div class="w3-col s12 m12 l12 slides" style="display: block;">
                @foreach ($story_hot as $key => $item)
                    @if($key <= 2 )
                        <div class="w3-col s6 m4 l4">
                            <span class="hot-label"></span><a rel="nofollow" class="w3-hover-opacity" title="{{ $item->name }}" href="{{ $item->slug }}"><img width="214" height="260" class="slides-img" src="frontend/images/{{ $item->image }}" alt="{{ $item->name }}"></a>
                            <div class="slides-caption">
                                <h3><a href="{{ $item->slug }}" title="{{ $item->name }}">{{ $item->name }}</a></h3>
                                @php
                                 foreach ($item->chapters as $value){
                                    if($loop->last)
                                    {
                                        $value;
                                    }     
                                 }
                                @endphp
                                <p class="slides-chapter">{{ $value->name_chapter }}</p>
                            </div>
                            <span class="slides-view"><i class="fa fa-eye"></i>{{ $item->view }}</span>
                        </div>
                    @endif
                @endforeach
        </div>
            <div class="w3-col s12 m12 l12 slides">
                @foreach ($story_hot as $key => $item)
                @if($key >=3 )
                    <div class="w3-col s6 m4 l4">
                        <span class="hot-label"></span><a rel="nofollow" class="w3-hover-opacity" title="{{ $item->name }}" href="co-vo-hop-dong-bo-tron-cua-tong-giam-doc/index.html"><img width="214" height="260" class="slides-img" src="frontend/images/{{ $item->image }}" alt="{{ $item->name }}"></a>
                        <div class="slides-caption">
                            <h3><a href="{{ $item->slug }}" title="{{ $item->name }}">{{ $item->name }}</a></h3>
                            <p class="slides-chapter">{{ $value->name_chapter }}</p>
                        </div>
                        <span class="slides-view"><i class="fa fa-eye"></i> {{ $item->view }}</span>
                    </div>
                @endif
                @endforeach
            </div>
            <div class="w3-text-shadow w3-text-white w3-hover-text-grey slides-prev" onclick="plusDivs(-1)"><i class="fa fa-angle-left"></i></div>
            <div class="w3-text-shadow w3-text-white w3-hover-text-grey slides-next" onclick="plusDivs(1)"><i class="fa fa-angle-right"></i></div>
        </div>
    </div>
    <!-- LIST UPDATE --><!-- LIST UPDATE -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="title-left">
                <h2><a href="truyen-vip/index.html" title="TRUYỆN VIP">TRUYỆN VIP <i class="fa fa-angle-right"></i></a></h2>
            </div>
        </div>
        <div class="w3-row list-content">
            @foreach ($story_vips as $item)
            <div class="w3-col s6 m3 l3 list">
                <span class="vip-label"></span><a rel="nofollow" class="w3-hover-opacity" href="{{ $item->slug }}" title="{{ $item->name }}"><img width="157" height="208" class="list-thumbnail" src="frontend/images/{{ $item->image }}" alt="{{ $item->name }}"/></a>
                <div class="list-caption">
                    <a href="{{ $item->slug }}" title="{{ $item->name }}">{{ $item->name }}</a><span class="caption-view"><i class="fa fa-eye"></i> {{ $item->view }}</span>
                    @php
                    foreach ($item->chapters as $value){
                       if($loop->last)
                       {
                           $value;
                       }     
                    }
                   @endphp
                    <p class="caption-chapter">{{ $value->name_chapter }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- LIST UPDATE -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="title-left">
                <h2><a href="all/index.html" title="TRUYỆN MỚI CẬP NHẬT">TRUYỆN MỚI CẬP NHẬT <i class="fa fa-angle-right"></i></a></h2>
            </div>
            <div class="title-right">
                <a title="Truyện Ngôn tình hay" href="ngon-tinh">Ngôn Tình</a><i class="fa fa-circle"></i>
                <a title="Truyện Teen hay" href="truyen-teen">Truyện Teen</a><i class="fa fa-circle"></i>
                <a title="Truyện Tiên Hiệp hay" href="tien-hiep">Tiên Hiệp</a><i class="fa fa-circle"></i>
                <a title="Truyện Kiếm Hiệp hay" href="kiem-hiep">Kiếm Hiệp</a><i class="fa fa-circle"></i>
                <a title="Truyện Xuyên Không hay" href="xuyen-khong">Xuyên Không</a>
            </div>
        </div>
        <div class="w3-row list-content">
            @foreach ($chapter_new as $item)
            <div class="w3-col s12 m12 l12 list-row">
                <div class="w3-col s12 m7 l7 row-first">
                    <span><i class="fa fa-chevron-right"></i></span>
                    <span>
                        <a href="{{ $item->story->slug }}" title="{{ $item->story->name }}">{{ $item->story->name }}</a>
                    </span>
                    @if($item->story->active == 1)
                        <span class="vip-icon"></span>
                    @elseif($item->story->active == 2)
                        <span class="hot-icon"></span>
                    @endif
                </div>
                {{-- @php
                foreach ($item->chapters as $value){
                   if($loop->last)
                   {
                       $value;
                   }     
                }
               @endphp --}}
                <div class="w3-col s12 m3 l3 row-second">
                    <p class="row-link">{{ $item->name_chapter }}</p>
                </div>
                <div class="w3-col s12 m2 l2 row-third w3-hide-small">
                    <p class="row-text">{{ Carbon\Carbon::parse($item->created_at )->format('d/m/Y') }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- LIST FULL --><!-- LIST UPDATE -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="title-left">
                <h2><a href="truyen-full/index.html" title="TRUYỆN FULL">TRUYỆN FULL <i class="fa fa-angle-right"></i></a></h2>
            </div>
            <div class="title-right">
                <a title="Truyện Ngôn tình hay" href="ngon-tinh">Ngôn Tình</a><i class="fa fa-circle"></i>
                <a title="Truyện Teen hay" href="truyen-teen">Truyện Teen</a><i class="fa fa-circle"></i>
                <a title="Truyện Tiên Hiệp hay" href="tien-hiep">Tiên Hiệp</a><i class="fa fa-circle"></i>
                <a title="Truyện Kiếm Hiệp hay" href="kiem-hiep">Kiếm Hiệp</a><i class="fa fa-circle"></i>
                <a title="Truyện Xuyên Không hay" href="xuyen-khong">Xuyên Không</a>
            </div>
        </div>
        <div class="w3-row list-content">
            @foreach ($story_full as $item)
                <div class="w3-col s6 m3 l3 list">
                    <a rel="nofollow" class="w3-hover-opacity" href="{{ $item->slug }}" title="{{ $item->name }}"><img width="157" height="208" class="list-thumbnail" src="frontend/images/{{ $item->image }}" alt="{{ $item->name }}"/></a>
                    <div class="list-caption">
                        <a href="{{ $item->slug }}" title="{{ $item->name }}">{{ $item->name }}</a><span class="caption-view"><i class="fa fa-eye"></i> {{ $item->view }}</span>
                        <p class="caption-chapter">FULL</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <!-- LIST SHORTSTORY -->
    <div class="w3-margin-top list-shortstory">
        <div class="list-title">
            <h2><a href="{{ route('get.short.story') }}">TRUYỆN NGẮN <i class="fa fa-angle-right"></i></a></h2>
        </div>
        <div class="w3-row list-content">
            @foreach ($short_story as $key => $item)
                @if($key == 0)
                <div class="w3-col s12 m6 l6 w3-display-container list-left">
                    <a rel="nofollow" href="{{ route('get.short.story.detail',['slug' => $item->slug]) }}" title="{{ $item->title }}">
                        <img width="345" height="213" alt="{{ $item->title }}" class="w3-hover-opacity" src="frontend/images/{{ $item->image }}">
                    </a>
                    <h3 class="w3-display-bottommiddle list-left-title"><a href="{{ route('get.short.story.detail',['slug' => $item->slug]) }}" title="{{ $item->title }}">"{{ $item->title }}"</a></h3>
                </div>
                @else
                <div class="w3-col s12 m6 l6 w3-container list-right">

                    <div class="w3-row">
                        <div class="w3-col s3 m3 l3 list-right-thumbnail">
                            <a href="{{ route('get.short.story.detail',['slug' => $item->slug]) }}" title="{{ $item->title }}">
                                <img rel="nofollow" width="81" height="47" alt="{{ $item->title }}" class="w3-hover-opacity" src="frontend/images/{{ $item->image }}"/>
                            </a>
                        </div>
                        <div class="w3-col s9 m9 l9 list-right-title">
                            <h3><a href="{{ route('get.short.story.detail',['slug' => $item->slug]) }}" title="{{ $item->title }}">{{ $item->title }}</a></h3>
                        </div>
                    </div>
                  
                </div>
                @endif
                
            @endforeach
        </div>
    </div>
</div>
@endsection