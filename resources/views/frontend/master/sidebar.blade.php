<div id="sidebar" class="w3-col s12 m12 l4 sidebar w3-container">
    <div class="w3-row">
        <!-- RECENT -->
        <div id="sidebar-recent" class="w3-col s12 m12 l12 w3-margin-top sidebar-list sidebar-recent"></div>
        <!-- CATEGORY -->
        <div class="w3-col s12 m12 l12 sidebar-list sidebar-category">
            <div class="sidebar-title">
                <h2>THỂ LOẠI TRUYỆN <i class="fa fa-angle-right"></i></h2>
            </div>
            <div class="sidebar-content">
                <ul class="w3-ul w3-row">
                    @foreach ($categories as $item)
                        <li class="w3-col s6 m6 l6"><i class="{{ $item->id == 1 || $item->id == 2 || $item->id ==3 || $item->id == 4 || $item->id == 17  ? 'fa fa-diamond w3-text-red' : 'fa fa-tags' }}"></i> <a href="{{ $item->slug }}" title="{{ $item->name }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="w3-col s12 m12 l12 w3-margin-top sidebar-list">
            <div class="sidebar-title"><span>REVIEW TRUYỆN <i class="fa fa-angle-right"></i></span></div>
            <div class="sidebar-row">
                <ul class="w3-ul">
                    @foreach ($reviews as $item)
                        <li class="w3-row w3-margin">
                            <div class="w3-col s3 m3 l3 list-right-thumbnail"><a rel="nofollow" href="{{ route('get.review.detail',['slug' => $item->slug]) }}" title="{{ $item->title }}"><img alt="{{ $item->title }}" src="frontend/images/{{ $item->image }}"></a></div>
                            <div class="w3-col s8 m8 l8 list-right-title-review w3-margin-left"><a href="{{ route('get.review.detail',['slug' => $item->slug]) }}" title="{{ $item->title }}">{{ $item->title }}</a></div>
                        </li>
                    @endforeach
                    <li class="w3-row top-more"><a title="xem thêm bài review" href="{{ route('get.review') }}">Xem Thêm <i class="fa fa-angle-down"></i></a></li>
                </ul>
            </div>
        </div>
        <!--<div><a href="https://thegioitruyen.com/truyen-ngon-tinh/" title="truyện ngôn tình" target="_blank"><img src="https://webtruyen.com/ads/truyen-ngon-tinh.jpg" alt="truyen ngon tinh" style="margin-top: 10px;" /></a></div>--><!-- TOP STORY -->
        <div class="w3-col s12 m12 l12 sidebar-list sidebar-top">
            <div class="w3-row sidebar-title">
                <h2>Truyện Hay <i class="fa fa-angle-right"></i></h2>
            </div>
            <div class="sidebar-row">
                <ul class="w3-ul">
                    @foreach ($story_goods as $key => $item)
                        <li class="w3-row">
                            <div class="w3-col s2 m2 l2 top-left"><span class="top-number top-one">{{ $key+1 }}</span></div>
                            <div class="w3-col s10 m10 l10 top-right"><a title="{{ $item->name }}" href="{{ $item->slug }}">{{ $item->name }}</a>
                                <span class="sidebar-caption"><i class="fa fa-eye"></i> {{ $item->view }}</span></div>
                        </li>
                    @endforeach
                    
                    <li class="w3-row top-more"><a title="Truyện Hay" href="{{ route('get.story.good') }}">Xem Thêm <i class="fa fa-angle-down"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- New STORY -->
        <div class="w3-col s12 m12 l12 sidebar-list sidebar-top">
            <div class="w3-row sidebar-title">
                <h2>Truyện Mới Đăng <i class="fa fa-angle-right"></i></h2>
            </div>
            <div class="sidebar-row">
                <ul class="w3-ul">
                    @foreach ($story_news as $key => $item)
                        <li class="w3-row">
                            <div class="w3-col s2 m2 l2 top-left"><span class="top-number top-one">{{ $key+1 }}</span></div>
                            <div class="w3-col s10 m10 l10 top-right"><a title="{{ $item->name }}" href="{{ $item->slug }}">{{ $item->name }}</a>
                                <span class="sidebar-caption"><i class="fa fa-eye"></i> {{ $item->view }}</span></div>
                        </li>
                    @endforeach
                    
                
                    <li class="w3-row top-more"><a title="Truyện Mới Đăng" href="{{ route('get.story.new') }}">Xem Thêm <i class="fa fa-angle-down"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>