<div id="footer">
    <div class="w3-row-padding copyright">
        <div class="w3-col s12 m8 l8 w3-justify">
            <p>
                Website <a title="đọc truyện online" href="#">đọc truyện</a> online chất lượng hàng đầu việt
                nam, với nhiều <a title="Truyện Tiên Hiệp" href="#">truyện tiên hiệp</a>, <a
                    title="Truyện Kiếm Hiệp" href="#">truyện kiếm hiệp</a>, <a
                    title="truyen ngon tinh" href="#">truyện ngôn tình</a>, <a title="truyen teen"
                    href="#l">truyện teen</a>, <a title="Truyện Đô Thị"
                    href="#">truyện đô thị</a> được chọn lọc và đăng tải một cách kĩ lưỡng.Mời bạn đọc
                và ủng hộ website bằng cách giới thiệu đến bạn bè, chúc bạn có những giây phút đọc truyện thư giãn thật
                bổ ích. <br> <a rel="nofollow" href="#" title="Privacy Policy">Privacy
                    Policy</a> &nbsp; - &nbsp;<a rel="nofollow" href="#" title="Contact">Contact</a>
                &nbsp; - &nbsp;<a rel="nofollow" href="#" title="About us">About us</a> <br />
                {{-- <script src="frontend/js/DMCABadgeHelper.min.js"></script>   --}}
            </p>
        </div>
        <div class="w3-col s12 m4 l4 w3-center">
            <p>Copyright <i class="fa fa-copyright"></i> 2016 <a title="https://webtruyen.com"
                    href="#">WebTruyen.Com</a><br> Kết nối với chúng tôi</p>
            <p> <a class="w3-btn-floating w3-hover-opacity w3-indigo w3-margin-right" title="Fanpge của webtruyen.com"
                    href="#" rel="nofollow"><i class="fa fa-facebook"></i></a>
            </p>
        </div>
    </div>
</div>
