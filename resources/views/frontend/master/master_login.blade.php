<!DOCTYPE html>
<html xml:lang="vi" lang="vi">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <meta name="google-site-verification" content="JPbIb8GvJdk7bOIVz8f4l7k93qtbxW-EnI3IMR3m0Is" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" type="image/png" href="https://webtruyen.com/frontend/img-thumb/favicon.ico" />
        <title>@yield('title')</title>
        <base href="{{ asset('') }}">
        <link rel="stylesheet" href="frontend/css/style.css">
        <meta property="og:image" content="https://img.webtruyen.com/public/frontend/desk/img/logo_b.png" />
       {{--  noty  --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
        <script src="{{ asset('asset/js/notify.js') }}"></script>
    </head>
    <body>
        @if (session('success'))
            <script>
                notify("<div style='font-size:15px'><i style='line-height: 20px'; class='fa fa-thumbs-up'><i/> {{ session('success') }} </div>",'success');
            </script>
        @endif

        @if (session('danger'))
            <script>
                notify("<div style='font-size:15px'><i style='line-height: 20px;' class='fa ffa fa-exclamation-circle'><i/> {{ session('danger') }} </div>",'error');
            </script>
        @endif
        @include('frontend.master.header_login')
        @yield('content_login')
        @include('frontend.master.sidebar')
        @include('frontend.master.footer')


       <script src="frontend/js/jquery.min.js"></script> 
        <noscript id="deferred-styles">
            <link rel="stylesheet" type="text/css" href="frontend/css/style_10_12.css" />
        </noscript>
        <script> $('head').append( '<link rel="stylesheet preload" as="style" type="text/css" href="frontend/css/style_10_12.css"/>' ); </script>
        <script src="frontend/js/myscript_080420.js"></script>
        <script src="frontend/js/yt_tracking.js"></script> 
        <script async defer src="frontend/js/ads.js"></script>
        <script type="text/javascript"> var _avlVar = _avlVar || []; _avlVar.push(["ff0ebc5a6b9b437db6eb219f5ce0de44", "[yo_page_url]", "[width]", "[height]"]); </script> 
        <script src="frontend/js/yomedia-sdk30f4.js?v=3" id="s-ff0ebc5a6b9b437db6eb219f5ce0de44"></script> 
        <script src="frontend/css/truyenhay.js"></script> 
    </body>
</html>