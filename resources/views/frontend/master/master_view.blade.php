<!DOCTYPE html>
<html xml:lang="vi" lang="vi">
    <head>
        <meta name="google-site-verification" content="JPbIb8GvJdk7bOIVz8f4l7k93qtbxW-EnI3IMR3m0Is" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" type="image/png" href="https://webtruyen.com/frontend/img-thumb/favicon.ico" />
        <title>@yield('title')</title>
        <base href="{{ asset('') }}">
        <link rel="stylesheet" href="https://img.webtruyen.com/public/frontend/desk/css/style_05_02.css">
    </head>
    <body>
        @include('frontend.master.header')
        @yield('content_view')
        </div>
        @include('frontend.master.footer')
    </body>
</html>