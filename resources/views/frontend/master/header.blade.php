<div id="header" class="w3-top">
    <!--class="w3-top"-->
    <!-- NAVIGATION BAR -->
    <ul class="w3-navbar w3-large w3-left-align navbar">
        <li class="w3-hide-medium w3-hide-large w3-opennav w3-right nav-icon"> <a href="javascript:void(0);"
                onclick="collapseNav()" rel="nofollow"><i class="fa fa-navicon"></i></a> </li>
        <li id="logo">
            <h1><a title="doc truyen online" href="{{ route('index') }}">Đoc truyen online</a></h1>
        </li>
        <li class="w3-hide-small w3-dropdown-hover">
            <a title="Menu Thể loại" rel="nofollow" href="javascript:void(0);"><i class="fa fa-navicon"></i> Thể
                Loại</a>
            <div class="w3-row w3-dropdown-content w3-card-2 navbar-dropdown">
                @foreach ($categories as $item)
                <a class="w3-col s6 m6 l6" title="{{ $item->name }}" href="{{ $item->slug }}"><i class="fa fa-tags"></i>
                    {{ $item->name }}</a>
                @endforeach
                <a class="w3-col s6 m6 l6" href="{{ route('get.review') }}" title=""><i class="fa fa-tags"></i>
                    Review</a> </div>
        </li>
        <li class="w3-hide-small w3-dropdown-hover">
            <a title="Menu sắp xếp" rel="nofollow" href="javascript:void(0);"><i class="fa fa-sort"></i> Sắp Xếp</a>
            <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                <a class="w3-col s6 w3-text-red" href="{{ route('get.story.vip') }}" title="Truyện VIP"><i class="fa fa-diamond"></i>
                    Truyện VIP</a>
                
                <a class="w3-col s6" href="{{ route('get.story.new') }}" title="Truyện mới Đăng"><i
                        class="fa fa-cloud-upload"></i> Mới Đăng</a>
                <a class="w3-col s6" href="{{ route('get.story.good') }}" title="Truyện đọc nhiều"><i class="fa fa-eye"></i> Xem
                    Nhiều</a>
               

                <a class="w3-col s6" href="{{ route('get.story.full') }}" title="Truyện Full"><i class="fa fa-signal"></i>
                    Truyện Full</a> 
            </div>
        </li>
        <li class="w3-hide-small navbar-search">
            <!-- search box new [add sitelink searchbox] -->
            <div itemscope itemtype="">
                <meta itemprop="url" content="index.html" />
                <form itemprop="potentialAction" itemscope itemtype="" class="search-bar"
                    method="post" action="{{ route('post.search') }}" id="search-form">
                    <meta itemprop="target" content="" />
                    @csrf
                    <input itemprop="query-input" class="w3-input search-bar-input" type="text" id="key" name="key"  placeholder="Tìm truyện .." autocomplete="off"> 
                    <button class="w3-btn search-bar-button" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- SEARCH RESULT -->
            <div id="display" class="w3-white w3-card-2 search-result" style="display:none"> </div>
        </li>
        @if(Auth::check())
        <li id="boxunmember" class="w3-hide-small w3-dropdown-hover w3-right">
            <a href="javascrip::voi(0)" rel="nofollow"><i class="fa fa-user"></i> {{ Auth::user()->email }}</a>
            <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                <a href="{{ route('get.info.user',['id' => Auth::user()->id]) }}" rel="nofollow"><i
                        class="fa fa-sign-in"></i> Thông tin tài khoản</a>
                <a href="{{ route('get.changer.password') }}" rel="nofollow"><i class="fa fa-pencil-square-o"></i> Đổi
                    mật khẩu</a>
                <a href="{{ route('get.logout.user') }}" rel="nofollow"><i class="fa fa-sign-out"></i> Đăng xuất</a>
            </div>
        </li>
        @else
        <li id="boxunmember" class="w3-hide-small w3-dropdown-hover w3-right">
            <a href="#" rel="nofollow"><i class="fa fa-user"></i> Thành Viên</a>
            <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                <a href="#" rel="nofollow" onclick="openModal('login')"><i class="fa fa-sign-in"></i> Đăng Nhập</a>
                <a href="#" rel="nofollow" onclick="openModal('reg')"><i class="fa fa-pencil-square-o"></i> Đăng Ký</a>
            </div>
        </li>
        @endif
    </ul>
    <!-- RESPONSIVE NAV -->
    <div id="nav" class="w3-hide w3-hide-large w3-hide-medium">
        <ul class="w3-navbar w3-left-align w3-large">
            <li class="nav-search">
                <form class="nav-search-bar" role="search" method="get"
                    action="https://webtruyen.com/home/action_search1"> <input class="w3-input w3-border" type="text"
                        id="key1" name="key1" placeholder="Tìm truyện, tác giả..." autocomplete="off" /> <button
                        class="w3-btn w3-grey w3-round"><i class="fa fa-search"></i></button> </form>
            </li>
            @if(Auth::check())
            <li id="boxunmember_1" class="w3-hide-small w3-dropdown-hover w3-right">
                <a href="javascrip::voi(0)" rel="nofollow"><i class="fa fa-user"></i> {{ Auth::user()->email }}</a>
                <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                    <a href="{{ route('get.info.user',['id' => Auth::user()->id]) }}" rel="nofollow"><i
                            class="fa fa-sign-in"></i> Thông tin tài khoản</a>
                    <a href="{{ route('get.changer.password') }}" rel="nofollow"><i class="fa fa-pencil-square-o"></i>
                        Đổi mật khẩu</a>
                    <a href="{{ route('get.logout.user') }}" rel="nofollow"><i class="fa fa-sign-out"></i> Đăng xuất</a>
                </div>
            </li>
            @else
            <li id="boxunmember_1" class="w3-hide-small w3-dropdown-hover w3-right">
                <a href="#" rel="nofollow"><i class="fa fa-user"></i> Thành Viên</a>
                <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                    <a href="#" rel="nofollow" onclick="openModal('login')"><i class="fa fa-sign-in"></i> Đăng Nhập</a>
                    <a href="#" rel="nofollow" onclick="openModal('reg')"><i class="fa fa-pencil-square-o"></i> Đăng
                        Ký</a>
                </div>
            </li>
            @endif

            <li>
                <a title="Menu Thể loại" rel="nofollow" href="javascript:void(0);">Thể Loại <i
                        class="fa fa-angle-down w3-right" style="font-size: 22px;"></i></a>
                <div class="w3-row nav-content">
                    @foreach ($categories as $item)
                    <a class="w3-col s6 m6 l6" title="Truyện Tiên hiệp" href="theloai.html"><i class="fa fa-tags"></i>
                        {{ $item->name }}</a>
                    @endforeach
                    <a class="w3-col s6 m6 l6" href="review/index.html" title="Review truyện"><i class="fa fa-tags"></i>
                        Review</a> </div>
            </li>
            <li>
                <a title="Menu sắp sếp" rel="nofollow" href="javascript:void(0);">Sắp Xếp <i
                        class="fa fa-angle-down w3-right" style="font-size: 22px;"></i></a>
                <div class="w3-row nav-content"> 
                    <a class="w3-col s6 w3-text-red" href="{{ route('get.story.vip') }}" title="Truyện VIP"><i class="fa fa-diamond"></i>
                        Truyện VIP</a>
                   
                    <a class="w3-col s6" href="{{ route('get.story.new') }}" title="Truyện mới Đăng"><i
                            class="fa fa-cloud-upload"></i> Mới Đăng</a>
                    <a class="w3-col s6" href="{{ route('get.story.good') }}" title="Truyện đọc nhiều"><i class="fa fa-eye"></i> Xem
                        Nhiều</a>
                    
                    <a class="w3-col s6" href="{{ route('get.story.full') }}" title="Truyện Full"><i class="fa fa-signal"></i>
                        Truyện Full</a> 
                    </div>
            </li>
        </ul>
    </div>
    <!-- LOGIN MODAL -->
    <div id="login" class="w3-modal w3-round">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom modal-login">
            <div class="w3-center modal-title">
                <span onclick="closeModal('login')" class="w3-closebtn" style="margin-right: 10px;"><i
                        class="fa fa-close"></i></span>
                <p><b>THÀNH VIÊN</b></p>
            </div>
            <div class="w3-container modal-content">
                <form name="frmlogin" id="frmlogin" action="{{ route('post.login-modal.user') }}" method="post">
                    @csrf
                    <input class="w3-input w3-border w3-margin-bottom" type="text" name="email" id="email"
                        placeholder="Email">
                    <input class="w3-input w3-border w3-margin-bottom" type="password" name="password" id="password"
                        placeholder="Mật khẩu">
                    <input class="w3-border w3-margin-bottom" type="checkbox" name="remember" checked="checked">
                    <label class="w3-label w3-text-grey"><b>Ghi nhớ mật khẩu</b> </label>
                    <a rel="nofollow" title="Quên mật khẩu" href="{{ route('get.forgot.password') }}">Quên mật khẩu?</a>
                    <input type="hidden" name="link" id="link" value="index.html">
                    <button class="w3-btn w3-btn-block w3-green" type="submit">Đăng Nhập</button>

                </form>

                <div class="w3-row w3-center" style="margin: 8px 0;">

                </div>
            </div>
            <div class="w3-border-top w3-center w3-padding w3-light-grey"> <span>Chưa có tài khoản? </span>
                <a rel="nofollow" title="đăng ký thành viên" href="{{ route('get.register.user') }}"><b>Đăng Ký</b></a>
            </div>
        </div>
    </div>
    <!-- register MODAL -->
    <div id="reg" class="w3-modal w3-round">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom modal-login">
            <div class="w3-center modal-title">
                <span onclick="closeModal('reg')" class="w3-closebtn" style="margin-right: 10px;"><i
                        class="fa fa-close"></i></span>
                <p><b>ĐĂNG KÝ THÀNH VIÊN</b></p>
            </div>
            <div class="w3-container modal-content">
                <form class="w3-container" name="frmAdd" id="frmAdd" action="{{ route('post.register.modal.user') }}"
                    method="post">
                    @csrf
                    <p><label class="w3-label w3-text-grey"><b>Email</b> <i>(bắt buộc)</i></label>
                        <input name="reg_email" id="reg_email" class="w3-input w3-border" type="text"
                            placeholder="Youremail@gmail.com" value=""></p>
                    <p><label class="w3-label w3-text-grey"><b>Mật Khẩu</b> <i>(ít nhất 6 ký tự)</i></label>
                        <input name="reg_password" id="reg_password" class="w3-input w3-border" type="password"
                            placeholder="********"></p>
                    <p><label class="w3-label w3-text-grey"><b>Nhập lại Mật Khẩu</b></label>
                        <input name="passwordConfirmation" id="reg_repassword" class="w3-input w3-border"
                            type="password" placeholder="********"></p>
                    <p><label class="w3-label w3-text-grey"><b>Tên hiển thị</b> <i>(bắt buộc)</i></label> <input
                            name="namedisplay" id="namedisplay" class="w3-input w3-border" type="text"
                            placeholder="Tên hiển thị" value=""></p>
                    <p> <input class="w3-radio" type="radio" name="gender" value="1" checked>
                        <label class="w3-validate w3-margin-right">Nam</label> <input class="w3-radio" type="radio"
                            name="gender" value="2"> <label class="w3-validate w3-margin-right">Nữ</label>
                    </p>
                    <p><button class="w3-btn w3-round w3-green" type="submit">Đăng ký</button>
                        <a class="w3-btn w3-round w3-grey w3-margin-left" title="quay về"
                            href="{{ route('index') }}">Hủy</a></p>
                </form>
            </div>
        </div>
    </div>
    <div class="support-bar"> <a href="#" id="scroll" class="w3-tooltip" title="Về Đầu Trang" rel="nofollow"
            style="display: none;"> <i class="fa fa-chevron-up"></i> <span
                class="w3-text w3-tag w3-animate-opacity left-tooltip">Về Đầu Trang</span> </a> <a
            href="https://docs.google.com/forms/d/e/1FAIpQLSeWjMHxS5sR45L8Uou_m9zgPCdX0oIjmm3b1c4AHw8Xa5R9Gg/viewform"
            id="mail" title="Góp ý" rel="nofollow" class="w3-tooltip" target="_blank"> <i class="fa fa-envelope-o"></i>
            <span class="w3-text w3-tag w3-animate-right left-tooltip">Góp ý</span> </a> </div>
    <div id="fb-root"></div>
</div>
<div id="main" class="w3-row ">
    <!--not-fixed-->
    <div id="truyen-sticky"
        style="background-color: antiquewhite; padding: 3px; margin-top: 5px; margin-bottom: 5px; text-align: center; max-width: 95%; margin: 0 auto;">
    </div>
