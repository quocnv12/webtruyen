<div id="header" class="w3-top">
    <!--class="w3-top"-->
    <!-- NAVIGATION BAR -->
    <ul class="w3-navbar w3-large w3-left-align navbar">
        <li class="w3-hide-medium w3-hide-large w3-opennav w3-right nav-icon"> <a href="javascript:void(0);"
                onclick="collapseNav()" rel="nofollow"><i class="fa fa-navicon"></i></a> </li>
        <li id="logo">
            <h1><a title="doc truyen online" href="{{ route('index') }}">Đoc truyen online</a></h1>
        </li>
        <li class="w3-hide-small w3-dropdown-hover">
            <a title="Menu Thể loại" rel="nofollow" href="javascript:void(0);"><i class="fa fa-navicon"></i> Thể
                Loại</a>
            <div class="w3-row w3-dropdown-content w3-card-2 navbar-dropdown">
                @foreach ($categories as $item)
                <a class="w3-col s6 m6 l6" title="Truyện Tiên hiệp" href="{{ $item->slug }}"><i class="fa fa-tags"></i>
                    {{ $item->name }}</a>
                @endforeach
                <a class="w3-col s6 m6 l6" href="review/index.html" title="{{ route('get.review') }}"><i class="fa fa-tags"></i>
                    Review</a> </div>
        </li>
        <li class="w3-hide-small w3-dropdown-hover">
            <a title="Menu sắp xếp" rel="nofollow" href="javascript:void(0);"><i class="fa fa-sort"></i> Sắp Xếp</a>
            <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                <a class="w3-col s6 w3-text-red" href="{{ route('get.story.vip') }}" title="Truyện VIP"><i class="fa fa-diamond"></i>
                    Truyện VIP</a>
                
                <a class="w3-col s6" href="{{ route('get.story.new') }}" title="Truyện mới Đăng"><i
                        class="fa fa-cloud-upload"></i> Mới Đăng</a>
                <a class="w3-col s6" href="{{ route('get.story.good') }}" title="Truyện đọc nhiều"><i class="fa fa-eye"></i> Xem
                    Nhiều</a>
               
                <a class="w3-col s6" href="{{ route('get.story.full') }}" title="Truyện Full"><i class="fa fa-signal"></i>
                    Truyện Full</a> 
                </div>
        </li>
        <li class="w3-hide-small navbar-search">
            <!-- search box new [add sitelink searchbox] -->
            <div itemscope itemtype="https://schema.org/WebSite">
                <meta itemprop="url" content="index.html" />
                <form itemprop="potentialAction" itemscope itemtype="https://schema.org/SearchAction" class="search-bar"
                    method="get" action="https://webtruyen.com/home/action_search/" id="search-form">
                    <meta itemprop="target" content="home/action_search/indexe8f6.html?key={key}" />
                    <input itemprop="query-input" class="w3-input search-bar-input" type="text" id="key" name="key"
                        placeholder="Tìm truyện, tác giả..." autocomplete="off"> <button
                        class="w3-btn search-bar-button" type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- SEARCH RESULT -->
            <div id="display" class="w3-white w3-card-2 search-result" style="display:none"> </div>
        </li>
        <li id="boxunmember" class="w3-hide-small w3-dropdown-hover w3-right">
            <a href="javascript:;void(0)" rel="nofollow"><i class="fa fa-user"></i> Đăng nhập/Đăng ký</a>
            <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                <a href="{{ route('get.login.user') }}" rel="nofollow"><i class="fa fa-pencil-square-o"></i> Đăng
                    nhập</a>
                <a href="{{ route('get.register.user') }}" rel="nofollow"><i class="fa fa-pencil-square-o"></i> Đăng
                    Ký</a>
            </div>

        </li>
    </ul>
    <!-- RESPONSIVE NAV -->
    <div id="nav" class="w3-hide w3-hide-large w3-hide-medium">
        <ul class="w3-navbar w3-left-align w3-large">
            <li class="nav-search">
                <form itemprop="potentialAction" itemscope itemtype="" class="search-bar"
                method="post" action="{{ route('post.search') }}" id="search-form">
                <meta itemprop="target" content="" />
                @csrf
                <input itemprop="query-input" class="w3-input search-bar-input" type="text" id="key" name="key"  placeholder="Tìm truyện .." autocomplete="off"> 
                <button class="w3-btn search-bar-button" type="submit"><i class="fa fa-search"></i></button>
            </form>
            </li>
            <li id="boxunmember_1">
                <a href="javascript:;void(0)" rel="nofollow"><i class="fa fa-user"></i> Đăng nhập/Đăng ký</a>
                <div class="w3-dropdown-content w3-card-2 navbar-dropdown">
                    <a href="{{ route('get.login.user') }}" rel="nofollow"><i class="fa fa-pencil-square-o"></i> Đăng
                        nhập</a>
                    <a href="{{ route('get.register.user') }}" rel="nofollow"><i class="fa fa-pencil-square-o"></i> Đăng
                        Ký</a>
                </div>
            </li>
            <li>
                <a title="Menu Thể loại" rel="nofollow" href="javascript:void(0);">Thể Loại <i
                        class="fa fa-angle-down w3-right" style="font-size: 22px;"></i></a>
                <div class="w3-row nav-content">
                    @foreach ($categories as $item)
                    <a class="w3-col s6 m6 l6" title="Truyện Tiên hiệp" href="theloai.html"><i class="fa fa-tags"></i>
                        {{ $item->name }}</a>
                    @endforeach
                    <a class="w3-col s6 m6 l6" href="review/index.html" title="Review truyện"><i class="fa fa-tags"></i>
                        Review</a> </div>
            </li>
            <li>
                <a title="Menu sắp sếp" rel="nofollow" href="javascript:void(0);">Sắp Xếp <i
                        class="fa fa-angle-down w3-right" style="font-size: 22px;"></i></a>
                <div class="w3-row nav-content"> 
                    <a class="w3-col s6 w3-text-red" href="{{ route('get.story.vip') }}" title="Truyện VIP"><i class="fa fa-diamond"></i>
                        Truyện VIP</a>
                    <a class="w3-col s6" href="{{ route('get.story.new') }}" title="Truyện mới Đăng"><i
                            class="fa fa-cloud-upload"></i> Mới Đăng</a>
                    <a class="w3-col s6" href="{{ route('get.story.good') }}" title="Truyện đọc nhiều"><i class="fa fa-eye"></i> Xem
                        Nhiều</a>
                   
                    <a class="w3-col s6" href="{{ route('get.story.full') }}" title="Truyện Full"><i class="fa fa-signal"></i>
                        Truyện Full</a> 
                    </div>
            </li>
        </ul>
    </div>

    <div class="support-bar"> <a href="#" id="scroll" class="w3-tooltip" title="Về Đầu Trang" rel="nofollow"
            style="display: none;"> <i class="fa fa-chevron-up"></i> <span
                class="w3-text w3-tag w3-animate-opacity left-tooltip">Về Đầu Trang</span> </a> <a
            href="https://docs.google.com/forms/d/e/1FAIpQLSeWjMHxS5sR45L8Uou_m9zgPCdX0oIjmm3b1c4AHw8Xa5R9Gg/viewform"
            id="mail" title="Góp ý" rel="nofollow" class="w3-tooltip" target="_blank"> <i class="fa fa-envelope-o"></i>
            <span class="w3-text w3-tag w3-animate-right left-tooltip">Góp ý</span> </a> </div>
    <div id="fb-root"></div>
</div>
<div id="main" class="w3-row ">
    <!--not-fixed-->
    <div id="truyen-sticky"
        style="background-color: antiquewhite; padding: 3px; margin-top: 5px; margin-bottom: 5px; text-align: center; max-width: 95%; margin: 0 auto;">
    </div>
