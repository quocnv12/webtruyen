@extends('frontend.master.master')
@section('title','Đọc Truyện Online Hay Và Mới Nhất')
@section('content')
<div id="list" class="w3-col s12 m12 l8">
    <!-- SITEMAP -->
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="javascrip::voi(0)" itemprop="name" class="active"> <span itemprop="name">Tìm kiếm</span> </a> 
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="w3-col s12 m6 l6 title-left">
                <h1><a href="javascrip::voi(0)">Tìm kiếm <i class="fa fa-angle-right"></i></a></h1>
            </div>
        </div>
        <div class="w3-row list-content">
            
          @if(count($search) >= 1)
                @foreach ($search as $item)
                <div class="w3-row list-row-img">
                    <div class="w3-col s2 m2 l2 row-image">
                        <div><a rel="nofollow" href="{{ $item->slug }}" title="{{ $item->name }}"><img src="frontend/images/{{ $item->image }}" alt="Vạn Cổ Thần Đế" width="55" height="70"/></a></div>
                    </div>
                    <div class="w3-col s7 m7 l7 row-info">
                        <div>
                            <a href="{{ $item->slug }}" title="{{ $item->name }}">{{ $item->name }}</a><span class="row-author"><i class="fa fa-user"></i> {{ $item->author }}</span>
                        </div>
                    </div>
                    <div class="w3-col s3 m3 l3 row-number">
                        @php
                        foreach ($item->chapters as $value){
                            if($loop->last)
                            {
                                $value;
                            }     
                        }
                        @endphp
                        <div><span class="row-chapter">{{ $value->name }}</span><span class="row-time"><i class="fa fa-history"></i>{{ Carbon\Carbon::parse($item->updated_at )->format('H:i  d-m-Y') }}</span></div>
                    </div>
                </div>
                @endforeach
            @else
            <div class="w3-row list-row-img">
               <p style="margin-left:10px"> Không tìm thấy truyện "{{ $key }}"</p>
            </div>
          @endif
            {{-- <div class="w3-row list-row-img">
                <div class="w3-col s2 m2 l2 row-image">
                    <div><a rel="nofollow" href="https://webtruyen.com/yeu-phai-thien-tai-nam-vung/" title="Yêu Phải Thiên Tài Nằm Vùng"><img src="https://img.webtruyen.com/public/images/medium/yeuphaithientainamvungi3TuKaDJSc.jpg" alt="Yêu Phải Thiên Tài Nằm Vùng" width="55" height="70"/></a></div>
                </div>
                <div class="w3-col s7 m7 l7 row-info">
                    <div>
                        <a href="https://webtruyen.com/yeu-phai-thien-tai-nam-vung/" title="Yêu Phải Thiên Tài Nằm Vùng">Yêu Phải Thiên Tài Nằm Vùng</a><!--if($row['Vip'] == 1) echo '<span class="vip-icon"></span>';	else--><span class="full-icon"></span><!--<span class="new-icon"></span>--><span class="row-author"><i class="fa fa-user"></i> Apple</span>
                    </div>
                </div>
                <div class="w3-col s3 m3 l3 row-number">
                    <div><span class="row-chapter">FULL</span><span class="row-time"><i class="fa fa-history"></i>2 năm trước</span></div>
                </div>
            </div> --}}
            
        </div>
    </div>
    {{ $search->links() }}
</div>
@endsection