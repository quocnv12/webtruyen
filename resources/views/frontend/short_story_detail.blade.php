@extends('frontend.master.master_view')
@section('title', $short_story->title)
@section('content_view')
<div id="reading" style="background: rgb(255, 255, 255); color: rgb(51, 51, 51);">
    <!-- TITLE -->
    <div class="w3-row w3-center chapter-header">
        <ul class="w3-ul">
            <li>
                <h1><a title="{{ $short_story->name }}"
                        href="{{ $short_story->slug }}"
                        class="w3-text-red w3-hover-text-grey">{{ $short_story->title }}</a></h1>
            </li>
            <li><i class="fa fa-user"></i> {{ $short_story->author }}<i class="fa fa-clock-o w3-margin-left"></i>
                {{ Carbon\Carbon::parse($short_story->created_at )->format('d-m-Y H:i:s') }}</li>
        </ul>
    </div>
    <div style='font-size: 20px;margin: 12px;'>
        <b>{!! $short_story->summary !!}</b>
    </div>
    <div style="padding: 10px;text-align:center">
        <img src="frontend/images/{!! $short_story->image !!}" width="300px" alt="">
    </div>
    <div id="content" class="w3-justify shortstory-content reviews-content">
    {!! $short_story->content !!}
    </div>
</div>
@endsection
