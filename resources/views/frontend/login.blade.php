@extends('frontend.master.master_login')
@section('title', 'Đăng nhập')
@section('content_login')
<div id="list" class="w3-col s12 m12 l8">
    <!-- SITEMAP -->
    <div class="w3-round sitemap" itemscope itemtype="">
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('index') }}" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i>Trang
                    Chủ</span></a>
            <meta itemprop="position" content="1" />
            <i class="fa fa-angle-right"></i>
        </div>
        <div itemprop="itemListElement" itemscope itemtype="" style="float: left;">
            <a href="{{ route('get.login.user') }}" itemprop="name" class="active"> <span itemprop="name">Đăng
                    nhập</span> </a>
            <meta itemprop="position" content="2" />
        </div>
        <div class="clear"></div>
    </div>
    <!-- LIST STORY -->
    <div class="list-update">
        <div class="w3-row list-title">
            <div class="w3-col s12 m6 l6 title-left">
                <h1><a href="{{ route('get.login.user') }}">Đăng nhập <i class="fa fa-angle-right"></i></a></h1>
            </div>
        </div>
        <form class="w3-container" name="frmlogin" id="frmlogin" action="{{ route('post.login.user') }}" method="post" enctype="multipart-formdata">
            @csrf
            <p><label class="w3-label w3-text-grey"><b>Email</b> </label>
                <input name="email" id="email" class="w3-input w3-border" type="text" placeholder="youremail@xxx.com" value="">
                {!! showErrors($errors,'email') !!}
            </p>
            <p><label class="w3-label w3-text-grey"><b>Mật Khẩu</b></label>
                <input name="password" id="password" class="w3-input w3-border" type="password" placeholder="********">
                {!! showErrors($errors,'password') !!}
            </p>

            <p><input type="checkbox" name="remember"><label class="w3-label w3-text-grey"><b> Ghi nhớ mật khẩu</b> </label></p>
            <p><a rel="nofollow" title="Quên mật khẩu" href="{{ route('get.forgot.password') }}"><b>Quên mật khẩu?</b></a> | <a rel="nofollow" title="đăng ký thành viên"
                    href="{{ route('get.register.user') }}"><b>Đăng Ký</b></a></p>
            <p><input class="w3-btn w3-round w3-green" value="Đăng nhập" type="submit"><a
                    class="w3-btn w3-round w3-grey w3-margin-left" type="reset" title="quay về"
                    href="{{ route('index') }}">Hủy</a></p>
        </form>
    </div>
</div>
@endsection
