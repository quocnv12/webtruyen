@extends('backend.master.master')
@section('title', 'Thêm thể loại')
@section('category', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Thể loại</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.category') }}">Thể loại</a></li>
                    <li class="active">Thêm</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">Thêm thể loại</div>
                    <div class="card-body card-block">
                        <form method="POST">
                            @csrf
                            <div class="form-group"><label for="company" class=" form-control-label">Thể loại</label>
                                <input type="text" name="name" id="company" value="{{ old('name') }}" placeholder="Nhập thể loại" class="form-control">
                                {!! showErrors($errors,'name') !!}
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp; Thêm</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection
