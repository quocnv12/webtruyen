@extends('backend.master.master')
@section('category', 'show')
@section('title', 'Danh sách thể loại')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Thể loại</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.category') }}">Thể loại</a></li>
                    <li class="active">Danh sách</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Danh sách thể loại</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên thể loại</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.get.edit.category',['id' => $item->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-edit"></i>&nbsp; Edit</a>
                                        <a href="{{ route('admin.get.delete.category',['id' => $item->id]) }}" onclick="return confirm('Bạn có chắc chắn muốn xóa thể loại!')"  class="btn btn-sm btn-link" data-button-type="delete"><i class="fa fa-trash"></i>&nbsp; Delete</a>                                     
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div>
@endsection