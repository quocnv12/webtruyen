<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Đăng nhập quản tri</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="backend/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="backend/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="backend/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="backend/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="backend/vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="backend/assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>

<body class="bg-dark">

    @if (session('danger'))
    <script>
        notify("<div style='font-size:15px'><i style='line-height: 20px;' class='fa ffa fa-exclamation-circle'><i/> {{ session('danger') }} </div>",
            'error');

    </script>
    @endif

    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="images/logo.png" alt="">
                    </a>
                </div>
                <div class="login-form">
                    <form action="{{ route('post.login.admin') }}" method="POST">
                        @csrf
                        @csrf
                        @if(session('success'))
                            <div style='font-size:15px;margin-top:10px;color:#28a745'> {{ session('success') }} </div>
                        @endif
                        @if(session('danger'))
                            <div style='font-size:15px;margin-top:10px;color:red'> {{ session('danger') }} </div>
                        @endif
                        <div class="form-group">
                            <label>Email:</label>
                            <input type="email" class="form-control" name="email" placeholder="Email">
                            {!! showErrors($errors,'email') !!}
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu:</label>
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
                            {!! showErrors($errors,'email') !!}
                        </div>
                        <div class="checkbox">
                            <label>
                                <input name="remember" type="checkbox"> Remember Me
                            </label>
                            <label class="pull-right">
                                <a href="{{ route('admin.get.forgot.password') }}">Quên mật khẩu?</a>
                            </label>

                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Đăng nhập</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="backend/assets/admin/vendors/jquery/dist/jquery.min.js"></script>
    <script src="backend/assets/admin/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="backend/assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="backend/assets/admin/js/main.js"></script>


</body>

</html>
