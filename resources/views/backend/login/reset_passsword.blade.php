<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Quên mật khẩu</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">


    <link rel="stylesheet" href="backend/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="backend/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="backend/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="backend/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="backend/vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="backend/assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>

<body class="bg-dark">

    @if (session('danger'))
    <script>
        notify("<div style='font-size:15px'><i style='line-height: 20px;' class='fa ffa fa-exclamation-circle'><i/> {{ session('danger') }} </div>",
            'error');

    </script>
    @endif

    @if (session('success'))
        <script>
            notify("<div style='font-size:15px'><i style='line-height: 20px'; class='fa fa-thumbs-up'><i/> {{ session('success') }} </div>",'success');
        </script>
    @endif

    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="images/logo.png" alt="">
                    </a>
                </div>
                <div class="login-form">
                    <form action="" method="POST">
                        @csrf
                        @if(session('success'))
                            <div style='font-size:15px;margin-top:10px;color:#28a745'> {{ session('success') }} </div>
                        @endif
                        @if(session('danger'))
                            <div style='font-size:15px;margin-top:10px;color:red'> {{ session('danger') }} </div>
                        @endif
                        <div class="form-group">
                            <label>Mật khẩu mới <i>(ít nhất 6 ký tự)</i></label>
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu mới">
                            {!! showErrors($errors,'password') !!}
                        </div>
                        <div class="form-group">
                            <label>Nhập lại mật khẩu</label>
                            <input type="passwordConfirmation" class="form-control" name="passwordConfirmation" placeholder="Nhập lại mật khẩu">
                            {!! showErrors($errors,'passwordConfirmation') !!}
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Lấy lại mật khẩu</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="backend/assets/admin/vendors/jquery/dist/jquery.min.js"></script>
    <script src="backend/assets/admin/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="backend/assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="backend/assets/admin/js/main.js"></script>


</body>

</html>
