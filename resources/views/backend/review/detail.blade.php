@extends('backend.master.master')
@section('title', 'Chi tiết review')
@section('review', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Review</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.review') }}">Review</a></li>
                    <li class="active">Chi tiết</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><a href="{{ route('admin.get.list.review') }}"><< Quay lại</a></div>
                    <div class="card-body card-block">
                      <label for=""> Nội dung:</label>
                        <p>{!! $review->content !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection
