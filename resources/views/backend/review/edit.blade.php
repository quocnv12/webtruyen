@extends('backend.master.master')
@section('title', 'Sửa review')
@section('review', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Review</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.review') }}">Review</a></li>
                    <li class="active">Sửa</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Sửa review</div>
                    <div class="card-body card-block">
                        <form method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Tiêu đề:</label>
                                <input type="text" name="title" id="company" value="{{ $review->title }}" placeholder="Nhập tiêu đề" class="form-control">
                                {!! showErrors($errors,'title') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Tác giả:</label>
                                <input type="text" name="author" id="company" value="{{ $review->author }}" placeholder="Nhập tên tác giả" class="form-control">
                                {!! showErrors($errors,'author') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Tóm tắt:</label>
                                <textarea name="summary" id="" value="" class="form-control" cols="30" rows="5">{{ $review->summary }}</textarea>
                                {!! showErrors($errors,'summary') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Nội dung:</label>
                                <textarea name="content" value="" id="content-review-edit" class="form-control" cols="30" rows="15">{!! $review->content !!}</textarea>
                                {!! showErrors($errors,'content') !!}
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Hình ảnh </label>
                                <input type="file" id="image" value="{{ old('image') }}" name="image" onchange="showIMG()">
                            </div>
                            <div class="form-group">
                                {!! showErrors($errors,'image') !!}
                                <label for="" style="margin-left: 10px">Ảnh hiển thị : </label>
                                <div id="viewImg">
                                    <img src="frontend/images/{{ $review->image }}" width="200px" height="110px">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp; Sửa</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection
@section('js')
    <script>
        function showIMG() {
            var fileInput = document.getElementById('image');
            var filePath = fileInput.value; //lấy giá trị input theo id
            var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i; //các tập tin cho phép
            //Kiểm tra định dạng
            if (!allowedExtensions.exec(filePath)) {
                alert('Bạn chỉ có thể dùng ảnh dưới định dạng .jpeg/.jpg/.png/.gif extension.');
                fileInput.value = '';
                return false;
            } else {
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        document.getElementById('viewImg').innerHTML =
                            '<img style="width:200px; height: 110px;" src="' + e.target.result + '"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }

    </script>
@endsection
