<header id="header" class="header">

    <div class="header-menu">

        <div class="col-sm-7">
        </div>

        <div class="col-sm-5">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" src="backend/images/admin.jpg" alt="User Avatar">
                </a>

                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="{{ route('admin.get.profile',['id' => Auth::user()->id]) }}"><i class="fa fa-user"></i> Tài khoản</a>

                    <a class="nav-link" href="{{ route('admin.get.change.password') }}"><i class="fa fa-cog"></i> Đổi mật khẩu</a>

                    <a class="nav-link" href="{{ route('admin.get.logout') }}"><i class="fa fa-power-off"></i> Đăng xuất</a>
                </div>
            </div>

        </div>
    </div>

</header>