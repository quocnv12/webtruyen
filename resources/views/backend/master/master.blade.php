<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <base href="{{ asset('') }}">
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="backend/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="backend/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="backend/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="backend/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="backend/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="backend/vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="backend/assets/css/style.css">
    <link rel="stylesheet" href="backend/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="backend/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


    {{--  noty  --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
    <script src="{{ asset('asset/js/notify.js') }}"></script>
</head>

<body>

    @if (session('success'))
        <script>
            notify("<div style='font-size:15px'><i style='line-height: 20px'; class='fa fa-thumbs-up'><i/> {{ session('success') }} </div>",'success');
        </script>
    @endif

    @if (session('danger'))
        <script>
            notify("<div style='font-size:15px'><i style='line-height: 20px;' class='fa ffa fa-exclamation-circle'><i/> {{ session('danger') }} </div>",'error');
        </script>
    @endif

    @include('backend.master.sidebar')
    @include('backend.master.header')
    @yield('content')


    </div>
    <script src="backend/vendors/jquery/dist/jquery.min.js"></script>
    <script src="backend/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="backend/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="backend/assets/js/main.js"></script>


    {{-- <script src="backend/vendors/chart.js/dist/Chart.bundle.min.js"></script> --}}
    {{-- <script src="backend/assets/js/dashboard.js"></script> --}}
    {{-- <script src="backend/assets/js/widgets.js"></script> --}}
    {{-- <script src="backend/vendors/jqvmap/dist/jquery.vmap.min.js"></script> --}}
    <script src="backend/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    {{-- <script src="backend/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script> --}}


    <script src="backend/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="backend/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="backend/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="backend/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="backend/vendors/jszip/dist/jszip.min.js"></script>
    <script src="backend/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="backend/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="backend/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="backend/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="backend/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="backend/assets/js/init-scripts/data-table/datatables-init.js"></script>


    <script src="asset/ckfinder/ckfinder.js" type="text/javascript"></script>
    <script src="asset/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script>
            CKEDITOR.replace('content-review');
            CKEDITOR.replace('content-review-edit');
            CKEDITOR.replace('content-chapter');
            CKEDITOR.replace('content-chapter-edit');
            CKEDITOR.replace('content-short-story');
            CKEDITOR.replace('content-short-story-edit');
    </script>
    @yield('js')

</body>

</html>
