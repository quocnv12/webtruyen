@extends('backend.master.master')
@section('title', 'Add Stories')
@section('story', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Chapter</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.chapter',['id' => $id]) }}">Chapter</a></li>
                    <li class="active">Sửa</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Thêm chương</div>
                    <div class="card-body card-block">
                        <form method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group"><label for="company" class=" form-control-label">Tên chương:</label>
                                        <input type="text" name="name_chapter" id="company" value="{{ $chapter->name_chapter }}" placeholder="ex. Chương 1" class="form-control">
                                        {!! showErrors($errors,'name_chapter') !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"><label for="company" class=" form-control-label">Nội dung tên chương:</label>
                                        <input type="text" name="name" id="company" value="{{ $chapter->name }}" placeholder="Nhập nội dung tên chương" class="form-control">
                                        {!! showErrors($errors,'name') !!}
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Nội dung:</label>
                                <textarea name="content" id="content-chapter-edit" value="" class="form-control" cols="30" rows="5">{{ $chapter->content }}</textarea>
                                {!! showErrors($errors,'content') !!}
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp; Sửa</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection

