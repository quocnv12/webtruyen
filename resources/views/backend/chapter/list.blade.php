@extends('backend.master.master')
@section('title', 'List Chapter')
@section('story', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Chapter</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.chapter',['id' => $id]) }}">Chapter</a></li>
                    <li class="active">Danh sách</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('admin.get.add.chapter',['id' => $id]) }}"><strong class="card-title"><i class="fa fa-plus"></i>&nbsp; Thêm chapter</strong></a>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên chương</th>
                                    <th>Nội dung tên chương</th>
                                    <th>Nội dung</th> 
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($story->chapters as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $item->name_chapter }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{!! $item->content !!}</td>
                                    <td>
                                        {{-- <a href="{{ route('admin.get.add.chapter',['id' => $item->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-plus"></i>&nbsp; Add</a> --}}
                                        <a href="{{ route('admin.get.edit.chapter',['id' => $item->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-edit"></i>&nbsp; Edit</a>
                                        {{--  <a href="{{ route('admin.get.delete.chapter',['id' => $item->id]) }}" onclick="return confirm('Bạn có chắc chắn muốn xóa truyện!')"  class="btn btn-sm btn-link" data-button-type="delete"><i class="fa fa-trash"></i>&nbsp; Delete</a>                                        --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div>
@endsection