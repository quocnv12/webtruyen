@extends('backend.master.master')
@section('title', 'List Stories')
@section('story', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Story</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.story') }}">Story</a></li>
                    <li class="active">Danh sách</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Danh sách story</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên Truyện</th>
                                    <th>Tóm tắt</th>
                                    <th>Nguồn</th>
                                    <th>Trạng thái</th>
                                    <th>Active</th>
                                    <th>Tác giả</th>
                                    <th>Lượt xem</th>
                                    <th>Hiển thị</th>
                                    <th>Hình ảnh</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stories as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->summary }}</td>
                                    <td>{{ $item->source }}</td>
                                    <td>
                                        @if ($item->status == 1)
                                            {{ 'Đang cập nhật' }}
                                        @else
                                            {{ 'Hoàn thành' }}
                                        @endif
                                    <td>
                                        @if ($item->active == 1)
                                            {{ 'VIP' }}
                                        @elseif($item->active == 2)
                                            {{ 'HOT' }}
                                        @else 
                                            {{ 'FULL' }}
                                        @endif
                                        
                                    </td>
                                    <td>{{ $item->author }}</td>
                                    <td>{{ $item->view }}</td>
                                    <td>
                                        @if($item->display == 1)
                                        {{ 'Đang hiển thị' }}
                                        @else
                                        {{ 'Chưa hiển thị' }}
                                        @endif    
                                    </td>
                                    <td><img src="frontend/images/{{ $item->image }}" width="250" height="55px" alt=""></td>
                                    <td>
                                        <a href="{{ route('admin.get.edit.story',['id' => $item->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-edit"></i>&nbsp; Edit</a>
                                        <a href="{{ route('admin.get.delete.story',['id' => $item->id]) }}" onclick="return confirm('Bạn có chắc chắn muốn xóa truyện!')"  class="btn btn-sm btn-link" data-button-type="delete"><i class="fa fa-trash"></i>&nbsp; Delete</a>                                     
                                        <a href="{{ route('admin.get.list.chapter',['id' => $item->id]) }}"  class="btn btn-sm btn-link" data-button-type="delete"><i class="fa fa-bars"></i>&nbsp; Chapter</a>                                     
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div>
@endsection