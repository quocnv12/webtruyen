@extends('backend.master.master')
@section('title', 'Edit Stories')
@section('story', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Story</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.story') }}">Story</a></li>
                    <li class="active">Sửa</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Sửa truyện</div>
                    <div class="card-body card-block">
                        <form method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group"><label for="company" class=" form-control-label">Tên truyện:</label>
                                <input type="text" name="name" id="company" value="{{ $story->name }}" placeholder="Nhập tên truyện" class="form-control">
                                {!! showErrors($errors,'name') !!}
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                      <input type="checkbox" {{ $story->display == 1 ? 'checked' : '' }} name="display"  value=""> Hiển thị
                                    </label>
                                  </div>
                            </div>
                            <div class="form-group"><label for="company" class=" form-control-label">Tác giả:</label>
                                <input type="text" name="author" id="company" value="{{ $story->author }}" placeholder="Nhập tên tác giả" class="form-control">
                                {!! showErrors($errors,'author') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Tóm tắt:</label>
                                <textarea name="summary" id="" value="" class="form-control" cols="30" rows="5">{{ $story->summary }}</textarea>
                                {!! showErrors($errors,'summary') !!}
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group"><label for="company" class=" form-control-label">Nguồn:</label>
                                        <input type="text" name="source" id="company" value="{{ $story->source }}" placeholder="Nhập nguồn" class="form-control">
                                        {!! showErrors($errors,'source') !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group"><label for="company" class=" form-control-label">Trạng thái:</label>
                                        <select name="status" id="" class="form-control">
                                            <option {{ $story->status == 1 ? 'selected' : '' }} value="1">Đang cập nhật</option>
                                            <option {{ $story->status == 2 ? 'selected' : '' }} value="2">Hoàn thành</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group"><label for="company" class=" form-control-label">Active:</label>
                                        <select name="active" id="" class="form-control">
                                            <option {{ $story->active == 1 ? 'selected' : '' }} value="1">Vip</option>
                                            <option {{ $story->active == 2 ? 'selected' : '' }} value="2">Hot</option>
                                            <option {{ $story->active == 3 ? 'selected' : '' }} value="3">Full</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <label for="">Thể loại:</label>
                            {!! showErrors($errors,'categories') !!}
                            <div class="row" style="padding: .375rem .75rem;">
                                @foreach ($categories as $item)
                                  <div class="col-sm-4">
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox" {{ $categories_of_story->contains($item->id) ? 'checked' :'' }} name="categories[]"  value="{{ $item->id }}">  {{ $item->name }}
                                        </label>
                                      </div>
                                  </div>
                                @endforeach
                              </div>
                              <hr>
                              <div class="form-group">
                                <label for="exampleInputFile">Hình ảnh </label>
                                <input type="file" id="image" value="{{ old('image') }}" name="image" onchange="showIMG()">
                            </div>
                            <div class="form-group">
                                {!! showErrors($errors,'image') !!}
                                <label for="" style="margin-left: 10px">Ảnh hiển thị : </label>
                                <div id="viewImg">
                                    <img src="frontend/images/{{ $story->image }}" width="200px" height="110px">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp; Sửa</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection
@section('js')
    <script>
        function showIMG() {
            var fileInput = document.getElementById('image');
            var filePath = fileInput.value; //lấy giá trị input theo id
            var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i; //các tập tin cho phép
            //Kiểm tra định dạng
            if (!allowedExtensions.exec(filePath)) {
                alert('Bạn chỉ có thể dùng ảnh dưới định dạng .jpeg/.jpg/.png/.gif extension.');
                fileInput.value = '';
                return false;
            } else {
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        document.getElementById('viewImg').innerHTML =
                            '<img style="width:200px; height: 110px;" src="' + e.target.result + '"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }

    </script>
@endsection
