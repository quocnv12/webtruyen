@extends('backend.master.master')
@section('title', 'Thông tin tài khoản')
@section('content')

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Thông tin tài khoản</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li class="active">Thông tin tài khoản</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">Thông tin tài khoản</div>
                    <div class="card-body card-block">
                        <form method="POST" action="">
                            @csrf
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Email</label>
                                <input type="email" name="email" id="company" value="{{ $user->email }}" placeholder="" class="form-control">
                                {!! showErrors($errors,'email') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Tên hiển thị</label>
                                <input type="text" name="name" id="company" value="{{ $user->name }}" placeholder="" class="form-control">
                                {!! showErrors($errors,'name') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Giới tính</label>
                                <div class="checkbox">
                                    <label>
                                      <input type="radio"{{ $user->gender == 1 ? 'checked' : '' }} name="gender"  value="1"> Nam
                                    </label>
                                    <label>
                                        <input type="radio"{{ $user->gender == 2 ? 'checked' : '' }} name="gender"  value="2"> Nữ
                                      </label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp; Cập nhật</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection
