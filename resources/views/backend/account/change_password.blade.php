@extends('backend.master.master')
@section('title', 'Đổi mật khẩu')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Đổi mật khẩu</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li class="active">Đổi mật khẩu</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">Đổi mật khẩu</div>
                    <div class="card-body card-block">
                        <form method="POST" action="{{ route('post.changer.password') }}">
                            @csrf
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Mật khẩu cũ</label>
                                <input type="password" name="password_old" id="company" value="{{ old('password_old') }}" placeholder="Mật khẩu cũ" class="form-control">
                                {!! showErrors($errors,'password_old') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Mật khẩu mới</label>
                                <input type="password" name="password" id="company" value="{{ old('password') }}" placeholder="Mật khẩu cũ" class="form-control">
                                {!! showErrors($errors,'password') !!}
                            </div>
                            <div class="form-group">
                                <label for="company" class=" form-control-label">Nhập lại mật khẩu mới</label>
                                <input type="password" name="password_confirmation" id="company" value="{{ old('password_confirmation') }}" placeholder="Mật khẩu cũ" class="form-control">
                                {!! showErrors($errors,'password_confirmation') !!}
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>&nbsp; Đổi mật khẩu</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection
