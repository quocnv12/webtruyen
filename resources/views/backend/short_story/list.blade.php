@extends('backend.master.master')
@section('title', 'Danh sách truyện ngắn')
@section('short_story', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Short Story</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.short.story') }}">Truyện ngắn</a></li>
                    <li class="active">Danh sách</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Danh sách truyện ngắn</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tiêu đề</th>
                                    <th>Tóm tắt</th>
                                    <th>Tác giả</th>
                                    <th>Hình ảnh</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($short_story as $key => $item)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->summary }}</td>
                                    <td>{{ $item->author }}</td>
                                    <td><img src="frontend/images/{{ $item->image }}" width="300px" height="100px" alt=""></td>
                                    <td>
                                        <a href="{{ route('admin.get.edit.short.story',['id' => $item->id]) }}" class="btn btn-sm btn-link"><i class="fa fa-edit"></i>&nbsp; Edit</a>
                                        <a href="{{ route('admin.get.delete.short.story',['id' => $item->id]) }}" onclick="return confirm('Bạn có chắc chắn muốn xóa truyện ngắn!')"  class="btn btn-sm btn-link" data-button-type="delete"><i class="fa fa-trash"></i>&nbsp; Delete</a>                                     
                                        <a href="{{ route('admin.get.detail.short.story',['id' => $item->id]) }}" class="btn btn-sm btn-link" data-button-type="delete"><i class="fa fa-eye"></i>&nbsp; Detail</a>                                     
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div>
@endsection