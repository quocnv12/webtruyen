@extends('backend.master.master')
@section('title', 'Chi tiết truyện ngắn')
@section('short_story', 'show')
@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Truyện ngắn</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('admin.index') }}">Trang chủ</a></li>
                    <li><a href="{{ route('admin.get.list.short.story') }}">Truyện ngắn</a></li>
                    <li class="active">Chi tiết</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><a href="{{ route('admin.get.list.short.story') }}"><< Quay lại</a></div>
                    <div class="card-body card-block">
                      <label for=""> Nội dung:</label>
                        <p>{!! $short_story->content !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
@endsection
