/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

};

CKEDITOR.editorConfig = function( config ) {
    config.filebrowserBrowseUrl = 'asset/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = 'asset/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl = 'asset/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl = 'asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = 'asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = 'asset/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};


