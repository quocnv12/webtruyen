function notify(text,type)
{
    new Noty({
        text,
        type,
        theme:"bootstrap-v4",
        timeout:3500,
        progressBar:true,
    }).show();
    
}