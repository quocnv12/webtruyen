if(typeof wtgetCookie != 'function'){
    function wtgetCookie(name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        }
        else
        {
            begin += 2;
            var end = document.cookie.indexOf(";", begin);
            if (end == -1) {
                end = dc.length;
            }
        }
        return decodeURI(dc.substring(begin + prefix.length, end));
    }
}
if(typeof wtsetCookie != 'function'){
    function wtsetCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}
var wt_ref = wtgetCookie("wt_ref");
if(wt_ref==null){
    wt_ref = document.referrer;
    wtsetCookie("wt_ref",wt_ref);
}
if(wt_ref!="" && window.location.href.match(/([a-zA-Z0-9-_]+)\/([a-zA-Z0-9-_]+)_([0-9]+)/)){
	var r = Math.round(Math.random());
	document.write('<script src=\'https://www.googletagservices.com/tag/js/gpt.js\'>\n' +
        '  googletag.pubads().definePassback(\'/21805296964/Webtruyen_footer\', [1, 1]).display();\n' +
        '</script>');
}

