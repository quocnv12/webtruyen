$(document).ready(function () {
    setTimeout(function(){
	        window.fbAsyncInit = function() {
		    FB.init({
		    appId      : '212174092286047',
		    cookie     : true,  // enable cookies to allow the server to access 
		    xfbml      : true,  // parse social plugins on this page
		    version    : 'v5.0' // use graph api version 2.12
		});
		
		};
		// Load the SDK asynchronously
		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
    }, 5000);
    
});



$('#diverro').hide();
function openTab(evt, tab) {
	var i, x, tablinks;
	x = document.getElementsByClassName("comment-tab");
	for (i = 0; i < x.length; i++) {
	 x[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablink");
	for (i = 0; i < x.length; i++) {
	 tablinks[i].className = tablinks[i].className.replace(" w3-border-blue", "");
	}
	document.getElementById(tab).style.display = "block";
	evt.currentTarget.firstElementChild.className += " w3-border-blue";
}
function loadListComment() {
	var ajaxInProgress = $('#comment-flag').val();
	if(ajaxInProgress == 'false') return;
	$('div#lastPostsLoader').html('<img src="'+domainurl+'/frontend/images/loading.gif"/>');
    var StoryID     = $('#storyid-hidden').val();//comment-page
    var Page        = $('#comment-page').val();
    var Cate        = $('#comment-cate').val();
    Page = parseInt(Page) + 1;
    var url= domainurl+"/method/load_comment/"+StoryID+"/"+Page+"/"+Cate;
    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        beforeSend : function (){
            $('#comment-flag').val('false');
        },
        success:function(html){
            if(html != 'false')
            {
                $('#comment-page').val(Page);
                $('#comment-list').append(html);
            }
            else
            {
	            $('#loadMoreComment').empty();
            }
            $('div#lastPostsLoader').empty();
            $('#comment-flag').val('true');
        },
        error:function(){
            //alert('false');
            $('div#lastPostsLoader').empty();
            $('#comment-flag').val('true');
        }
    });
};


function btcomment()
{
	var currentdate = new Date(); 
    var datetime = currentdate.getHours() + ":" + currentdate.getMinutes() + " " + currentdate.getDate() + "/" + (currentdate.getMonth()+1) + "/" + currentdate.getFullYear() ;
    var StoryID     = $('#storyid-hidden').val();
    var Comment     = $('#txtcm').val();
    var Cate        = $('#comment-cate').val();
    
    if(Comment.length < 5 || Comment.length > 500)
    {
    	alert("Xin vui lòng nhập >5 và <500!! Cảm ơn");return;
    }
    
    var formData = {
    			'btcm': 'ok',
    			'notinput': '',
                'storyid': StoryID,
                'cate' : Cate,
                'comment' : Comment,
                
            };
    var url= domainurl+"/method/comment";
    $.ajax({
        type: "POST",
        url: url,
        data: formData,
        cache: false,
        beforeSend : function (){
            $('#name').val('');
			$('#email').val('');
			$('#txtcm').val('');
			$("#ccm").html('0');
        },
        success:function(html){
            if(html != 'false')
            {
                $('#comment-list').prepend(html);
                /*alert(html);*/
            }
        },
        error:function(){
            alert('Xảy ra lỗi, vui lòng thử lại sau!!');
        },
    });};
/*$('#numcapcha1').val(Math.floor((Math.random()*10)+1));
$('#numcapcha2').val(Math.floor((Math.random()*10)+1));
$('#result').val('');
function checkvailcm() {
    var txt = $("#txtcm").val();
    if(txt.length > 5 && txt.length < 500)
    { 
		if(parseFloat($('#numcapcha1').val()) + parseFloat($('#numcapcha2').val()) != parseFloat($('#result').val())) 		{
            $('#numcapcha1').val(Math.floor((Math.random()*10)+1));
            $('#numcapcha2').val(Math.floor((Math.random()*10)+1));
            $('#diverro').show();
            $('#lberro').text('Mã xác nhận không hợp lệ!');
            return false;
        }
        var StoryID     = $('#storyid-hidden').val();
        var Name        = $('#name').val();
        var Email       = $('#email').val();
        var Comment     = $('#txtcm').val();
        var Cate        = $('#comment-cate').val();
        alert('Bình luận của bạn đã được gửi, chúng tôi sẽ kiểm tra và duyệt sóm nhất có thể!');
        return true;
    }
    else{$("#btcm").attr("disabled", "disabled");$('#diverro').show();$('#lberro').text('Lời bình giới hạn từ 5 đến 500 ký tự');return false;}
};
$('#txtcm').click(function(event) {
	$('.comment-hide').slideDown('fast');
});*/



$("#txtcm").keyup(function(){
    var txt = $("#txtcm").val();        
    if(txt.length > 5 && txt.length < 500)
    {
        $("#btcm").removeAttr("disabled");
    }
    else
    {
        $("#btcm").attr("disabled", "disabled");
    }
    $("#ccm").html(txt.length);
});
$("#rscm").keyup(function(){
    $('#numcapcha1').val(Math.floor((Math.random()*10)+1));
	$('#numcapcha2').val(Math.floor((Math.random()*10)+1));
	$('#result').val('');
	$('#name').val('');
	$('#email').val('');
	$('#txtcm').val('');
	$("#ccm").html('0');
});
function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
};