//changeBackground(readCookie('backgoundcolor'));
//setsizeText(readCookie('fontsize'));
// SLIDE

if (document.getElementsByClassName('slides-img').length)
{
	var slideIndex = 1;
	showDivs(slideIndex);
	function plusDivs(n) {
	  showDivs(slideIndex += n);
	}
}
// CHANGE BACKGROUND
function changeBackground(bg) {
	if (bg == 'black') {
		document.getElementById("reading").style.background = "#333";
		document.getElementById("reading").style.color = "#eee";						
	}
	else {
		document.getElementById("reading").style.background = "#fff";
		document.getElementById("reading").style.color = "#333";	
	}
	createCookie('backgoundcolor', bg);
}

// RESIZE TEXT
//function resizeText(zoom) {
//	var fontsize = 20;
//	if (document.getElementById("content").style.fontSize == "") { fontsize = 20; } else { fontsize =  parseFloat(document.getElementById("content").style.fontSize);}
//	fontsize = fontsize + (zoom * 2);
	
//	document.getElementById("content").style.fontSize = fontsize+"px";
//	createCookie('fontsize', fontsize);
//}
function setsizeText(size) {
	document.getElementById("content").style.fontSize = size+"px";
}
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function loadListChap() {

    document.getElementById('list-drop').classList.toggle("w3-show");
    document.getElementById('list-drop1').classList.toggle("w3-show");
    var bid = $('#bookid-hidden').val(); 
	var url= domainurl+"/method/loadListChap?bid="+bid;
	
	$.ajax({type: "POST",
			url: url,
			cache: false,
			beforeSend:function(){$("#list-drop").html("");$("#list-drop1").html("");},
			success:function(html){$("#loadListChap").css("display", 'none'); $('#list-drop').html('<p>Danh Sách Chương: <select class="select-box w3-border" onchange="listchapter_select()" name="droplistchapter" id="droplistchapter">'+html+'</select></p>').show();
			$("#loadListChap1").css("display", 'none'); $('#list-drop1').html('<p>Danh Sách Chương: <select class="select-box w3-border" onchange="listchapter_select1()" name="droplistchapter1" id="droplistchapter1">'+html+'</select></p>').show();	
			
			},
			error:function(){$('#list-drop').html('Fails').show();}
		});
};

function listchapter_select(){
    var url = $("#droplistchapter").select().val();
    window.location.replace(url);
};
function listchapter_select1(){
    var url = $("#droplistchapter1").select().val();
    window.location.replace(url);
};

function report_chapter(){
  var report = prompt('Vui lòng nhập mô tả lỗi','');
  if(report == null){
    return;
  }else if(report != ''){
    var storyid = $('#storyid-hidden').val();
    var chapid = $('#bookid-hidden').val();
    $.ajax({type: "POST",
            data : {'report':report,'storyid':storyid,'chapid':chapid},
			url: domainurl+"/method/chaperror/",
			cache: false,
			success:function(){$('#chaperror').remove();alert('Cảm ơn bạn đã thông báo lỗi!');},
		});
  }else{
    alert('Vui lòng nhập mô tả và thông báo lại nhé!');
  }
};



if(readCookie('history_read') != null)
{
	var StoryID     = $('#storyid-hidden').val();
	var bookID      = $('#bookid-hidden').val();
	var ck = readCookie('history_read');
	var ar = ck.split('|');
	var arstoryid = ar[0].split(',');
	var arbookid = ar[1].split(',');
	
	
	
	var i;
	for (i = 0; i < arstoryid.length; ++i) {
	    // do something with `substr[i]`
	    if(arstoryid[i] == StoryID)
	    {
		    arstoryid.splice(i,1);
		    arbookid.splice(i,1);
	    }
	}
	
	if(arstoryid.length >= 10) 
	{
		arstoryid.splice(0,1);
		arbookid.splice(0,1);
	}
	
	var strsid = "";
	var strbid = "";
	if(arstoryid.length >= 1)
	{
		strsid = arstoryid.toString()+","+StoryID;
		strbid = arbookid.toString()+","+bookID;
	}
	else 
	{
		strsid = StoryID;
		strbid = bookID;
	}
	var hr = strsid+'|'+strbid;
	createCookie("history_read", hr );
}
else 
{
	var StoryID     = $('#storyid-hidden').val();
	var bookID      = $('#bookid-hidden').val();
	var hr = StoryID+'|'+bookID;
	createCookie('history_read', hr);
}
$(window).keydown(function(e) {
  switch (e.keyCode) {
    case 37: if($('#prevchap').length <= 0) { alert('Bạn đang ở chương 1 rồi, sao lùi chương được nữa ^^!'); } else{ var url = $('#prevchap').attr('href'); window.location= (url); } return;// left arrow key
    case 39: if($('#nextchap').length <= 0) { alert('Hiện tại không tìm thấy chương kế tiếp!'); } else{ var url = $('#nextchap').attr('href'); window.location= (url); } return;// right arrow key
  }
});

$("#maunen").change(function(){
    var maunen = $("#maunen option:selected").val();
    if(maunen != "262626"){$('#reading').css("background-color","#"+maunen);$('#reading').css("color","#2B2B2B");
    }else{$('#reading').css("background-color","#"+maunen);$('#reading').css("color","#c0c4c9");}
    createCookie('backgoundcolor',"#"+maunen,360);
});
function resizeText(zoom){
    var fontsize = parseFloat($("#content").css("font-size"));
	fontsize = fontsize+zoom;
	$("#content").css("font-size",fontsize+"px");
	createCookie('fontsize', fontsize+"px", 360);
}
$("#fontfa").change(function(){
    var font = $("#fontfa option:selected").val();
    $('#content').css("font-family",font+",serif");
    createCookie('fontfa', font+",serif",360);
});
$("#fonthe").change(function(){
    var height = $("#fonthe option:selected").val();
    $('#content').css("line-height",height+"%");
    createCookie('fonthe', height+"%",360);
});

function doctruyenvip(book_id){
    $.ajax({type: "GET",
			url: domainurl+"/user/mt_credits/"+book_id,
			cache: false,
			beforeSend : function (){$('#divcontent').html('');},
			success:function(html){$('#divcontent').html(html);},
		});

};