changeBackground(readCookie('backgoundcolor'));
setsizeText(readCookie('fontsize'));
// CHANGE BACKGROUND
function changeBackground(bg) {
	if (bg == 'black') {
		document.getElementById("reading").style.background = "#333";
		document.getElementById("reading").style.color = "#eee";						
	}
	else {
		document.getElementById("reading").style.background = "#fff";
		document.getElementById("reading").style.color = "#333";	
	}
	createCookie('backgoundcolor', bg);
}

// RESIZE TEXT
function resizeText(zoom) {
	var fontsize = 20;
	if (document.getElementById("content").style.fontSize == "") { fontsize = 20; } else { fontsize =  parseFloat(document.getElementById("content").style.fontSize);}
	fontsize = fontsize + (zoom * 2);
	
	document.getElementById("content").style.fontSize = fontsize+"px";
	createCookie('fontsize', fontsize);
}
function setsizeText(size) {
	document.getElementById("content").style.fontSize = size+"px";
}
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
$(function() {
    var readdddonly;
    readdddonly = $('#default-demo').attr('data-readonly');
    $('#default-demo').raty({ 
        readOnly: readdddonly,
        half: true,
        number: 10,
        numberMax: 10,
        score: function() {return $(this).attr('data-score');},
        click: function(score, evt) {
            var rate = $(this).attr('data-score');
            var StoryID     = $('#hiddenID').val();
            var CateRate    = $('#hiddenCateRate').val();
            var Ratimg      = score;
            var url=domainurl+"/method/rating?sid="+StoryID+"&rate="+score+"&cate="+CateRate;
            $.ajax({
                type: "POST",
                url: url,
                cache: false,
                success:function(html){
                    countrate = parseFloat($('#countrate').text()) + 1;
                    $('#countrate').text(countrate);
                    $('#averagerate').text(html);
                    $('#default-demo').raty({
                        readOnly: true,
                        half: true,
                        number: 10,
                        numberMax: 10,
                        score: html
                    });
                },
                error:function(){
                    $('#default-demo').raty({
                        readOnly: true,
                        half: true,
                        number: 10,
                        numberMax: 10,
                        score: rate,
                    });
                }
            });
        }
    });
});