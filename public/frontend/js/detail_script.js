function gotodschuong()
{
	$('html, body').animate({ scrollTop: $("#divtab").offset().top }, 'slow');
};
function bt_a_d_fs(act)
{
	if(act == "delete")
		act = "mt_del_favorite";
	else
		act = "mt_add_favorite";
	
	var StoryID     = $('#hiddenID').val();
	$.ajax({
	    type:"POST",
	    url: domainurl+"/user/"+act+"/"+StoryID,
	    cache:false,
	    success:function(html){
	    	if(html == "fail_login")
	    	{
		    	openModal('login');
	    	}
	    	else if(html == "add_success")
	    	{	
	    		alert("Đã thêm thành công truyện vào danh sách truyện yêu thích!");
		    	$('#bt_a_d_fs').attr("onclick","bt_a_d_fs('delete')");
		    	$('#bt_a_d_fs').html("<i class='fa fa-heart-o'></i> Bỏ Yêu Thích");
	    	}
	    	else if(html == "del_success")
	    	{
	    		alert("Đã xoá truyện ra khỏi danh sách truyện yêu thích!");
		    	$('#bt_a_d_fs').attr("onclick","bt_a_d_fs('add')");
		    	$('#bt_a_d_fs').html("<i class='fa fa-heart-o'></i> Thêm vào danh sách truyện Yêu Thích");
	    	}
	    },
	});
	
};
$('article').readmore({speed: 500});
$(function() {
    var readdddonly;
    readdddonly = $('#default-demo').attr('data-readonly');
    $('#default-demo').raty({ 
        readOnly: readdddonly,
        half: true,
        number: 10,
        numberMax: 10,
        score: function() {return $(this).attr('data-score');},
        click: function(score, evt) {
            var rate = $(this).attr('data-score');
            var StoryID     = $('#hiddenID').val();
            var CateRate    = $('#hiddenCateRate').val();
            var Ratimg      = score;
            var url=domainurl+"/method/rating?sid="+StoryID+"&rate="+score+"&cate="+CateRate;
            //alert(url);
            $.ajax({
                type: "POST",
                url: url,
                cache: false,
                success:function(html){
                    countrate = parseFloat($('#countrate').text()) + 1;
                    $('#countrate').text(countrate);
                    $('#averagerate').text(html);
                    $('#default-demo').raty({
                        readOnly: true,
                        half: true,
                        number: 10,
                        numberMax: 10,
                        score: html
                    });
                },
                error:function(){
                    $('#default-demo').raty({
                        readOnly: true,
                        half: true,
                        number: 10,
                        numberMax: 10,
                        score: rate,
                    });
                }
            });
        }
    });
});
