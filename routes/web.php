<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//user đăng kí
Route::get('register','frontend\AccountController@getRegister')->name('get.register.user');
Route::post('register','frontend\AccountController@postRegister')->name('post.register.user');
Route::post('register-modal','frontend\AccountController@postRegisterModal')->name('post.register.modal.user');

//user đăng nhập
Route::get('login','frontend\LoginController@getLogin')->name('get.login.user');
Route::post('login','frontend\LoginController@postLogin')->name('post.login.user');
Route::post('login-modal','frontend\LoginController@postLoginModal')->name('post.login-modal.user');
//đăng xuất
Route::get('logout','frontend\LoginController@getLogout')->name('get.logout.user');

//thông tin user
Route::get('info/{id}','frontend\AccountController@getInfo')->name('get.info.user');
Route::post('info/{id}','frontend\AccountController@postInfo')->name('post.info.user');

//Đổi mật khẩu
Route::get('changer-password','frontend\AccountController@getChangerPassword')->name('get.changer.password');
Route::post('changer-password','frontend\AccountController@postChangerPassword')->name('post.changer.password');

//quên mật khẩu
Route::get('forgot-password','frontend\ForgotPasswordController@getForgotPassword')->name('get.forgot.password');
Route::post('forgot-password','frontend\ForgotPasswordController@postForgotPassword')->name('post.forgot.password');

Route::get('reset-password','frontend\ForgotPasswordController@getResetPassword')->name('link.reset.password');
Route::post('reset-password','frontend\ForgotPasswordController@postResetPassword')->name('post.link.reset.password');


//trang chủ
Route::get('/', 'frontend\IndexController@getIndex')->name('index');

Route::group(['prefix' => 'review'], function () {
    Route::get('/','frontend\IndexController@getReview')->name('get.review');
    Route::get('{slug}','frontend\IndexController@getReviewDetail')->name('get.review.detail');
});

Route::group(['prefix' => 'truyen-ngan'], function () {
    Route::get('/','frontend\IndexController@getShortStory')->name('get.short.story');
    Route::get('{slug}','frontend\IndexController@getShortStoryDetail')->name('get.short.story.detail');
});

Route::get('truyen-duoc-xem-nhieu-nhat', 'frontend\IndexController@getAllStoryGood')->name('get.story.good');
Route::get('truyen-moi-dang', 'frontend\IndexController@getStoryNew')->name('get.story.new');
Route::get('truyen-full', 'frontend\IndexController@getStoryFull')->name('get.story.full');
Route::get('truyen-vip', 'frontend\IndexController@getStoryVip')->name('get.story.vip');
Route::post('tim-kiem', 'frontend\IndexController@getSearch')->name('post.search');

// Route::get('tim-kiem', function () {
//     return view('frontend.search');
// });
//-----------------------------------backend admin----------------------------------------

Route::get('login-admin', 'backend\LoginController@getLoginAdmin')->middleware('CheckLogout')->name('get.login.admin');
Route::post('login-admin', 'backend\LoginController@postLoginAdmin')->name('post.login.admin');

 //quên mật khẩu
 Route::get('forgot-password-admin','backend\AccountController@getForgotPassword')->name('admin.get.forgot.password');
 Route::post('forgot-password-admin','backend\AccountController@postForgotPassword')->name('admin.post.forgot.password');

 Route::get('reset-password-admin','backend\AccountController@getResetPassword')->name('admin.link.reset.password');
 Route::post('reset-password-admin','backend\AccountController@postResetPassword')->name('admin.post.link.reset.password');


Route::group(['prefix' => 'admin', 'middleware' => 'CheckLogin'], function () {

    Route::get('/', 'frontend\IndexController@getIndexAdmin')->name('admin.index');

    Route::get('profile/{id}','backend\AccountController@getProfile')->name('admin.get.profile');
    Route::post('profile/{id}','backend\AccountController@postProfile')->name('admin.post.profile');

    Route::get('change-password','backend\AccountController@getChangerPassword')->name('admin.get.change.password');

    Route::get('logout','backend\AccountController@getLogout')->name('admin.get.logout');

    //thể loại
    Route::group(['prefix' => 'category'], function () {
        Route::get('/','backend\CategoryController@getList')->name('admin.get.list.category');
        Route::get('add','backend\CategoryController@getAdd')->name('admin.get.add.category');
        Route::post('add','backend\CategoryController@postAdd')->name('admin.post.add.category');
        Route::get('edit/{id}','backend\CategoryController@getEdit')->name('admin.get.edit.category');
        Route::post('edit/{id}','backend\CategoryController@postEdit')->name('admin.post.edit.category');
        Route::get('delete/{id}','backend\CategoryController@delete')->name('admin.get.delete.category');
    });

    //stories
    Route::group(['prefix' => 'story'], function () {
        Route::get('/','backend\StoryController@getList')->name('admin.get.list.story');
        Route::get('add','backend\StoryController@getAdd')->name('admin.get.add.story');
        Route::post('add','backend\StoryController@postAdd')->name('admin.post.add.story');
        Route::get('edit/{id}','backend\StoryController@getEdit')->name('admin.get.edit.story');
        Route::post('edit/{id}','backend\StoryController@postEdit')->name('admin.post.edit.story');
        Route::get('delete/{id}','backend\StoryController@delete')->name('admin.get.delete.story');

        Route::group(['prefix' => 'chapter'], function () {
            Route::get('{id}','backend\ChapterController@getList')->name('admin.get.list.chapter');
            Route::get('add/{id}','backend\ChapterController@getAdd')->name('admin.get.add.chapter');
            Route::post('add/{id}','backend\ChapterController@postAdd')->name('admin.post.add.chapter');
            Route::get('edit/{id}','backend\ChapterController@getEdit')->name('admin.get.edit.chapter');
            Route::post('edit/{id}','backend\ChapterController@postEdit')->name('admin.post.edit.chapter');
            // Route::get('delete/{id}','backend\ChapterController@delete')->name('admin.get.delete.chapter');

        });

    });

    //review
    Route::group(['prefix' => 'review'], function () {

        Route::get('/','backend\ReviewController@getList')->name('admin.get.list.review');
        Route::get('detail/{id}','backend\ReviewController@getDetail')->name('admin.get.detail.review');
        Route::get('add','backend\ReviewController@getAdd')->name('admin.get.add.review');
        Route::post('add','backend\ReviewController@postAdd')->name('admin.post.add.review');
        Route::get('edit/{id}','backend\ReviewController@getEdit')->name('admin.get.edit.review');
        Route::post('edit/{id}','backend\ReviewController@postEdit')->name('admin.post.edit.review');
        Route::get('delete/{id}','backend\ReviewController@delete')->name('admin.get.delete.review');
    });


    Route::group(['prefix' => 'short_story'], function () {

        Route::get('/','backend\ShortStoryController@getList')->name('admin.get.list.short.story');
        Route::get('detail/{id}','backend\ShortStoryController@getDetail')->name('admin.get.detail.short.story');
        Route::get('add','backend\ShortStoryController@getAdd')->name('admin.get.add.short.story');
        Route::post('add','backend\ShortStoryController@postAdd')->name('admin.post.add.short.story');
        Route::get('edit/{id}','backend\ShortStoryController@getEdit')->name('admin.get.edit.short.story');
        Route::post('edit/{id}','backend\ShortStoryController@postEdit')->name('admin.post.edit.short.story');
        Route::get('delete/{id}','backend\ShortStoryController@delete')->name('admin.get.delete.short.story');
    });
});


Route::get('{slug}','frontend\IndexController@getCategory')->name('get.category');
Route::get('{slug_chapter}/{slug}','frontend\IndexController@getDetailChapter')->name('get.detail.chapter');
