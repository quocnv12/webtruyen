<?php

use Illuminate\Database\Seeder;

class StoriesTabldeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stories')->delete();
        DB::table('stories')->insert([
            ['id' => 1,'display' => 1, 'view' =>0, 'name' => 'Kiếm Lai', 'slug' => str_slug('Kiếm Lai'), 'source' => 'webtruyen', 'status' => 1,'story_good'=> 1 , 'active' => 1, 'image' => 'amduongtamthusinhoiYlHfYzZr.jpg', 'author' => 'Phong Hỏa Hí Chư Hầu',
            'summary' => '
                Thiếu niên họ Trần, tên Bình An, mồ côi cha mẹ từ khi còn nhỏ. Đồ sứ của trấn Nê Bình rất nổi tiếng, vì vậy từ khi triều đại khai quốc tới nay, nó liền mang cái danh kèm trọng trách ‘Phụng chiếu trông coi việc đốt lò làm đố hiến tế lăng’, theo đó, triều đình còn phái cả quan viên tới đóng chốt quanh năm ở đây để giám sát công việc của các lò.
            ',
            'created_at' => now(), 
            'updated_at' => now(), 
            ],
            ['id' => 2,'display' => 1, 'view' =>0, 'name' => 'KHào Môn Vui Nhộn Vợ Nhỏ Đến Cậy Cửa', 'slug' => str_slug('Hào Môn Vui Nhộn Vợ Nhỏ Đến Cậy Cửa'), 'source' => 'webtruyen', 'status' => 2,'story_good'=> 1 , 'active' => 2, 'image' => 'amduongthanchuongGKZsJCBs9A.jpg', 'author' => 'Khốc Vựng Tại Xí Sở',
            'summary' => '
                Giới thiệu truyện ngôn tình hấp dẫn Hào Môn Vui Nhộn Vợ Nhỏ Đến Cậy Cửa của tác giả Khốc Vựng Tại Xí Sở (Khóc Ngất Ở Nhà Xí):

                Ninh thành, khách sạn năm sao KELOO.
                
                “Đi bắt gian với chị!” Mạc Tinh Nhu đưa một bức ảnh tới trước mặt Hứa Mễ Nặc.
            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],
            ['id' => 3,'display' => 1, 'view' =>0, 'name' => 'Cao Thủ Thâu Hương', 'slug' => str_slug('Cao Thủ Thâu Hương'), 'source' => 'webtruyen', 'status' => 1, 'story_good'=> 0 ,'active' => 3, 'image' => 'bisinhthutrunghR73PSRieG.jpg', 'author' => 'Lục Như Hòa Thượng',
            'summary' => '
                Tai nạn, xuyên không, đoạt xá trọng sinh, nhưng sẽ là ai đây? Trọng sinh vào một phế vật không thể tu luyện võ công của một gia tộc? Sai! Vậy trọng sinh vào thân xác của một đại thiếu gia khoẻ mạnh bình thường, cũng sai nốt! Vậy sẽ là ai?
            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],


            ['id' => 4,'display' => 1, 'view' =>0, 'name' => 'Thiên Tài Triệu Hồi Sư', 'slug' => str_slug('Thiên Tài Triệu Hồi Sư'), 'source' => 'qttp.wordpress.com', 'status' => 1,'story_good'=> 1 , 'active' => 2, 'image' => '12chomsaovalophoctinhyeuTdJipeeyoG.jpg', 'author' => 'Nhược Tuyết Tam Thiên',
            'summary' => '
                Truyện Thiên Tài Triệu Hồi Sư là một truyện mới được giới thiệu với bạn đọc trên trang đọc truyện online, một truyện với chủ đề xuyên qua thời gian nhưng không hề nhàm chán. Đọc truyện bạn đọc sẽ được dẫn dắt vào một thế giới mới lạ, những lần hóa kiếp chuyển mình, đọc truyện xuyên không này để giải đáp được thắc mắc.
            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],


            ['id' => 5,'display' => 1, 'view' =>0, 'name' => 'Toàn Chức Cao Thủ', 'slug' => str_slug('Toàn Chức Cao Thủ'), 'source' => 'alldiepall', 'status' => 1,'story_good'=> 1 , 'active' => 2, 'image' => 'bantraikylacuatoiuoURFQz7nh.jpg', 'author' => 'Hồ Điệp Lam',
            'summary' => '
                Truyện Toàn Chức Cao Thủ của tác giả Hồ Điệp Lam kể về một nhân vật chính đam mê game cực đỉnh và có vô số lý do để trục xuất hắn nhưng hắn vẫn còn niềm đam mê tìm ẩn!

            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],
            ['id' => 6,'display' => 1, 'view' =>0, 'name' => 'Cô Vợ Hợp Đồng Bỏ Trốn Của Tổng Giám Đốc', 'slug' => str_slug('Cô Vợ Hợp Đồng Bỏ Trốn Của Tổng Giám Đốc'), 'source' => 'webtruyen', 'status' => 2,'story_good'=> 1 , 'active' => 2, 'image' => 'baoboigiatrentroi6RHygwADM1.jpg', 'author' => 'Niêm Hoa Phất Liễu',
            'summary' => '
                Mời bạn đón đọc truyện Cô Vợ Hợp Đồng Bỏ Trốn Của Tổng Giám Đốc của tác giả Niêm Hoa Phất Liễu nói về Cố Hề Hề vì một kế hoạch mà lần đầu tiên của cô đã bị cướp đi.

                Mối tình ngọt ngào ân ái nay đã bị chôn vùi trong giá rét.
            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],

            ['id' => 7,'display' => 1, 'view' =>0, 'name' => 'Tu Chân Tứ Vạn Niên', 'slug' => str_slug('Tu Chân Tứ Vạn Niên'), 'source' => 'VIP', 'status' => 2,'story_good'=> 1 , 'active' => 1, 'image' => 'chuatechivuongTj4zTt43ox.jpg', 'author' => 'Ngọa Ngưu Chân Nhân',
            'summary' => '
            Nếu nói trước kia, những con người được xưng là tu chân giả rất quý hiếm thì bây giờ họ lại tràn lan khắp mọi nơi. Có thể dùng để đi du lịch, học tập, đi làm, đến mọi làng quê cũng có thể sử dụng.
            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],

            ['id' => 8,'display' => 1, 'view' =>0, 'name' => 'Kẻ Đến Từ Địa Ngục', 'slug' => str_slug('Kẻ Đến Từ Địa Ngục'), 'source' => 'VIP', 'status' => 2,'story_good'=> 1 , 'active' => 1, 'image' => 'chanlinhcuubienqycGFLzrLL.jpg', 'author' => 'Chính Nghĩa Địch',
            'summary' => '
                Kẻ Đến Từ Địa Ngục - Chính Nghĩa Địch

                Giới thiệu truyện khoa huyễn hấp dẫn này:
                
                Ta...
                
                Ta đang ở đâu...
                
                Nơi này là chỗ nào, địa ngục sao? ?
                
                Địa ngục.
                
                Ta từng nhìn thấy địa ngục chân chính.
            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],

            ['id' => 9,'display' => 1, 'view' =>0, 'name' => 'Đại Phản Phái Hệ Thống', 'slug' => str_slug('Đại Phản Phái Hệ Thống'), 'source' => 'Team Dịch Webtruyen', 'status' => 2,'story_good'=> 1 , 'active' => 1, 'image' => 'chanhtaluongdaoFGIcI1wbmK.jpg', 'author' => 'Thiên Sách Giáo Úy',
            'summary' => '
                Trùng sinh trở thành cao phú soái ở thế giới huyền huyễn! Thiên phú siêu tuyệt! Bối cảnh thâm hậu!

                Nhưng còn không chờ Tần Đình mừng thầm bao lâu thì lại phát hiện mình đã đắc tội khí vận chi tử trên thế giới này.
                
                Có điều hắn cũng không hoảng chút nào, ngươi có khí vận, ta có hệ thống!
                
                Đại phản phái hệ thống, cướp đoạt các loại khí vận, thành tựu con đường đại đạo vô địch của bản thân!
            ',
            'created_at' => now(), 
            'updated_at' => now(),
            ],

        ]);
    }
}
