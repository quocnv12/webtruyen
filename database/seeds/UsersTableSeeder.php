<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['id' => 1,'admin' => 0, 'name' => 'van', 'email' => 'van@gmail.com', 'password' => bcrypt('123456'), 'gender' => 2],
            ['id' => 2,'admin' => 0, 'name' => 'hoa', 'email' => 'hoa@gmail.com', 'password' => bcrypt('123456'), 'gender' => 2],
            ['id' => 3,'admin' => 0, 'name' => 'nam', 'email' => 'nam@gmail.com', 'password' => bcrypt('123456'), 'gender' => 1],
            ['id' => 4,'admin' => 1, 'name' => 'bac', 'email' => 'admin@gmail.com', 'password' => bcrypt('123456'), 'gender' => 2],
            ['id' => 5,'admin' => 1, 'name' => 'trung', 'email' => 'trung@gmail.com', 'password' => bcrypt('123456'), 'gender' => 1]
        ]);
    }
}
