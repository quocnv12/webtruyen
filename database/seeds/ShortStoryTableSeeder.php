<?php

use Illuminate\Database\Seeder;

class ShortStoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('short_story')->delete();
        DB::table('short_story')->insert([
            ['id' => 1,'title' => 'Ánh trăng và kẻ 2 lòng', 'slug' => str_slug('Ánh trăng và kẻ 2 lòng') , 'author' => 'Pipi Tất Màu', 'image' => 'aassanhtrangvakehailongnDWdcWAlo4.jpg',
                'content' => '
                Phần em.

                Trăng đêm nay tròn quá dỗi, ánh sáng thanh nhẹ soi vào lòng người. Hắn vẫn luôn gặp em vào những đêm tình như thế, hắn khẽ thì thầm vào tai em vài lời:
                
                - Sói thích bầu trời nhưng Sói cũng yêu ánh trăng.
                
                Hắn khẽ hôn lên trán em. Vẻ mặt em hân hoan ngọt ngào và tràn đầy hạnh phúc như hắn chẳng có nói gì, hắn chỉ một lòng yêu em. Em yêu hắn nhiều đến thế trong khi hắn lộ rõ bản chất của kẻ hai lòng? Tôi đang ghen đấy em à.
                
                Ai đó khẽ gọi hắn, một vài cơn gió cuốn ngang bầu trời, đám mây con che đi ánh trăng vàng vành vạnh. Cả phố phường nhộn nhịp chìm vào bóng tối lửng lơ. Hắn vuốt lại mái tóc em thật ân cần, trao cho em những cử chỉ đường mật nhất. Một cái véo nhẹ vào má, hay nụ cười say lòng có phần kiêu ngạo của hắn,... hắn muốn cho em biết một cuộc tình vụn trộm nó thú vị thế nào và đau đớn ra sao ấy. Mà sao em cứ mãi lầm lũi trong một tình yêu sẽ chia như thế, em có biết tôi đau thế nào không?
                ',

                'summary' =>'
                Sói là kẻ hai lòng. Vì tôi chỉ yêu riêng mình em, một tình yêu bi lụy, ray rứt và khốn khổ. Vì hắn chỉ yêu riêng mình cô ấy, một thứ tình yêu ngông cuồng, mạnh mẽ, và... có đôi chút hoang dại. Sói là kẻ hai lòng, em biết không?
                '
                
            ],
            ['id' => 2,'title' => 'Cầu Tôi Mãi Một Đời Yêu Em', 'slug' => str_slug('Cầu Tôi Mãi Một Đời Yêu Em') , 'author' => 'Nhà văn tóc rối và cún con', 'image' => 'aasscautoimaimotdoiyeuemPSAHvETDRI.jpg',
                'content' => '
                Trong một lần trên đường về nhà, vẫn là con đường cũ quen thuộc: Một tiệm sách cũ; một khu thu mua phế liệu và đồ đồng nát; một dãy nhà đã không còn mới mẻ gì, màu sắc thể hiện tro tàn lâu năm bị gió phớt từng đợt ngả màu u ám có phần kinh dị và một tiệm bán thú cưng. Một cảm giác mãnh liệt thôi thúc tôi dừng chân trước cửa tiệm thú cưng, bởi vì tôi chưa bao giờ cho một cái nhìn về cửa tiệmĐã rất nhiều lần tôi ghé vào tiệm sách cũ kĩ và mỗi lần bước ra lại có thêm một chồng sách mới ngang ngửa tầm nhìn tôi một cách chật vật. Và hiện tại căn phòng bé nhỏ của tôi đã tràn ngập sách cũ lẫn sách mới lúc nhúc những chỗ hóc hẻm.Cũng đã rất nhiều lần tôi có vô số cuộc trò chuyện với các cô chú khu phế liệu và đồng nát, và đã thuộc nằm lòng cái kiến trúc của dãy nhà cổ. Thế nhưng tôi chẳng bao giờ bước chân vào tiệm thú cưng. Bởi vì tôi không có nhu cầu nuôi thêm một người bạn mới nào nữa.
                ',
                
                'summary' =>'
                    Viết để gửi tặng chú cún thân thương, ra đi trong sự nỗi nhớ khôn nguôi của cô chủ nhỏ của nhiều năm về trước khi còn là một cô bé nhỏ.
                '
            ],
            ['id' => 3,'title' => 'Tình Yêu Vốn Dĩ Không Cần Quá Tự Cao', 'slug' => str_slug('Tình Yêu Vốn Dĩ Không Cần Quá Tự Cao') , 'author' => 'Huyền Trang Bất Hối', 'image' => 'aasstinhyeuvondikhongcanquatucaomuonocanhnhauhaydepbokieungaoxRxlv9enXJ.jpg',
                'content' => '
                Người đàn ông đấy hét lên nói: “Mệt lắm rồi, em muốn làm gì thì làm”.

                Cô gái đùng đùng đứng dậy bỏ về, nhưng bước chân và hành động lấy đồ trên bàn, hành động chậm đến mức không thể chậm hơn.
                
                Anh ta mặc kệ cô gái đang cầm đồ để đứng lên bỏ về, mặc kệ sự tức giận, vẫn thản nhiên ngồi gắp đồ ăn vào bát. Anh ta gắp đến bốn năm lần cũng không gắp được một miếng thức ăn nào, sau đó anh ta châm thuốc, chỉ một hành động bật lửa thôi mà cũng không thể làm nổi.
                
                Khi ấy, tôi mới thấy rõ ràng sự nực cười của tình yêu. Hai người đều rất yêu nhau, đều rất quan tâm đến sự hiện diện của đối phương nhưng lại vì thể diện và tự trọng của bản thân quá cao để rồi chẳng ai chịu nhún mình thỏa hiệp?
                
                Anh ta đợi cô gái dừng bước không bỏ về, cô gái đợi anh ta đứng lên kéo lại để thôi không đi nữa. Nhưng sau cùng, chẳng ai làm theo được suy nghĩ của chính mình, hai người đều lặng thinh và để chính sự ích kỷ của bản thân đưa tình cảm vào bế tắc.
                ',
                
                'summary' =>'
                Hai người đều rất yêu nhau, đều rất quan tâm đến sự hiện diện của đối phương nhưng lại vì thể diện và tự trọng của bản thân quá cao để rồi chẳng ai chịu nhún mình thỏa hiệp?
                '
            ],
            ['id' => 4,'title' => 'Phụ Nữ Khi yêu Phải Biết Bảo Vệ Mình', 'slug' => str_slug('Phụ Nữ Khi yêu Phải Biết Bảo Vệ Mình') , 'author' => 'Dung Mai', 'image' => 'aassphunukhiyeuphaibietbaoveminhomvDzbNZlp.jpg',
                'content' => '
                Xã hội hiện đại nên chuyện tình dục trước hôn nhân không còn xa lạ nữa. Tuy nhiên, điều đó đồng nghĩa với việc phụ nữ cần phải biết bảo vệ mình hơn. Tình dục không an toàn là đi kèm với biết bao nhiêu loại bệnh tật rồi chẳng may nhỡ có bầu thì phụ nữ vẫn là người thiệt thòi nhất.

                Tình dục là chuyện bình thường như mọi nhu cầu sinh hoạt khác của con người nhưng không có nghĩa là các cô gái nên buông thả, không biết bảo vệ bản thân. Nếu bạn trai không muốn sử dụng bao cao su thì hãy yêu cầu anh ấy đưa ra bằng chứng anh ấy khỏe mạnh, không nhiễm bất cứ loại bệnh truyền nhiễm nào. Còn nếu không có tốt nhất là dùng các biện pháp an toàn để bảo vệ bản thân. Một phút vui có thể để lại hậu quả rất lớn.
                ',
                
                'summary' =>'
                Tình dục là chuyện bình thường như mọi nhu cầu sinh hoạt khác của con người nhưng không có nghĩa là các cô gái nên buông thả, không biết bảo vệ bản thân.
                '
            ],
            ['id' => 5,'title' => 'Em Buông Tay Bởi Anh Không Biết Giữ', 'slug' => str_slug('Em Buông Tay Bởi Anh Không Biết Giữ') , 'author' => 'Pipi Tất Màu', 'image' => 'aassembuongtayboianhkhongbietgiuvjWdXJk7Lf.jpg',
                'content' => '
                Dưới mỗi căn nhà đều có một người đàn bà đang khóc. Cuộc đời người làm mẹ, làm vợ, làm dâu có bao nhiêu nỗi buồn và nỗi cô đơn mà ngay cả người chồng bên cạnh vẫn không thể thấu hiểu. Nhưng tạo hóa đã cho người đàn bà một sức mạnh rất lớn để vượt qua tất cả những nỗi cô đơn đó để nhìn con lớn lên, làm tròn thiên chức cho con được hạnh phúc.

                Đàn bà, không phải chỉ gặp một chút khó khăn là đã chịu đầu hàng. Họ kiên cường lắm. Chồng nhậu nhẹt, chồng lười biếng, chồng vô tâm… Buồn lắm, nhưng nhiều người vẫn tặc lưỡi bỏ qua vì vẫn còn tình cảm, vẫn mong muốn cho con mình lớn lên tròn một mái ấm. Chỉ đến khi, sức chịu đựng đã vượt quá giới hạn, những nỗi đau và sự tổn thương mà họ đã chịu đựng chạm đến ngưỡng tận cùng, họ sẽ rời đi. Đàn bà trước khi lựa chọn phương án buông tay người đàn ông mình từng thương yêu đã phải rất dằn vặt, rất đau đớn và khổ sở. Họ đã từng dò dẫm trong bóng đêm để tự hỏi: Còn phương án nào tốt hơn cho mình, cho con không? Khi tất cả mọi câu hỏi đều đi vào ngõ cụt, chia tay là lựa chọn cuối cùng.
                
                ',
                
                'summary' =>'
                Đâu có một người đàn bà nào đủ bao dung đến độ biết chồng tệ bạc vẫn rộng lòng tha thứ hết lần này đến lần khác. Đàn bà buông tay bởi đã quá tuyệt vọng, biết mình chẳng thể hy vọng gì thêm ở người đàn ông bên cạnh.
                '
            ],
           
        ]);
    }
}
