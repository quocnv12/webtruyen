<?php

use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('review')->delete();
        DB::table('review')->insert([
            ['id' => 1,'author' => 'Nhóm review Webtruyen.com', 'title' => 'Tổng hợp review 5 truyện trọng sinh đáng đọc nhất của “mọt” ngôn tình', 
             'slug' => str_slug('Tổng hợp review 5 truyện trọng sinh đáng đọc nhất của “mọt” ngôn tình'),
             'summary' => 'Trên đây là top 5 review truyện trọng sinh được “mọt” ngôn tình mê mẩn nhất. Bạn thấy truyện nào trong số những truyện trên đây đáng đọc nhất. Đừng quên bấm thích và để lại ý kiến của mình nhé.',
             'content' => '
             <p></p>
             <p><strong>Trọng sinh là thể loại có nhân vật chính chết đi vì lý do gì đó và được giữ nguyên linh hồn cũng như
                     kí ức ở ngay chính thân thể của người đó hoặc của một người khác tại thời điểm khác. Thể loại trọng sinh
                     luôn luôn nằm top truyện ngôn tình được nhiều “mọt” ngôn tình đọc nhất. Dưới đây chúng ta sẽ điểm qua 5
                     review truyện trọng sinh được cho là hay và chất lượng nhất.</strong></p>
             <h2>1. Truyện Bổ Thiên Ký - Hàn Vũ Ký</h2>
             <figure class="image"><img
                     src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-trong-sinh-bo-thien-ky.jpg"
                     alt="bo thien ky" width="700" height="344">
                 <figcaption>Ảnh bìa Truyện Bổ Thiên Ký của Hàn Vũ Ký</figcaption>
             </figure>
             <p>Bổ Thiên Ký của Hàn Vũ Ký là một trong những truyện trọng sinh mới được cập nhật, đã hoàn thành quá trình
                 dịch thuật. Được đánh giá là truyện trọng sinh nhẹ nhàng, hài hước. Bổ Thiên Ký nhận được đánh giá cao của
                 những người yêu thích và hay review truyện trọng sinh ngôn tình.</p>
             <p>Tác giả: Hàn Vũ Ký.</p>
             <p>Thể Loại: Tiên Hiệp, Ngôn Tình, Huyền Huyễn, <a href="https://webtruyen.com/xuyen-khong/" target="_blank"
                     rel="noopener">Xuyên Không</a>, Trọng Sinh, Cổ Đại</p>
             <p>Tóm tắt nội dung:</p>
             <p>Vinh Tuệ Khanh đang sống ngày qua ngày bình yên với cha mẹ, ông nội. Cuộc sống nơi sơn thôn trên núi Lạc Thần
                 tuy hẻo lánh mà tránh xa những xô bồ cuộc sống. Bất chợt một ngày biến cố ập tới, cha và ông của Vinh Tuệ
                 Khanh bị giết chết. Còn mẹ của cô bị Vương gia Nguỵ Nam Tâm bắt đi.&nbsp;</p>
             <p>Vinh Tuệ Khanh có linh căn là nhờ có cơ duyên. Nhưng trớ trêu thay lại bị Đoá Linh phu nhân hòng bắt về cướp
                 lấy linh căn của cô.</p>
             <p>Tuệ Khanh vướng phải rắc rối, rốt cuộc là cô phải làm sao để có thể tự cứu lấy chính mình, có thể giết kẻ thù
                 để cứu mẹ báo thù cho cha. Con đường tu tiên trắc trở gập ghềnh, rốt cuộc cô thể làm được không? Nên dừng
                 lại hay chọn bước tiếp?&nbsp;</p>
             <p>Đối diện với những thứ như công pháp, tố chất, cơ duyên hay linh căn, người tu tiên còn phải đối mặt với
                 những nữ phụ xuyên không. Điều này khiến Tuệ Khanh cảm thấy như áp lực đè nặng lên vai. Rốt cuộc là tại sao
                 ông trời lại lắm lỗ hổng đến thế, có lối nào dễ đi cho Tuệ Khanh hay không?</p>
             <p>Đọc ngay <a href="https://webtruyen.com/bo-thien-ky/" target="_blank" rel="noopener">Bổ Thiên Ký</a></p>
             <h2>2.&nbsp;&nbsp;Truyện Nàng Dâu Cực Phẩm - Du Nhân</h2>
             <figure class="image"><img
                     src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-trong-sinh-nang-dau-cuc-pham.jpg"
                     alt="nang dau cuc pham" width="700" height="697">
                 <figcaption>Ảnh bìa Truyện Nàng Dâu Cực Phẩm của Du Nhân</figcaption>
             </figure>
             <p>Tác giả: Du Nhân</p>
             <p>Thể loại: <a href="https://webtruyen.com/ngon-tinh/" target="_blank" rel="noopener">Ngôn Tình</a>, Trùng
                 Sinh.</p>
             <p>Tóm tắt nội dung:</p>
             <p>“Đàn ông ấy mà, kéo quần lên rồi là trở mặt vô tình ngay.” Ai đã đọc nàng dâu cực phẩm chắc chắn không thể
                 quên câu nói đầy bá đạo, cá tính mà cũng rất hài hước của cô nàng nhân vật chính.</p>
             <p>Đàm Hi - nhân vật chính chính thô tục mà lại nhanh mồm nhanh miệng. Kiếp trước cô chính là thiên tài chơi
                 chứng khoán. Vì bị kéo vào vụ án rửa tiền mà phải bỏ mạng nơi vực sâu. Kiếp này một lần sống lại, cô được là
                 Đàm Hi. Đàm Hi bị gả khi bản thân cô còn chưa đủ tuổi kết hôn. Trong một lần Đàm Hi bị Tần Thiên Lâm ác độc
                 đánh tới chết. Đàm Hi bỏ mạng, Viêm Hề sống những năm tháng còn lại của cuộc đời mà đáng lẽ Đàm Hi nên được
                 sống. Top review truyện trọng sinh không thể không có mặt Nàng Dâu Cực Phẩm</p>
             <p>Đọc <a href="https://webtruyen.com/nang-dau-cuc-pham/" target="_blank" rel="noopener">Nàng Dâu Cực Phẩm</a>
             </p>
             <h2>3.&nbsp;Truyện&nbsp;Dạo Bước Phồn Hoa - Vân Nghê&nbsp;</h2>
             <figure class="image"><img style="width: 699px; height: 988px;"
                     src="https://img.webtruyen.com/public/images/reviews_img/20200131/dao_buoc_phon_hoa.jpg"
                     alt="Dạo Bước Phồn Hoa" width="849" height="1200">
                 <figcaption>Ảnh bìa Truyện Dạo Bước Phồn Hoa của Vân Nghê</figcaption>
             </figure>
             <p>Tác giả: Vân Nghê</p>
             <p>Thể loại: Ngôn Tình, Trùng Sinh</p>
             <p>Tóm tắt nội dung:</p>
             <p>Cố Lang Hoa bị vu oan tội thông dâm với nam nhân tên Bùi Tướng. Dẫu nàng có ra sức thanh minh đều bị Lục gia
                 - gia đình chồng không chấp nhận. Họ cưỡng nàng tới con đường phải chết. Tận tới khi từ giã cõi đời, Lang
                 Hoa mang theo nỗi oan nhục không thể gột rửa.</p>
             <p>Ông trời cho Lang Hoa một lần nữa tái sinh, trở về tháng năm tuổi thơ lúc bảy tuổi. Nàng nhìn thấy rõ mồn một
                 năm đó, không phải vì thuỷ đậu mà bị mù mắt, có kẻ đã âm mưu hãm hại nàng. Trong thân thể non nớt của đứa
                 trẻ lên bảy, nàng tìm mọi cách vạch trần sự thật mà nàng đã từng bị dối gạt kiếp trước.</p>
             <p>Đọc truyện Dạo Bước Phồn Hoa Tại Đây</p>
             <h2>4. Review truyện trọng sinh: Sinh con thời mạt thế - Vân Nghê</h2>
             <figure class="image"><img
                     src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-trong-sinh-sinh-con-thoi-mat-the.jpg"
                     alt="Sinh con thời mạt thế" width="700" height="350">
                 <figcaption>Ảnh bìa Truyện Sinh con thời mạt thế của Vân Nghê</figcaption>
             </figure>
             <p>Tác giả: Vân Nghê</p>
             <p>Thể loại: Ngôn Tình, Trọng Sinh, Trùng sinh</p>
             <p>Tóm tắt nội dung:</p>
             <p>Tô Tô từng là nhân vật được nhiều người biết đến. Cô trong mắt người khác là một sinh viên thỏ bạch yếu đuối
                 nhu nhược, tựa hoa dây tơ hồng vô cùng mềm mại, vô cùng yếu ớt.</p>
             <p>Kiếp trước Tô Tô từng phạm phải ba sai lầm lớn của một đời người: Lạc cha mẹ, tin sai người và không thể bảo
                 vệ con mình. Trong lòng Tô Tô có một khoảng trống vĩnh viễn không thể lấp đầy. Tô Tô cảm thấy áp lực đè nén
                 và cô chọn tự sát.&nbsp;</p>
             <p>Kiếp này được trọng sinh, cô quyết tâm trở thành một con người hoàn toàn khác. Trải qua sóng gió cùng khổ ải,
                 cô nhất định sẽ gặt hái được trái ngọt. Top các <a href="https://webtruyen.com/review/">review truyện</a>
                 trọng sinh đều đánh giá cao Sinh Con Thời Mạt Thế.</p>
             <p>Đọc Sinh Con Thời Mạt Thế Tại Đây</p>
             <h2>5.&nbsp;Truyện&nbsp;Thứ nữ hữu độc - Tần Giản</h2>
             <figure class="image"><img
                     src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-trong-sinh-thu-nu-huu-doc.jpg"
                     alt="Thứ nữ hữu độc" width="700" height="455">
                 <figcaption>Ảnh bìa Truyện Thứ nữ hữu độc của Tần Giản</figcaption>
             </figure>
             <p>Tác giả: Tần Giản</p>
             <p>Thể loại: Trọng Sinh,Cung Đấu.</p>
             <p>Tóm tắt nội dung:</p>
             <p>Lí Vị Ương vốn là tiểu thư nhà giàu, đáng lẽ được sống trong nhung lụa nhưng lại trớ trêu mang phận “thứ nữ”.
                 Cô sinh vào tháng xấu nên không được cha sủng ái, coi trọng, bị họ hàng ruồng bỏ, sống trong khổ cực. Biến
                 cố đưa đẩy cô trở thành thế thân cho tỷ tỷ mà một bước làm chủ hậu cung nhưng rồi cũng bị giam vào lãnh
                 cung. Còn con trai uống phải rượu độc mà chết.</p>
             <p>Cơ hội được trùng sinh đã khiến Lí Vị Ương thay đổi, từ hiền lành đôn hậu trở nên tàn bạo độc ác. Kiếp này cô
                 tự hứa với lòng sẽ phải báo thù cho chính mình kiếp trước. Trải qua bao bão giông khổ luyện, Lí Vị Ương từng
                 bước trả thù, trả lại những oan trái kiếp trước cô phải gánh chịu. Nhưng cũng chính Lí Vị Ương kiếp này vì
                 vướng vào lưới tình, mà dây dưa một đời không dứt....</p>
             <p>Đọc truyện <a href="https://webtruyen.com/thu-nu-huu-doc/" target="_blank" rel="noopener">Thứ Nữ Hữu Độc</a>
             </p>
             <p>Trên đây là top 5 review truyện trọng sinh được “mọt” ngôn tình mê mẩn nhất. Bạn thấy truyện nào trong số
                 những truyện trên đây đáng đọc nhất. Đừng quên bấm thích và để lại ý kiến của mình nhé.</p>
             <p></p>
             ',
             'image' => 'review1.jpg'
            ],
            ['id' => 2,'author' => 'Nhóm review Webtruyen.com', 'title' => 'Review Top 5 truyện huyền huyễn được yêu thích nhất', 
             'slug' => str_slug('Review Top 5 truyện huyền huyễn được yêu thích nhất'),
             'summary' => 'Đó là 5 review truyện huyền huyễn bạn nên thử đọc qua. Mỗi truyện đều có phong cách khác nhau bởi những ngòi bút khác nhau. Điểm chung là thế giới huyền huyễn kỳ ảo vô cùng thích hợp để giải trí.',
             'content' => '
                <div id="content" class="w3-justify shortstory-content reviews-content"><!-- ADS CHAPTER 1 <div class="ads-chapter-1"><script type="text/javascript" src="https://webtruyen.com/wp-admin/qt/qc.js"></script></div>--><p></p><p><strong>Thể loại truyện huyền huyễn không giống so với các thể loại truyện ngôn tình khác. Thế giới trong huyền huyễn kỳ ảo, muôn màu muôn vẻ. Chính vì thế mà huyền huyễn dị giới được nhiều fan của ngôn tình yêu thích. Hôm nay, chúng ta sẽ xem qua 5 review truyện huyền huyễn được nhắc tới nhiều nhất 2019 nhé.</strong></p><h2>1. Truyện Thần khống thiên hạ</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-than-khong-thien-ha.png" alt="than khong thien ha" width="700" height="394"><figcaption>Ảnh bìa Truyện Thần khống thiên hạ của Ngã Bản Thuần Khiết</figcaption></figure><p>Tác giả: Ngã Bản Thuần Khiết</p><p>Thể loại: Tiên Hiệp, Dị Giới, <a href="https://webtruyen.com/kiem-hiep/" target="_blank" rel="noopener">Kiếm hiệp</a></p><p>Tóm tắt nội dung</p><p>Tuyệt kỹ đã sớm nổi danh trong thiên hạ của Phong Vân với Bài Vân Chưởng, Phong Thần Cước, khi Lăng Tiếu xuyên việt tới không gian dị giới đã sử dụng mà tiêu diệt hết tất cả những khó khăn trở ngại. Luyện cường giả Thần Giai nhờ làm khôi lỗi, bắt những thần thú mà làm tọa kỵ, bắt thần nữ thành thê thiếp mà dần tiến theo con đường thành Thần Khống Thiên Hạ.</p><p>Chốn giang hồ vốn dĩ là nơi hiểm nguy tiềm ẩn. Thế nhưng bạn bè, chiến hữu cũng nhiều vô kể. Ngã Bản Thuần Khiết đã dùng ngòi bút của mình, biến những chuyện võ lâm trở nên thật gần gũi và sống động. Trong tác phẩm Thần Khống Thiên Hạ có anh hùng cũng có bảo kiếm, có quyền pháp cũng lắm kẻ xấu xa. Tất thảy đều được mô tả đầy logic và hài hoà trong Thần Khống Thiên Hạ.</p><h2>2.&nbsp;Truyện&nbsp;Xà Vương Quấn Thân: Bà Xã, Sinh Quả Trứng</h2><p>Tác giả: Chu Tử Ngư</p><p>Thể loại: Ngôn Tình, Huyền Huyễn.</p><p>Cũng chỉ vì là một thiên tài bắt yêu vì tham lam ăn một quả táo mà Kim Đậu Đậu nghẹn đến chết. Trong thế giới của Xà Vương Quấn Thân: Bà Xã, Sinh Quả Trứng có vô số điều thú vị đang chờ Đậu Đậu cùng trải qua.</p><p>Tóm tắt nội dung:</p><p>Mọi chuyện diễn ra thật sự bất ngờ so với sức tưởng tượng của Đậu Đậu, nhưng dù sao linh hồn của cô lại xuyên không nhập vào một cô bé có tên giống cô, xấu đen, lại lùn. Nhưng còn kỳ quái hơn khi cô phát hiện bản thân mình đang mang thai trứng rắn.&nbsp;</p><p>Mẹ kiếp, đã xấu thế này rồi, con yêu nào không có mắt thế hả?”.</p><p>Vậy, cả đời này phải nuôi con của yêu quái hay sao? Không thể được, cô không thể tự làm xấu hổ danh xưng đạo sĩ bắt yêu của mình thế được. Mặc dù âm thầm chửi tên yêu quái không có mắt nào đã làm cô mang thai trứng rắn, thế nhưng cô lần đầu nhìn thấy cha rắn đã vô cùng ngạc nhiên:Mỹ nam! Là mỹ nam sao, câu chuyện của Kim Đậu Đậu có cái để hấp dẫn hơn rồi.</p><h2>3.&nbsp;Truyện&nbsp;Người Chồng Yêu</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-nguoi-chong-yeu.jpg" alt="truyen nguoi chong yeu" width="700" height="419"><figcaption>Ảnh bìa Truyện Người Chồng Yêu của Vụ Thỉ Dực</figcaption></figure><p>Tóm tắt nội dung:&nbsp;</p><p>Khi hay tin bà ngoại bị ốm nặng, Úc Linh liền rời chốn thị thành phồn hoa trở về nơi thôn nhỏ thanh nhàn yên tĩnh đó.</p><p>Từ lúc hay tin bà ngoại bị ốm, Úc Linh khăn gói tạm xa chốn phồn hoa đô thị để về thăm nom, chăm sóc bà. Ở thôn nhỏ bình yên tĩnh lặng đó, cô vì muốn bà ngoại yên lòng mà đã trực tiếp đồng ý kết hôn với đối tượng xem mắt.</p><p>Đêm tân hôn đầu tiên, Úc Linh mặt đỏ như say nhìn người đàn ông đẹp hoàn mỹ dưới ánh đèn, cô như bị khống chế hoàn toàn. Người này thoạt nhìn thì ngỡ nhã nhặn lịch thiệp, nhưng tiếp xúc dần dà mới hiểu thực sự là cẩu.&nbsp;</p><p>Sau đó không lâu, Úc Linh cũng phát hiện chồng mình vốn dĩ không phải là người. Độc giả sẽ cùng Úc Linh trải qua đủ cung bậc cảm xúc trong cuốn truyện này. Vì thế review truyện huyền huyễn vẫn đánh giá cao yếu tố cảm xúc trong Người Chồng Yêu.</p><h2>4.&nbsp;Truyện&nbsp;Boss Là Nữ Phụ</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-boss-la-phu-nu.jpg" alt="boss la nu phu" width="700" height="525"><figcaption>Ảnh bìa Truyện Boss Là Nữ Phụ của Mặc Linh</figcaption></figure><p>Tác giả: Mặc Linh</p><p>Thể loại: Ngôn Tình, <a href="https://webtruyen.com/di-gioi/" target="_blank" rel="noopener">Dị Giới</a>, Huyền Huyễn.</p><p>Độc giả đọc review truyện huyền huyễn đều thấy rằng bất cứ cuốn tiểu thuyết ngôn tình nào, nữ chính đều là người may mắn được quan tâm, chăm sóc, yêu thương. Còn nữ phụ chỉ được làm nền cho sự nổi bật của nữ chính.</p><p>Tóm tắt nội dung:&nbsp;</p><p>Nhưng trong câu chuyện này, Mặc Linh lại đứng về phía nữ phụ, dùng ngòi bút mà bức tử nữ chính. Nữ phụ trong truyện Boss Là Nữ Phụ đã xuyên không, thay đổi vận mệnh của biết bao người, chứng minh được rằng chính nghĩa luôn thắng thế so với âm mưu tàn độc.</p><p>Mỗi lần qua một thế giới, đều là những câu chuyện nhỏ đáng suy ngẫm.</p><h2>5.&nbsp;Truyện&nbsp;Hệ Thống Đi Lạc Tu Tiên Ký</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-he-thong-di-la-tu-tien-ky.jpg" alt="he thong di lac tu tien ky" width="700" height="1050"><figcaption>Ảnh bìa Truyện Hệ Thống Đi Lạc Tu Tiên Ký của duatop1001</figcaption></figure><p>Tác giả: duatop1001</p><p>Thể loại: <a href="https://webtruyen.com/tien-hiep/" target="_blank" rel="noopener">Tiên Hiệp</a>, Huyền Huyễn.</p><p>Tóm tắt nội dung:&nbsp;</p><p>Nam chính tên là Trần Tùng, vốn là một thanh niên Việt Nam may mắn xuyên việt đến Triệu Vô Cực. Vui mừng còn chưa kịp dứt thì bao khổ ải đã tới nơi. Trần Tùng chỉ có thể thật chăm chỉ tu luyện để tiến tới nhân sinh đỉnh phong.&nbsp;</p><p>Thế nhưng, hắn một lần nữa phát hiện trong môn phái lại có kẻ không cùng chiến tuyến với hắn. Tình tiết truyện diễn ra khá chậm và độc giả có cơ hội “nghiền ngẫm kỹ tâm lý của nam chính”.</p><p>Đó là 5 review truyện <a href="https://webtruyen.com/huyen-huyen/" target="_blank" rel="noopener">huyền huyễn</a> bạn nên thử đọc qua. Mỗi truyện đều có phong cách khác nhau bởi những ngòi bút khác nhau. Điểm chung là thế giới huyền huyễn kỳ ảo vô cùng thích hợp để giải trí.</p><p></p></div>
             ',
             'image' => 'review2.png'
             ],
            ['id' => 3,'author' => 'Nhóm review Webtruyen.com', 'title' => 'Top 5 review truyện H không thể bỏ qua với dân nghiện ngôn tình sắc', 
             'slug' => str_slug('Top 5 review truyện H không thể bỏ qua với dân nghiện ngôn tình sắc'),
             'summary' => 'Trên đây là top 5 review truyện H. Các tác phẩm này đều có cảnh H được miêu tả rất nghệ thuật và đóng góp giá trị cho truyện chứ không hề dung tục. Bạn đọc đừng quên chia sẻ cảm nhận cá nhân, gu truyện H của mình nhé.',
             'content' => '
                <div id="content" class="w3-justify shortstory-content reviews-content"><!-- ADS CHAPTER 1 <div class="ads-chapter-1"><script type="text/javascript" src="https://webtruyen.com/wp-admin/qt/qc.js"></script></div>--><p></p><p><strong>Thể loại truyện huyền huyễn không giống so với các thể loại truyện ngôn tình khác. Thế giới trong huyền huyễn kỳ ảo, muôn màu muôn vẻ. Chính vì thế mà huyền huyễn dị giới được nhiều fan của ngôn tình yêu thích. Hôm nay, chúng ta sẽ xem qua 5 review truyện huyền huyễn được nhắc tới nhiều nhất 2019 nhé.</strong></p><h2>1. Truyện Thần khống thiên hạ</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-than-khong-thien-ha.png" alt="than khong thien ha" width="700" height="394"><figcaption>Ảnh bìa Truyện Thần khống thiên hạ của Ngã Bản Thuần Khiết</figcaption></figure><p>Tác giả: Ngã Bản Thuần Khiết</p><p>Thể loại: Tiên Hiệp, Dị Giới, <a href="https://webtruyen.com/kiem-hiep/" target="_blank" rel="noopener">Kiếm hiệp</a></p><p>Tóm tắt nội dung</p><p>Tuyệt kỹ đã sớm nổi danh trong thiên hạ của Phong Vân với Bài Vân Chưởng, Phong Thần Cước, khi Lăng Tiếu xuyên việt tới không gian dị giới đã sử dụng mà tiêu diệt hết tất cả những khó khăn trở ngại. Luyện cường giả Thần Giai nhờ làm khôi lỗi, bắt những thần thú mà làm tọa kỵ, bắt thần nữ thành thê thiếp mà dần tiến theo con đường thành Thần Khống Thiên Hạ.</p><p>Chốn giang hồ vốn dĩ là nơi hiểm nguy tiềm ẩn. Thế nhưng bạn bè, chiến hữu cũng nhiều vô kể. Ngã Bản Thuần Khiết đã dùng ngòi bút của mình, biến những chuyện võ lâm trở nên thật gần gũi và sống động. Trong tác phẩm Thần Khống Thiên Hạ có anh hùng cũng có bảo kiếm, có quyền pháp cũng lắm kẻ xấu xa. Tất thảy đều được mô tả đầy logic và hài hoà trong Thần Khống Thiên Hạ.</p><h2>2.&nbsp;Truyện&nbsp;Xà Vương Quấn Thân: Bà Xã, Sinh Quả Trứng</h2><p>Tác giả: Chu Tử Ngư</p><p>Thể loại: Ngôn Tình, Huyền Huyễn.</p><p>Cũng chỉ vì là một thiên tài bắt yêu vì tham lam ăn một quả táo mà Kim Đậu Đậu nghẹn đến chết. Trong thế giới của Xà Vương Quấn Thân: Bà Xã, Sinh Quả Trứng có vô số điều thú vị đang chờ Đậu Đậu cùng trải qua.</p><p>Tóm tắt nội dung:</p><p>Mọi chuyện diễn ra thật sự bất ngờ so với sức tưởng tượng của Đậu Đậu, nhưng dù sao linh hồn của cô lại xuyên không nhập vào một cô bé có tên giống cô, xấu đen, lại lùn. Nhưng còn kỳ quái hơn khi cô phát hiện bản thân mình đang mang thai trứng rắn.&nbsp;</p><p>Mẹ kiếp, đã xấu thế này rồi, con yêu nào không có mắt thế hả?”.</p><p>Vậy, cả đời này phải nuôi con của yêu quái hay sao? Không thể được, cô không thể tự làm xấu hổ danh xưng đạo sĩ bắt yêu của mình thế được. Mặc dù âm thầm chửi tên yêu quái không có mắt nào đã làm cô mang thai trứng rắn, thế nhưng cô lần đầu nhìn thấy cha rắn đã vô cùng ngạc nhiên:Mỹ nam! Là mỹ nam sao, câu chuyện của Kim Đậu Đậu có cái để hấp dẫn hơn rồi.</p><h2>3.&nbsp;Truyện&nbsp;Người Chồng Yêu</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-nguoi-chong-yeu.jpg" alt="truyen nguoi chong yeu" width="700" height="419"><figcaption>Ảnh bìa Truyện Người Chồng Yêu của Vụ Thỉ Dực</figcaption></figure><p>Tóm tắt nội dung:&nbsp;</p><p>Khi hay tin bà ngoại bị ốm nặng, Úc Linh liền rời chốn thị thành phồn hoa trở về nơi thôn nhỏ thanh nhàn yên tĩnh đó.</p><p>Từ lúc hay tin bà ngoại bị ốm, Úc Linh khăn gói tạm xa chốn phồn hoa đô thị để về thăm nom, chăm sóc bà. Ở thôn nhỏ bình yên tĩnh lặng đó, cô vì muốn bà ngoại yên lòng mà đã trực tiếp đồng ý kết hôn với đối tượng xem mắt.</p><p>Đêm tân hôn đầu tiên, Úc Linh mặt đỏ như say nhìn người đàn ông đẹp hoàn mỹ dưới ánh đèn, cô như bị khống chế hoàn toàn. Người này thoạt nhìn thì ngỡ nhã nhặn lịch thiệp, nhưng tiếp xúc dần dà mới hiểu thực sự là cẩu.&nbsp;</p><p>Sau đó không lâu, Úc Linh cũng phát hiện chồng mình vốn dĩ không phải là người. Độc giả sẽ cùng Úc Linh trải qua đủ cung bậc cảm xúc trong cuốn truyện này. Vì thế review truyện huyền huyễn vẫn đánh giá cao yếu tố cảm xúc trong Người Chồng Yêu.</p><h2>4.&nbsp;Truyện&nbsp;Boss Là Nữ Phụ</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-boss-la-phu-nu.jpg" alt="boss la nu phu" width="700" height="525"><figcaption>Ảnh bìa Truyện Boss Là Nữ Phụ của Mặc Linh</figcaption></figure><p>Tác giả: Mặc Linh</p><p>Thể loại: Ngôn Tình, <a href="https://webtruyen.com/di-gioi/" target="_blank" rel="noopener">Dị Giới</a>, Huyền Huyễn.</p><p>Độc giả đọc review truyện huyền huyễn đều thấy rằng bất cứ cuốn tiểu thuyết ngôn tình nào, nữ chính đều là người may mắn được quan tâm, chăm sóc, yêu thương. Còn nữ phụ chỉ được làm nền cho sự nổi bật của nữ chính.</p><p>Tóm tắt nội dung:&nbsp;</p><p>Nhưng trong câu chuyện này, Mặc Linh lại đứng về phía nữ phụ, dùng ngòi bút mà bức tử nữ chính. Nữ phụ trong truyện Boss Là Nữ Phụ đã xuyên không, thay đổi vận mệnh của biết bao người, chứng minh được rằng chính nghĩa luôn thắng thế so với âm mưu tàn độc.</p><p>Mỗi lần qua một thế giới, đều là những câu chuyện nhỏ đáng suy ngẫm.</p><h2>5.&nbsp;Truyện&nbsp;Hệ Thống Đi Lạc Tu Tiên Ký</h2><figure class="image"><img src="https://img.webtruyen.com/public/images/reviews_img/20200131/review-truyen-huyen-huyen-he-thong-di-la-tu-tien-ky.jpg" alt="he thong di lac tu tien ky" width="700" height="1050"><figcaption>Ảnh bìa Truyện Hệ Thống Đi Lạc Tu Tiên Ký của duatop1001</figcaption></figure><p>Tác giả: duatop1001</p><p>Thể loại: <a href="https://webtruyen.com/tien-hiep/" target="_blank" rel="noopener">Tiên Hiệp</a>, Huyền Huyễn.</p><p>Tóm tắt nội dung:&nbsp;</p><p>Nam chính tên là Trần Tùng, vốn là một thanh niên Việt Nam may mắn xuyên việt đến Triệu Vô Cực. Vui mừng còn chưa kịp dứt thì bao khổ ải đã tới nơi. Trần Tùng chỉ có thể thật chăm chỉ tu luyện để tiến tới nhân sinh đỉnh phong.&nbsp;</p><p>Thế nhưng, hắn một lần nữa phát hiện trong môn phái lại có kẻ không cùng chiến tuyến với hắn. Tình tiết truyện diễn ra khá chậm và độc giả có cơ hội “nghiền ngẫm kỹ tâm lý của nam chính”.</p><p>Đó là 5 review truyện <a href="https://webtruyen.com/huyen-huyen/" target="_blank" rel="noopener">huyền huyễn</a> bạn nên thử đọc qua. Mỗi truyện đều có phong cách khác nhau bởi những ngòi bút khác nhau. Điểm chung là thế giới huyền huyễn kỳ ảo vô cùng thích hợp để giải trí.</p><p></p></div>
             ',
             'image' => 'review3.jpg'
            ]
        ]);
    }
}
