<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->delete();
        DB::table('category')->insert([
            ['id' => 1,  'name' => 'Tiên Hiệp', 'slug' => str_slug('Tiên Hiệp')],
            ['id' => 2,  'name' => 'Ngôn Tình', 'slug' => str_slug('Ngôn Tình')],
            ['id' => 3,  'name' => 'Đô Thị', 'slug' => str_slug('Đô Thị')],
            ['id' => 4,  'name' => 'Lịch Sử', 'slug' => str_slug('Lịch Sử')],
            ['id' => 5,  'name' => 'Truyện Ma', 'slug' => str_slug('Truyện Ma')],
            ['id' => 6,  'name' => 'Huyền Huyễn', 'slug' => str_slug('Huyền Huyễn')],
            ['id' => 7,  'name' => 'Dị Giới', 'slug' => str_slug('Dị Giới')],
            ['id' => 8,  'name' => 'Truyện Ngắn', 'slug' => str_slug('Truyện Ngắn')],
            ['id' => 9,  'name' => 'Tiểu Thuyết', 'slug' => str_slug('Tiểu Thuyết')],
            ['id' => 10, 'name' => 'Kiếm Hiệp', 'slug' => str_slug('Kiếm Hiệp')],
            ['id' => 11, 'name' => 'Truyện Teen', 'slug' => str_slug('Truyện Teen')],
            ['id' => 12, 'name' => 'Quân Sự', 'slug' => str_slug('Quân Sự')],
            ['id' => 13, 'name' => 'Xuyên Không', 'slug' => str_slug('Xuyên Không')],
            ['id' => 14, 'name' => 'Trinh Thám', 'slug' => str_slug('Trinh Thám')],
            ['id' => 15, 'name' => 'Khoa Huyễn', 'slug' => str_slug('Khoa Huyễn')],
            ['id' => 16, 'name' => 'Võng Du', 'slug' => str_slug('Võng Du')],
            ['id' => 17, 'name' => 'Truyện Cười', 'slug' => str_slug('Truyện Cười')],
        ]);
    }
}
