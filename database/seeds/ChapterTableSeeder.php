<?php

use Illuminate\Database\Seeder;

class ChapterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chapters')->delete();
        DB::table('chapters')->insert([
            ['id' => 1,'number_chapter' => 1, 'name_chapter' =>'Chương 1', 'name' => 'Kinh Trập', 'slug' => str_slug('Kinh Trập'), 'id_stories' => 1,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '],

            ['id' => 2,'number_chapter' => 2,'name_chapter' =>'Chương 2', 'name' => 'Mở Cửa', 'slug' => str_slug('Mở Cửa'), 'id_stories' => 1,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '],

            ['id' => 3,'number_chapter' => 3,'name_chapter' =>'Chương 3', 'name' => 'Nhật Xuất', 'slug' => str_slug('Nhật Xuất'), 'id_stories' => 1,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '],

            ['id' => 4,'number_chapter' => 4,'name_chapter' =>'Chương 4', 'name' => 'Hoàng Điểu', 'slug' => str_slug('Hoàng Điểu'), 'id_stories' => 1,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '],

            ['id' => 5,'number_chapter' => 5,'name_chapter' =>'Chương 5', 'name' => 'Đạo Phá', 'slug' => str_slug('Đạo Phá'), 'id_stories' => 1,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '],

            ['id' => 6,'number_chapter' => 6,'name_chapter' =>'Chương 6', 'name' => 'Thẻ Hạ', 'slug' => str_slug('Thẻ Hạ'), 'id_stories' => 1,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '],


            ['id' => 7,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Phục Vụ Không Tệ, Không Cần Trả Tiền Thừa', 'slug' => str_slug('Phục Vụ Không Tệ, Không Cần Trả Tiền Thừa'), 'id_stories' => 2,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian. 
            '
            ],

            ['id' => 8,'number_chapter' => 2,'name_chapter' =>'Chương 2', 'name' => 'Bốc Hơi Khỏi Nhân Gian', 'slug' => str_slug('Bốc Hơi Khỏi Nhân Gian'), 'id_stories' => 2,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian. 
            '
            ],
            ['id' => 9,'number_chapter' => 3,'name_chapter' =>'Chương 3', 'name' => 'Quý Vũ Vi', 'slug' => str_slug('Quý Vũ Vi'), 'id_stories' => 2,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian. 
            '
            ],
            ['id' => 10,'number_chapter' => 4,'name_chapter' =>'Chương 4', 'name' => 'Bôi Thuốc Cho Bùi Kình Nam ', 'slug' => str_slug('Bôi Thuốc Cho Bùi Kình Nam '), 'id_stories' => 2,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian. 
            '
            ],
            ['id' => 11,'number_chapter' => 5,'name_chapter' =>'Chương 5', 'name' => 'Sửa Soạn Xong Buổi Chiều Đi Đăng Ký', 'slug' => str_slug('Sửa Soạn Xong Buổi Chiều Đi Đăng Ký'), 'id_stories' => 2,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],
            ['id' => 12,'number_chapter' => 6,'name_chapter' =>'Chương 6', 'name' => 'Nghẹn Ở Trong Quân Đội Đến Phát Điên Rồi', 'slug' => str_slug(' Nghẹn Ở Trong Quân Đội Đến Phát Điên Rồi'), 'id_stories' => 2,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian. 
            '
            ],


            ['id' => 13,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Tống Thanh Thư Kiếp Trước Kiếp Này ', 'slug' => str_slug('Tống Thanh Thư Kiếp Trước Kiếp Này '), 'id_stories' => 3,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],

            ['id' => 14,'number_chapter' => 2,'name_chapter' =>'Chương 2', 'name' => 'Phế Nhân Đứt Đoạn Kinh Mạch (1)', 'slug' => str_slug('Phế Nhân Đứt Đoạn Kinh Mạch (1)'), 'id_stories' => 3,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian. 
            '
            ],

            ['id' => 15,'number_chapter' => 3,'name_chapter' =>'Chương 3', 'name' => 'Phế Nhân Đứt Đoạn Kinh Mạch (2) ', 'slug' => str_slug('Phế Nhân Đứt Đoạn Kinh Mạch (2)'), 'id_stories' => 3,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian. 
            '
            ],
            ['id' => 16,'number_chapter' => 4,'name_chapter' =>'Chương 4', 'name' => 'Tuyên Ngôn Bảo Vệ Kiều Thê (1)', 'slug' => str_slug('Tuyên Ngôn Bảo Vệ Kiều Thê (1)'), 'id_stories' => 3,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],

            ['id' => 17,'number_chapter' => 5,'name_chapter' =>'Chương 5', 'name' => 'Tuyên Ngôn Bảo Vệ Kiều Thê (2)', 'slug' => str_slug('Tuyên Ngôn Bảo Vệ Kiều Thê (2)'), 'id_stories' => 3,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],

            ['id' => 18,'number_chapter' => 6, 'name_chapter' =>'Chương 6','name' => 'Vô Danh 7', 'slug' => str_slug('Vô Danh 7'), 'id_stories' => 3,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],


            ['id' => 19,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Vô Danh 5', 'slug' => str_slug('Vô Danh 5'), 'id_stories' => 4,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],

            ['id' => 20,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Vô Danh 4', 'slug' => str_slug('Vô Danh 4'), 'id_stories' => 5,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],

            ['id' => 21,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Vô Danh 3', 'slug' => str_slug('Vô Danh 3'), 'id_stories' => 6,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],


            ['id' => 22,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Vô Danh 2', 'slug' => str_slug('Vô Danh 2'), 'id_stories' => 7,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],

            ['id' => 23,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Vô Danh  (1)', 'slug' => str_slug('Vô Danh  (1)'), 'id_stories' => 8,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],


            ['id' => 24,'number_chapter' => 1,'name_chapter' =>'Chương 1', 'name' => 'Vô Danh Thánh  (1)', 'slug' => str_slug('Vô Danh Thánh  (1)'), 'id_stories' => 9,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],


            ['id' => 25,'number_chapter' => 2, 'name_chapter' =>'Chương 2','name' => 'Vôs Danh Thánh Tăng (1)', 'slug' => str_slug('Vôs Danh Thánh Tăng (1)'), 'id_stories' => 6,
            'content' => '
            <!--End-VIP stories-->Tản Mạn Ngoài Lề<br /><br />Từ rất xa xưa, lâu lắm rồi, Sáng thế thần ÔNG BỤT và Thần
            nữ CÔ TẤM cưỡi Thần thú Thần Kim Quy vận đủ 10 thành công lực nội ngoại và tỷ vạn phép thần thông cầm thanh
            Càn Khôn Kiếm chém nát Hỗn độn, mở ra một thời đại mới cho các sinh linh nghiện truyện kiếm hiệp, võ hiệp,
            tiên hiệp đủ các loại.<br /><br />Rồi một ngày nào đó các Tiên Đế, Chân Thần của hai môn phái Thất Giới và
            Đại Đường tình cờ thấy được mười chữ chân ngôn "Tụ nghĩa vô lợi kỷ, xuất thư bất tư danh“. Đễ có thể lãnh
            ngộ hết những ý nghĩa của mười chữ chân ngôn này, họ quyết định tụ hợp lại, dựng lên một môn phái còn to lớn
            hơn, mạnh mẽ hơn, đó chính là Tàng Thư Viện.<br /><br />Theo dòng sông thời gian, từ Tàng Thư Viện lần lượt
            xuất hiện thêm hàng loạt các tiểu môn phái khác như Truyện Dịch, Truyện Convert, Truyện Sáng Tác, Phong Lưu
            Bang, Thiên Hạ Hội, Cái Bang, Hoa Nguyệt Lâu…<br /><br />Từng thời đại một đều có vô số các Tiên Đế và vô
            địch Thần Hoàng, những cái tên như Biên Hoang Truyền Thuyết, Cực Phẩm Gia Đinh hay Đỉnh Cấp Lưu Manh khiến
            cho thiên hạ sợ run vì số lượng view, à nhầm, số lượng môn hạ đệ tử của bọn họ đến 7 triệu đến 9 triệu. Quét
            ngang thiên hạ, hùng bá trời đất.<br /><br />Cũng theo dòng chảy thời gian, vô số các vị đầu sỏ vô địch,
            Tiên Đế và Thần Hoàng vì muốn tham ngộ khổ tu mười chữ vàng để sớm đắc đạo phá toái hư không mà lần lượt quy
            ẩn nơi rừng sâu, suối thẳm, đồng ruộng nước chảy róc rách.<br /><br />Già ẩn, trẻ hiện, âu cũng là bình
            thường của nhân sinh.<br /><br />Vietstars tôi may mắn có một thời được sát cánh và đóng góp một phần công
            sức cực kỳ nhỏ bé trong thời gian đầu cùng Thất Giới, Đại Đường, Biên Hoang tranh tài với Nhạn Môn Quan và
            các tiểu môn phái ở Dị Giới khác. Đó là một thời đại đầy gian nan, máu lửa và đầy nhiệt huyết, nhưng cũng
            đồng thời dánh dấu vị trí to lớn của Tàng Thư Viện.<br /><br />Đỉnh Cấp Lưu Manh và Cực Phẩm Gia Đinh là
            thời đại kế tiếp với các vị vô địch đầu sỏ khác, trong đó chất chứa bao đam mê, sức trẻ, sức già cuồn cuộn,
            hoành tráng và không thiếu kém nhiệt huyết nếu so với các vị tiền bối.<br /><br />Nay vietstars tôi quy ẩn
            tu luyện đến bình cảnh không cách nào tiến tiếp bước nữa, lúc trước vừa quy ẩn vừa thi thoảng thò một chân
            dò dẫm giang hồ, giờ thì có bao chân tay thò xuống hết.<br /><br />Ôi, Tàng Thư Viện. Cảnh xưa người cũ,
            cảnh mới người mới. Cố nhân kẻ đi người ở lại, kẻ đã vô vi, người vẫn còn quy ẩn. Nay dán bố cáo hoài niệm
            một thời cũng như tri ân tới tất cả các huynh đệ tỷ muội sát cánh một thời ngang dọc. Chúc mọi người bình
            an, mạnh khỏe, phúc lộc đầy nhà.<br /><br />Đồng thời xin thông cáo thiết lập chiến địch Đế Bá. Hi vọng nó
            sẽ hoành tráng không thua kém gì thời trước. Chiêu hiền nạp sĩ, thân mời các cao thủ vào:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />PS: Ai đến Tàng Thư Viện mà chưa qua Thất
            Giới, Đại Đường Song Long, Biên Hoang Truyền Thuyết, chưa vượt Cực Phẩm Gia Đinh, Đỉnh Cấp Lưu Manh thì đáng
            tiếc, đáng tiếc.<br /><br />Lần sửa cuối bởi vietstars, ngày Hôm nay lúc 08:51. <br /><br />Đế Bá được dịch
            bởi Nhóm Dịch Toàn Cao Thủ! Đăng tại box truyện dịch Tàng Thư Viện Truyện hay!<br /><br />Hidden Content
            Hidden Content Hidden Content <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời
            kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành viên cảm ơn:: [Hiện
            ra]<br /><br />03-02-2015, 21:26 #2<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu Sinh<br /><br />Dịch: Huyết
            Long<br /><br />Biên dịch: vietstars<br /><br />Biên tập: vietstars<br /><br />Hợp đồng (Khế
            Tử)<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai
            đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được. Mọi người đừng hi vọng có chương dịch sớm nhé. Xin
            cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu này lên chỉ để marketing quảng cáo, đặt gạch chiếm
            chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao thủ chung sức dịch bộ truyện này. <br /><br />Sau
            khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu
            đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì thà để sau này đọc truyện dịch không bị chờ lâu còn
            hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày Hôm nay lúc 14:18. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm
            Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 38 thành viên
            cảm ơn:: [Hiện ra]<br /><br />06-02-2015, 08:13 #3<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Giới thiệu sơ qua nội dung của truyện Đế
            Bá<br /><br />Trước khi giới thiệu sơ qua nội dung của truyện Đế Bá, mình xin được phép lan man ngoài lề một
            chút.<br /><br />Khi nói về truyện thuộc thể loại tự sướng hay còn được gọi là YY được dịch tại Tàng Thư
            Viện, thì không thể không nhắc đến Cực Phẩm Gia Đinh hoặc Đỉnh Cấp Lưu Manh. Có thể đánh giá truyện Cực Phẩm
            Gia Đinh vào hạng truyện tự sướng đỉnh cấp cao nhất, vì ngòi bút của tác giả cứng cỏi trong phong thái hành
            văn, trong cách miêu tả, cách dàn dựng nội dung và cấu tạo một nhân vật tài trí, giỏi tán gái, giỏi quyền
            mưu, cực kỳ giỏi thơ văn, võ công tàm tạm, có thể nói trên thông thiên văn dưới tường địa lý và anh ta chỉ
            thiếu mỗi cái áo siêu nhân đeo trên người nữa là vô đối. Bộ truyện đó khiến hàng triệu độc giả say mê, hàng
            trăm dịch giả già có, trẻ có, cao thủ, cao nhân, thi sĩ, nhà văn hào hứng dịch vì nội dung hay và tác giả
            viết rất mượt. Nó hay và được phóng đại đến nỗi người ta không dám liệt nó vào thể loại truyện tự sướng vì
            sợ làm lu mờ hào quang của nó. Nhưng thử hỏi mấy cao thủ kiếm hiệp nào đầu thai dị giới mà siêu như vậy, ví
            dụ như văn thơ thì vác nội dung sách giáo khoa học ở trường tại thế giới hiện tại ra là thiên tài cũng bó
            tay. Nói chung do tài năng của tác giả mà bộ truyện này đã vượt qua phạm trù của truyện tự
            sướng.<br /><br />Link đây: http://www.tangthuvien.vn/forum/showthread.php?t=11304<br /><br />Bộ truyện thứ
            hai theo thể loại đô thị, đó chính là Đỉnh Cấp Lưu Manh, cũng thuộc dòng tự sướng. Nhân vật chính trong
            truyện này mạnh không kém siêu nhân, cũng giỏi tán gái và với "võ công" tuyệt thế của anh ta( có thể xử lý
            hầu như tất cả các cao thủ đến siêu cao thủ đứng gần, thêm vào nữa là môn "vô hình chỉ" giết người nhanh như
            chớp). Đáng tiếc là tác giả đã không giữ vững được ngòi bút tốt của mình, càng viết về sau càng lan man,
            khiến cho nội dung rời rạc, rơi vào ngõ cụt và không thể kết thúc nó được nữa.<br /><br />Link đây:
            http://www.tangthuvien.vn/forum/showthread.php?t=31521<br /><br />Giờ chúng ta nói về bộ truyện Đế Bá của
            tác giả Yếm Bút Tiêu Sinh, người được các độc giả Tàng Thư Viện mến mộ qua biệt danh Câu Chữ Đại Tông Sư với
            võ công Câu chữ đại thần thông.<br /><br />Thuộc thể loại truyện tự sướng, Đế Bá có thể trở thành một bộ
            truyện rất hay của thể loại này nếu ngòi bút của tác giả cứng cáp hơn nữa, cách hành văn và miêu tả mượt mà
            hơn nữa. Nhưng có lẽ vì tay bút còn non và hay câu chữ lan man( theo ý kiến cá nhân của mình: tác giả viết
            truyện kiếm tiền, nên cố viết kéo dài), nên bộ truyện này có lắm người thích, có lắm người chê, nhưng quanh
            đi quẩn lại vẫn quay lại đọc vì bị nội dung của truyện này gây tò mò muốn tìm giải đáp.<br /><br />Tò mò ư?
            Nghĩa là thế nào? Xin mạn phép đưa ra ý kiến khách quan của cá nhân mình về nội dung của truyện Đế
            Bá.<br /><br />Nhân vật chính Lý Thất Dạ bị chủ nhân thần bí của Tiên Ma Động cưỡng bức vào một hợp đồng
            tưởng như chỉ có lợi cho Tiên Ma Động khi Lý Thất Dạ chỉ là một đứa bé mười ba tuổi. Chủ nhân của Tiên Ma
            Động chuyển hồn phách của Lý Thất Dạ vào một vật do hắn luyện chế (với tên gọi Âm Nha) có hình của một con
            quạ đen. Dã tâm của chủ nhân của Thiên Ma Động to lớn đến cỡ nào( tác giả chưa cho chúng ta đáp án) thì độc
            giả vẫn chưa biết, nhưng chỉ biết kẻ này muốn dùng Âm Nha bất tử bay đi khắp Cửu giới, thu nạp tất cả các
            thông tin, tìm hiểu tất cả các bí mật của trời đất này, đi vào tất cả những nơi mà không ai dám vào vì bản
            thân Âm Nha không bao giờ chết( chỉ có hồn phách của Lý Thất Dạ sẽ bị tra tấn một cách đau đớn không tưởng
            nổi). Và rồi cứ sau một thế, chủ nhân của Tiên Ma Động lại triệu hồi Âm Nha Lý Thất Dạ trở lại, sau đó sưu
            hồn lục tìm toàn bộ trí nhớ và ký ức của Lý Thất Dạ trong thời gian qua. Hắn muốn những tin tức đó để làm
            gì? Hắn không sợ bị thiên khiển sao? Bạn có tò mò không? Thôi thì đành cắn răng đọc đến cuối cùng theo tác
            giả, hi vọng được thỏa mãn trí tò mò.<br /><br />Về phần Lý Thất Dạ, hắn tuyệt đối không cam lòng bị chủ
            nhân của Thiên Ma Động hành hạ. Làm Âm Nha hắn có thể bất tử, nhưng cuối cùng vẫn bị Thiên Ma Động coi như
            một vật sở hữu hoặc như một quả chanh chỉ chờ đến bị vắt cạn kiệt nước là vất vào đống rác rưởi. Nhưng một
            đứa bé có thể chất là phàm thể, thọ luân là phàm luân, mệnh cung là phàm mệnh thì cho dù có cách thoát ra
            khỏi Âm Nha và sự khống chế của Tiên Ma Động thì Lý Thất Dạ vẫn chỉ là 1 con kiến nhỏ bé dưới bàn chân của
            người khổng lồ thôi.<br /><br />Vì thế, ngày qua ngày, năm qua năm, thế qua thế, trong ngàn vạn năm qua, Lý
            Thất Dạ lợi dụng lợi thế bất tử của mình, tìm hiểu các bí mật ở khắp Cửu Giới. Với vốn kiến thức khổng lồ
            đó, hắn kết bạn với các thiên tài của các thời đại, đào tạo các vị Tiên Đế từ khi bọn họ chưa chấp chưởng
            thiên mệnh. Hắn lúc thì là bạn, lúc là sư phụ, lúc là cao nhân đứng phía sau màn. Đời qua đời, thế qua thế,
            trong những năm tháng đó hắn từng bước dựng lên một kế hoạch khổng lồ với mục đích duy nhất là tận diệt Tiên
            Ma Động. Các đời Tiên Đế, vô địch đẩu sỏ đều giúp đỡ hắn rất nhiều, ví dụ như bảo vệ thần trí của hắn, gia
            trì ký ức của hắn...Lúc mà vị vô địch cao thủ Hắc Long Vương phá vỡ liên kết giữa Cửu giới, đó là lúc bước
            đầu của kế hoạch khổng lồ này được thi hành. Lý Thất Dạ đã thoát khỏi Âm Nha và tìm lại được thân thể của
            mình.<br /><br />Kế hoạch đó to lớn như thế nào? Có những bước đi ra sao? Lý Thất Dạ sẽ làm gì để tiêu diệt
            Tiên Ma Động? Bạn có tò mò không? Nếu có thì đành cắn răng theo dõi tiếp vậy.<br /><br />Lý Thất Dạ bá đạo,
            ngông cuồng bởi vì hắn được tác giả tặng cho 1 bối cảnh sống qua ngàn vạn năm. Nếu sư tổ của các ngươi còn
            là bạn hoặc đồ đệ của ta thì các ngươi chưa có đủ tư cách ở trước mặt ta ngạo mạn. Nếu như Nữ đế còn là đồ
            đệ/ người tình( lúc còn là Âm Nha thì Lý Thất Dạ ở một thời đại nào đó đã thành công sáng chế ra một môn
            công pháp biến hình đổi dạng, hắn có thể biến thành hình người, nhưng có hạn chế và cuối cùng vẫn phải hóa
            thành quạ đen khi Tiên Ma Động triệu hồi) của ta thì mỹ nhân hiện tại chưa đủ phân lượng. Bí mật của thiên
            địa này ta biết rất nhiều, các ngươi chỉ là hạt cát trong sa mạc mà thôi.<br /><br />Tác giả càng viết càng
            cứng tay, tuy vẫn câu chữ câu chương, nhưng phong cách hành văn dần dần không bị hạn chế trong cách dùng câu
            từ hoặc lặp đi lặp lại. Đội ngũ dịch, biên dịch, biên tập truyện này sẽ chỉnh sửa một số các chương đầu mượt
            mà như có thể. Mong rằng ai ai cũng thấy thoải mái khi đọc bộ truyện giải trí thuần túy này.<br /><br />Mọi
            thắc mắc, hỏi han, góp ý xin vào các link sau:<br /><br />1. Hô hào dịch Đế Bá:
            http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />2. Nhóm dịch Toàn Cao Thủ:
            http://www.tangthuvien.vn/forum/showthread.php?t=115192<br /><br />Mã:<br /><br />Chú ý: Tuy chiến dịch đã
            chính thức bắt đầu, nhưng hiện thời vẫn đang ở giai đoạn dịch/biên dịch và biên tập nên chưa phát hỏa được.
            Mọi người đừng hi vọng có chương dịch sớm nhé. Xin cám ơn đã thông cảm.<br /><br />Mình post chương khởi đầu
            này lên chỉ để marketing quảng cáo, đặt gạch chiếm chỗ cũng như chiêu <br /><br />hiền nạp sĩ, mời các cao
            thủ chung sức dịch bộ truyện này. <br /><br />Sau khi hòm hòm mới chính thức đốt pháo công cáo toàn thiên
            hạ. <br /><br />Trơn tru thì đầu năm mới sẽ bắt đầu đăng các chương dịch không thì sẽ lùi lại vài tuần. Vì
            thà để sau này đọc truyện dịch không bị chờ lâu còn hơn đăng sớm rồi mất công ngồi hóng. <br /><br />Thân
            mời ghé qua
            :http://www.tangthuvien.vn/forum/showthread.php?t=115090<br /><br />Thân,<br /><br />vietstars<br /><br />Lần
            sửa cuối bởi vietstars, ngày 12-02-2015 lúc 07:49. <br /><br />Trả lời nhanh bài viết này Trả lời Trả lời
            kèm Trích dẫn Trả lời kèm Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 31 thành
            viên cảm ơn:: [Hiện ra]<br /><br />19-02-2015, 05:47 #4<br /><br />vietstarss
            Avatar<br /><br />vietstars<br /><br />vietstars Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ
            nhân<br /><br />Ngày tham gia<br /><br />Dec 2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài
            viết<br /><br />5,077 <br /><br />Xu<br /><br />10 <br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Nhân dịp đầu xuân Ất Mùi 2015, nhóm dịch Toàn Cao Thủ xin kính chúc tất cả các thành viên
            của Tàng Thư Viện và các độc giả chưa đăng ký gia nhập Tàng Thư Viện một năm mới mạnh khỏe, vui vẻ, vạn sự
            như ý và phúc lộc đầy nhà.<br /><br />Ngoài ra, nhóm dịch xin trân trọng tặng lì xì/ mừng tuổi tất cả các
            thành viên bằng một câu sau:<br /><br />Vào thứ 3, ngày mùng 3, tháng 3, năm 2015, nhóm dịch Toàn Cao Thủ
            chính thức đăng các chương truyện Đế Bá được dịch và pháo sẽ nổ tưng bừng suốt cả ngày!<br /><br />Happy New
            Year! Chúc mừng năm mới!<br /><br />Trả lời nhanh bài viết này Trả lời Trả lời kèm Trích dẫn Trả lời kèm
            Trích dẫn Trả lời với nhiều Trích dẫn Thanks <br /><br />Bài viết được 28 thành viên cảm ơn:: [Hiện
            ra]<br /><br />Hôm nay, 05:46 #5<br /><br />vietstarss Avatar<br /><br />vietstars<br /><br />vietstars
            Đang Ngoại tuyến Tiếu Ngạo Giang Hồ Thông Ngữ kỳ nhân<br /><br />Ngày tham gia<br /><br />Dec
            2007<br /><br />Đang ở<br /><br />Phong Lưu Bang<br /><br />Bài viết<br /><br />5,077
            <br /><br />Xu<br /><br />10 <br /><br />Nhóm dịch Toàn Cao Thủ xin tỏ lòng biết ơn sự ủng hộ và giúp đỡ
            nhiệt tình của ban quản trị Tàng Thư Viện, thành viên tobano với nhóm Dịch Thuần Việt và các huynh đệ tỷ
            muội khác.<br /><br />Bộ truyện Đế Bá là bộ đầu tiên của nhóm, hi vọng nó sẽ đem niềm vui và nụ cười đến với
            tất cả các độc giả. Tên nhóm dịch Toàn Cao Thủ nghe rất "bá", rất "vang", nhưng bọn mình chỉ là những dịch
            giả nghiệp dư tham gia dịch thuật vì sở thích và niềm vui, lấy cái tên nghe "chuối" vậy cũng chỉ để tự châm
            biếm mình một cách hài hước, vì tài năng của bọn mình chưa thể so sánh được với những dịch giả kỳ cựu với
            thâm niên cao hoặc cực thành thạo tiếng Trung.<br /><br />Bộ truyện này hay nhất ở điểm "bí mật" và khiến
            cho độc giả "tò mò", ngoài ra cốt truyện cũng không đến nỗi nào. Đế Bá là truyện để cười, để giải trí, đọc
            cho vui xả stress !!!<br /><br />Ngoài ra, nhóm dịch Toàn Cao Thủ xin chân thành cảm ơn tất cả các dịch giả,
            biên tập viên đã tham gia dịch thuật cùng nhóm trong thời gian đầu:<br /><br />* vietstars, Tiếu Hồng Trần,
            Dạ Đế, Hàn Dạ, Huyết Long, I LOVE U, Zeroman, thaiduongdhd, Tiểu hài nhi, tntkxx, htph, Zhu Xian,
            phuongngaytho, ronkute, Niêu, Miên Lý Tàng Châm, Thiên Vũ, Huyết Ma Thần, bradrangon,Tiêu Dao Nhân,
            guiliano, nhongcon_pupa, thuan0, tobano, gon, Everest, blackcat123, csasonic<br /><br />Ngày hôm nay sẽ có
            bom Đế Bá trải thảm ở box truyện dịch, mình chỉ đăng 2 chương 1 và 2, các chương còn lại các độc giả hãy đợi
            hoặc hỏi Tiếu Hồng Trần. Sang ngày thứ 4, mỗi ngày sẽ tạm thời chỉ đăng 1 chương truyện (hoặc có thể 2/
            nhiều chương nếu hàng ứ đọng nhiều hoặc ngày đặc biệt nào đó) cho đến hết tháng 3 để nhóm có thời gian tích
            trữ chương truyện dịch. Sang tháng 4, sẽ là cuộc chiến thần tốc theo đuổi tác giả, ngày có thể 2, 3 hoặc 4
            chương!!!<br /><br />Nhóm dịch Toàn Cao Thủ.<br /><br />Đế Bá<br /><br />Tác giả: Yếm Bút Tiêu
            Sinh<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />Dịch: Huyết Long<br /><br />Biên dịch:
            vietstars<br /><br />Biên tập: Tiếu Hồng Trần<br /><br />Chương 1: Tam Quỷ Gia (thượng)<br /><br />“Xoạt”
            một tiếng, Lý Thất Dạ đang trôi nổi giữa dòng sông bỗng được người vớt lên.<br /><br />- A!<br /><br />Trong
            lúc bị kéo lên, Lý Thất Dạ đột nhiên tỉnh lại, và phản ứng đầu tiên của hắn chính là nhảy dựng lên. Do chưa
            kịp thích ứng lại với cơ thể của mình, hắn lảo đảo suýt nữa ngã sấp mặt xuống.<br /><br />- A, thân thể của
            ta!<br /><br />Lý Thất Dạ cúi đầu nhìn, thấy cơ thể của mình không thay đổi gì, hắn vừa mừng vừa sợ. Mặc dù
            đã trải qua muôn ngàn khó khăn, vượt qua bao nhiêu sóng gió, nhưng Âm Nha Lý Thất Dạ - hắn vẫn không ngăn
            nổi sự kích động khi cuối cùng cũng đoạt lại thân thể của mình.<br /><br />Lý Thất Dạ hít sâu một hơi, ngẩng
            đầu nhìn lên, thấy một ông lão đang đứng trước mặt.<br /><br />- Hì, hì, hì, chính là lão già ta vớt ngươi
            từ dưới sông lên.<br /><br />Ông lão này cười cười lên tiếng. Khi ông ta mở miệng nói chuyện để lộ ra hàm
            răng chỉ còn ba chiếc vàng khè, khiến người ta cảm thấy nụ cười của ông ta đầy vẻ hèn mọn.<br /><br />Lý
            Thất Dạ liếc nhìn về phía thượng nguồn, từ xa hắn cũng có thể thấy được hình dáng mờ ảo của Tiên Ma Động.
            Nhìn thấy nó, ánh mắt của Lý Thất Dạ chợt trở nên lạnh lẽo khôn cùng, khí tức từ người hắn toát ra khác biệt
            hẳn so với những đứa trẻ cùng độ tuổi - mười ba.<br /><br />Lý Thất Dạ hít thêm một hơi thật sâu, quay lại
            nhìn ông lão, một lúc lâu sau hắn mới cất tiếng hỏi:<br /><br />- Nên xưng hô với ngươi như thế
            nào?<br /><br />- Tẩy Nhan Cổ Phái, Tam Quỷ Gia.<br /><br />Ông lão lại ngoác mồm lộ ba chiếc răng vàng,
            cười hì hì trả lời, nước miếng phun ra.<br /><br />- Tẩy Nhan Cổ Phái…<br /><br />Lý Thất Dạ thì thầm, cái
            tên này nhất thời gợi lên những ký ức từng bị phong ấn trong đầu hắn, lúc hồn phách của hắn còn đang bị nhốt
            trong thân thể của con quạ đen kia.<br /><br />- Hiện nay, ai là người đang chấp chưởng thiên
            mệnh?<br /><br />Lý Thất Dạ bình tĩnh lại, hỏi tiếp.<br /><br />- Thiên mệnh ư? Hiện thời vẫn chưa có ai có
            thể gánh được thiên mệnh.<br /><br />Tam Quỷ Gia vẫn cười hì hì trả lời.<br /><br />- Đạp Không Tiên Đế
            đâu?<br /><br />Nghe ông ta trả lời như vậy, sắc mặt của Lý Thất Dạ nhất thời đanh lại. Hắn đã ngủ say mất
            bao lâu? Hơn mười vạn năm hay sao?<br /><br />- Đạp Không Tiên đế đã mất tích từ ba vạn năm
            trước.<br /><br />Tam Quỷ Gia vẫn cười một cách hèn mọn, trả lời Lý Thất Dạ.<br /><br />- Vậy Hắc Long Vương
            của Trấn Thiên Hải thành thì sao?<br /><br />Lý Thất Dạ lại hỏi tiếp.<br /><br />- Không ai biết rõ cả, Hắc
            Long Vương cũng mất tích từ ba vạn năm trước.<br /><br />Tam Quỷ Gia lắc đầu đáp.<br /><br />Nghe xong, sắc
            mặt của Lý Thất Dạ biến đổi dữ dội, hắn lại ngẩng đầu nhìn về phía Tiên Ma Động đang ẩn hiện ở nơi xa. Lúc
            này Lý Thất Dạ đã hiểu vì sao hắn đoạt lại được thân thể của mình.<br /><br />- Chúng ta đi
            thôi.<br /><br />Vẻ mặt Lý Thất Dạ trầm xuống, hắn quay người rời đi, cũng mặc kệ Tam Quỷ Gia có bước theo
            hắn hay không. Hắn đã sống qua bao nhiêu năm tháng dằng dặc, trải qua muôn vàn cực khổ đắng cay, lúc này hắn
            biết mình nên làm như thế nào rồi.<br /><br />***<br /><br />Trấn Thiên Hải thành chính là truyền thừa cường
            đại nhất của một vị vô địch đương thời. Năm đó, khi Hắc Long Vương còn sống, nhìn khắp chín tầng trời mười
            tầng đất không ai có thể đánh lại. Ba đời đều được tôn kính như thế!<br /><br />Tuy Hắc Long Vương đã mất
            tích từ ba vạn năm nay, nhưng Trấn Thiên Hải thành vẫn như một quái vật khổng lồ, ngạo nghễ đứng giữa trời
            đất.<br /><br />Lúc này, một thiếu niên tầm mười ba tuổi và một ông lão có nụ cười hèn mọn lộ ra hàm răng
            với ba chiếc vàng chóe đang đứng ở phía ngoài Trấn Thiên Hải thành.<br /><br />Lý Thất Dạ yên lặng đứng đốt
            tiền giấy ở phía ngoài cửa thành, thì thầm:<br /><br />- Tiểu Hắc Tử, ngươi cứ an tâm đi. Kiếp này ngươi đã
            giúp ta đoạt lại được thân thể, một ngày nào đó ta sẽ đạp giết hung vực, báo thù cho ngươi.<br /><br />Xong
            xuôi, Lý Thất Dạ ngẩng đầu nhìn Trấn Thiên Hải thành khổng lồ phía trước, cảnh cũ mà người xưa đã không còn,
            hết thảy cũng trở nên lạ lẫm. Nhớ lại những năm tháng đó, hắn và Tiểu Hắc Tử đã cố gắng biết bao để xây dựng
            nên tòa hải thành hùng vĩ này.<br /><br />Đáng tiếc, qua ba vạn năm, mấy ai còn nhớ tới con quạ đen từng ẩn
            nấp phía sau màn.<br /><br />- Hì, chúng ta về Tẩy Nhan Cổ Phái thôi.<br /><br />Vào lúc này, Tam Quỷ Gia
            liếc nhìn Lý Thất Dạ, lên tiếng, ba chiếc răng vàng lấp lánh.<br /><br />- Chúng ta đi thôi.<br /><br />Lý
            Thất Dạ bình tĩnh gật đầu đáp. Cho dù vị Tam Quỷ Gia này có bản lĩnh xuất quỷ nhập thần thế nào, lai lịch ra
            sao cũng không thể khiến Lý Thất Dạ giật mình. Hắn đã trải qua hàng vạn khó khăn, hồn phách bị nhốt trong cơ
            thể con quạ đen Âm Nha trăm ngàn vạn năm, lần lượt đi qua từng thời đại một, sánh vai cùng Tiên Đế, kết bạn
            với Dược Thần. Thử hỏi còn cái gì có thể khiến hắn kinh ngạc được nữa?<br /><br />Hai người Lý Thất Dạ vừa
            rời đi không lâu, một người con gái vô cùng tao nhã, xinh đẹp bước ra từ Trấn Thiên Hải thành. Nàng giống
            như Bích Ba tiên tử, Hàn Hải nữ thần. Lúc nàng vừa bước ra khỏi cửa, ánh mắt vô tình liếc thấy một góc tờ
            tiền giấy còn sót lại trong đống vàng mã bị đốt. Trên đó còn có thể thấy một ký hiệu ẩn hiện.<br /><br />Vừa
            nhìn thấy ký hiệu đó, sắc mặt của vị Thần nữ đột biến, trầm giọng hỏi:<br /><br />- Vừa rồi đã có ai hóa
            tiền vàng mã ở đây?<br /><br />Một vị lão bộc bên người nàng vội đi hỏi thăm tin tức rồi nhanh chóng quay
            lại báo cáo:<br /><br />- Nghe thủ vệ nói, mới đây không lâu có một lão già và một thiếu niên tầm mười ba,
            mười bốn tuổi hóa vàng ở đây.<br /><br />- Mau đuổi theo cho ta, tìm bằng được bọn hắn.<br /><br />Vị Thần
            nữ này lập tức trầm giọng ra lệnh.<br /><br />- Điện hạ vốn phải đi Thần sơn đó.<br /><br />Lão bộc do dự
            nói.<br /><br />- Tìm!<br /><br />Vị Thần nữ này vừa quát khẽ một tiếng, thân hình đã phi xuyên không gian,
            truy tìm hai người kia.<br /><br />Rốt cuộc, vị Thần nữ này vẫn không tìm được người hóa vàng mã, đành phải
            quay trở lại Trấn Thiên Hải thành. Cô ta đứng trầm mặc tự vấn trong đầu. Ký hiệu kia từ lâu lắm đã không
            xuất hiện lại tại Trấn Thiên Hải thành, vì sao đột nhiên nó lại xuất hiện ở đây sau mấy vạn năm? Là địch hay
            là bạn?<br /><br />- Bẩm điện hạ, không tìm được người hóa vàng mã kia.<br /><br />Vị lão bộc trung thành
            tận tâm đứng bẩm báo.<br /><br />- Phân phó xuống, tất cả mọi người đều lưu ý, không được sao nhãng, có tin
            tức gì về hai người kia, lập tức báo cáo cho ta biết.<br /><br />Vị Thần nữ kia trầm giọng ra
            lệnh.<br /><br />Nghe chủ nhân nói như thế, lão bộc kinh ngạc không thôi. Dưới uy danh đương thời của Trấn
            Thiên Hải thành, tiếng tăm điện hạ Trấn Hải Thần Nữ của bọn họ cũng rất lừng lẫy, rất ít khi thấy nàng ta có
            thần thái ngưng trọng như vậy.<br /><br />- Vậy Thần sơn bên kia…<br /><br />Lão bộc trầm ngâm
            nói.<br /><br />- Hủy bỏ!<br /><br />Vị Thần nữ này nghiêm giọng.<br /><br />- Ta cần phải đọc thoáng qua
            một chút sách cổ mà tổ tông để lại, chuyện này thực có chút cổ quái.<br /><br />Nói xong nàng lập tức đi
            thẳng tới nơi sâu nhất trong cấm địa của Trấn Thiên Hải thành.<br /><br />***<br /><br />Tẩy Nhan Cổ Phái
            nằm trong cương quốc của Bảo Thánh thượng quốc, vốn là một Tiên Môn Đế Thống, có truyền thừa từ rất xa xưa.
            Vào những năm đầu của thời đại Chư Đế, một đời Minh Nhân Tiên Đế kiêu ngạo đứng giữa chín tầng trời mười
            tầng đất sáng lập ra môn phái này và đặt tên nó là Tẩy Nhan Cổ Phái.<br /><br />Đáng tiếc, trải qua trăm
            ngàn vạn năm, thời đại thay đổi, Tẩy Nhan Cổ Phái đã không còn là tiên môn đế thống tung hoành Tứ hải Bát
            hoang của ngày xưa nữa. Năm tháng trôi qua, Tẩy Nhan Cổ Phái dần dần xuống dốc, tuy đã cố gắng hết sức để
            trùng hưng lại uy danh thưở nào nhưng vẫn không có cách để vãn hồi xu hướng suy tàn này.<br /><br />- Trưởng
            lão, không tốt, có một phàm nhân tới môn phái chúng ta nói hắn muốn làm đại đệ tử thủ tịch .<br /><br />Đại
            trưởng lão của Tẩy Nhan cổ phái vừa bước ra khỏi cửa đã thấy môn hạ đệ tử vội vàng đi tới bẩm
            báo.<br /><br />- Đá hắn xuống núi!<br /><br />Đại trưởng lão không thèm liếc một cái, nói
            luôn.<br /><br />- Chuyện nhỏ như thế này cũng cần phải bẩm báo nữa sao?<br /><br />Một phàm nhân muốn làm
            đại đệ tử thủ tịch của Tẩy Nhan Cổ Phái ư? Nói đùa à? Làm đại đệ tử thủ tịch cũng có nghĩa là làm truyền
            nhân của chưởng môn, hơn nữa đại đệ tử thủ tịch thông thường cũng có khả năng trở thành chưởng môn đời kế
            tiếp. Tuy nhiên, nếu chưởng môn không có mặt ở trong môn phái thì Đại trưởng lão vẫn có thể tự mình giải
            quyết những vấn đề này.<br /><br />- Nhưng mà, nhưng mà hắn, hắn là người Tam Quỷ Gia giới thiệu
            đến.<br /><br />Vị đệ tử này giọng lắp bắp, vội vàng nói.<br /><br />- Tam Quỷ Gia?<br /><br />Đại trưởng
            lão nhíu mày không vui nói:<br /><br />- Hắn không phải do uống rượu của người ta mời mà mở miệng hứa hẹn
            cho người ta chỗ tốt đó chứ?<br /><br />Tam Quỷ Gia vốn là người của Tẩy Nhan Cổ Phái, nhưng Tẩy Nhan Cổ
            Phái lại không muốn thừa nhận thành viên này.<br /><br />Mặc dù cái tên Tam Quỷ Gia nghe thì rất uy phong,
            nhưng cái tên này lại khiến các trưởng lão Tẩy Nhan Cổ Phái không có chút mặt mũi nào.<br /><br />Tam Quỷ
            Gia có ba cái cực kỳ tốt, đó là tiêu tiền tốt, khoác lác tốt, chơi gái cũng tốt, lại còn thường tự xưng lão
            gia, cho nên thường bị người ta trêu tức gọi bằng Tam Quỷ Gia.<br /><br />Tam Quỷ Gia nghe nói chẳng tu
            luyện được công pháp nào ra hồn, nhưng lại có lai lịch rất lớn ở Tẩy Nhan Cổ Phái. Nghe đồn, ông ta là con
            riêng của chưởng môn đời trước, vì thế khi vị chưởng môn này sắp chết liền dặn dò chưởng môn đương nhiệm
            chiếu cố Tam Quỷ Gia giùm.<br /><br />Cũng có tin đồn rằng Tam Quỷ Gia đúng là con riêng nhưng là con riêng
            của chưởng môn cách đây hai đời, mà chưởng môn đời trước lại từng nhận đại ân của vị này, cho nên chưởng môn
            đó mới muối mặt nhận lấy vết nhơ này, cắn răng chiếu cố Tam Quỷ Gia. Trước khi vị chưởng môn của đời trước
            viên tịch thì ông ta cũng lại dặn dò chưởng môn đương nhiệm của Tẩy Nhan Cổ Phái phải đùm bọc cho Tam Quỷ
            Gia.<br /><br />Nói cho cùng, mặc kệ chuyện Tam Quỷ Gia là con riêng của ai đi nữa, các thành viên cao tầng
            của Tẩy Nhan Cổ Phái đều không muốn tìm hiểu làm gì và cũng chẳng bận tâm tới lời bàn tán của thiên
            hạ.<br /><br />Toàn bộ Tẩy Nhan Cổ Phái, từ trưởng lão cao tầng tới đệ tử thấp nhất đều không đón chào vị
            Tam Quỷ Gia không có bao nhiêu đạo hạnh này.<br /><br />- Tam Quỷ Gia giới thiệu thì thế nào, vẫn đá hắn
            xuống núi!<br /><br />Đại trưởng lão bực mình quát, mới sáng sớm đã bị tên này phá hỏng tâm tình
            tốt.<br /><br />- Nhưng mà, nhưng mà, hắn đang cầm Tẩy Nhan cổ lệnh của Tam Quỷ Gia.<br /><br />Vị đệ tử này
            lại lắp bắp nói tiếp.<br /><br />- Tẩy Nhan cổ lệnh!<br /><br />Vừa nghe xong mấy từ này, sắc mặt của Đại
            trưởng lão biến đổi khôn cùng, ông ta trầm ngâm một hồi lâu rồi phân phó:<br /><br />- Triệu kiến tất cả các
            trưởng lão, ngoài ra để kẻ phàm nhân kia tạm thời đợi ở đại điện.<br /><br />Tẩy Nhan Cổ Phái có tổng cộng
            sáu vị trưởng lão, năm vị kia vừa nghe đến bốn từ “Tẩy Nhan cổ lệnh” cũng đồng thời biến đổi sắc mặt, nhanh
            chóng đến dự họp.<br /><br />Thủy tổ của Tẩy Nhan Cổ Phái là Minh Nhân Tiên Đế từng để lại ba miếng Tẩy Nhan
            cổ lệnh, hai miếng cổ lệnh đã được thu hồi lại môn phái từ rất lâu rồi, nhưng không hiểu sao miếng thứ ba
            lại rơi vào trong tay của Tam Quỷ Gia.<br /><br />Ngoại trừ vì chưởng môn đời trước dặn dò đệ tử của Tẩy
            Nhan Cổ Phái phải chiếu cố cho Tam Quỷ Gia ra, nguyên nhân thứ hai khiến các trưởng lão tuy ức chế nhưng vẫn
            không làm gì được Tam Quỷ Gia chính là việc hắn sở hữu miếng cổ lệnh cuối cùng kia.<br /><br />Người cầm
            lệnh này đại biểu cho Minh Nhân Tiên Đế giá lâm. Người cầm lệnh này có thể đưa ra một điều kiện trao đổi với
            Tẩy Nhan Cổ Phái.<br /><br />Ngồi ở bên trong đại điện cũ kỹ này của Tẩy Nhan Cổ Phái, nhìn pho tượng mờ ảo
            giữa vầng khói kia, Lý Thất Dạ không kìm được lại tưởng nhớ tới rất nhiều chuyện xưa cũ.<br /><br />Pho
            tượng của Minh Nhân Tiên Đế đứng sừng sững trên chỗ cao nhất trong đại điện, tuy đã trải qua bao nhiêu năm
            tháng, nhưng pho tượng này vẫn toát ra một luồng thần uy cổ xưa tựa như xuyên thẳng chín tầng trời, khiến
            người ta nhìn vào không khỏi cảm thấy sùng bái.<br /><br />Lý Thất Dạ nhìn ngắm pho tượng này hồi lâu, hắn
            cũng không biết diễn tả lòng mình như thế nào nữa. Thời gian trôi vùn vụt, Minh Nhân Tiên Đế đã qua đời mà
            Lý Thất Dạ hắn vẫn còn sống. Hôm nay rốt cục hắn cũng toại nguyện, đoạt lại được thân thể của mình, nhưng
            nhìn lại chỉ thấy cố nhân, từng người, từng người một đã chìm trong mây khói thời gian.
            '
            ],

        ]);
    }
}
