<?php

use Illuminate\Database\Seeder;

class CategoryHasStoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_has_stories')->delete();
        DB::table('category_has_stories')->insert([
            ['id_category' => 1, 'id_stories' => 1],
            ['id_category' => 1, 'id_stories' => 3],
            ['id_category' => 2, 'id_stories' => 2],
            ['id_category' => 2, 'id_stories' => 3],
            ['id_category' => 7, 'id_stories' => 4],
            ['id_category' => 13, 'id_stories' => 4],
            ['id_category' => 10, 'id_stories' => 5],
            ['id_category' => 2, 'id_stories' => 6],
            ['id_category' => 6, 'id_stories' => 7],
            ['id_category' => 15, 'id_stories' => 8],
            ['id_category' => 7, 'id_stories' => 9],
            ['id_category' => 6, 'id_stories' => 9],
        ]);
    }
}
