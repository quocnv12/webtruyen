<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(ShortStoryTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(ReviewTableSeeder::class);
         $this->call(StoriesTabldeSeeder::class);
         $this->call(CategoryHasStoriesTableSeeder::class);
         $this->call(ChapterTableSeeder::class);
    }
}
